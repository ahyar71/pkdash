<?php


use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/dashboard/kost/{idkost}', 'dashboardcontroller@pemilik')->middleware('auth:pemilik');


Route::post('/new/kost', 'dashboardcontroller@newKost')->middleware('auth:pemilik');

Route::get('/kamar', 'kamarcontroller@index')->middleware('auth:pengurus');
Route::post('/kamar/create', 'kamarcontroller@create')->middleware('auth:pengurus');
Route::get('/kamar/edit', 'kamarcontroller@edit')->middleware('auth:pengurus');
Route::get('/kamar/editokamar', 'kamarcontroller@edit')->middleware('auth:pemilik');
Route::get('/kamar/destroy/{idkamar}', 'kamarcontroller@destroy')->middleware('auth:pengurus');
Route::get('/kamar/sudahservis/{idservis}', 'kamarcontroller@sudahservis')->middleware('auth:pengurus');

Route::get('/aset', 'asetcontroller@index')->middleware('auth:pengurus');
Route::post('/aset/create', 'asetcontroller@create')->middleware('auth:pengurus');
Route::get('/aset/edit', 'asetcontroller@edit')->middleware('auth:pengurus');
Route::get('/aset/destroy/{id}', 'asetcontroller@destroy')->middleware('auth:pengurus'); //BELUM


Route::get('/penghuni', 'penghunicontroller@index')->middleware('auth:pengurus');
Route::post('/penghuni/create', 'penghunicontroller@create')->middleware('auth:pengurus');
Route::post('/penghuni/edit', 'penghunicontroller@edit')->middleware('auth:pengurus');
Route::get('/penghuni/delete/{idpenghuni}/{idkamar}', 'penghunicontroller@destroy')->middleware('auth:pengurus');


Route::get('/keuangan', 'KeuanganController@index')->middleware('auth:pengurus');
Route::post('/keuangan/create', 'KeuanganController@create')->middleware('auth:pengurus');
Route::get('/keuangan/update', 'KeuanganController@index')->middleware('auth:pengurus');
Route::get('/keuangan/delete/{id}/{idpenghuni}/{idkamar}/{pilihan}/{jenis}', 'KeuanganController@destroy')->middleware('auth:pengurus');


Route::post('/send/email', 'dashboardcontroller@mail');
Route::post('/caritanggalpenjaga', 'KeuanganController@caritanggal')->middleware('auth:pengurus');
Route::post('/caritanggalpemilik', 'dashboardcontroller@caritanggal')->middleware('auth:pemilik');


Route::get('/pengurus', 'dashboardcontroller@pengurus')->middleware('auth:pengurus');
Route::get('/pemilik', 'dashboardcontroller@home')->middleware('auth:pemilik');


Route::get('/masuk', 'login@index');
Route::get('/login', 'login@index');

Route::post('/validasi', 'login@masuk');
Route::get('/logout', 'login@keluar');
Route::get('/loginvalid', 'login@index');


Route::post('/downloadfile', 'KeuanganController@downloadfile')->middleware('auth:pengurus');
Route::post('/downloadfilepemilik', 'dashboardcontroller@downloadfile')->middleware('auth:pemilik');


Route::get('/landing', 'login@landing');

Route::get('/loggedin', 'login@load');

Route::name('/login')->get('/masuk')->uses('login@index');

Route::name('/')->get('/landing');
Route::get('/', 'login@landing');
Route::get('/et_page', 'dashboardcontroller@etagih_index');
Route::get('/profil_page', 'dashboardcontroller@profil_index');
Route::get('/faq_page', 'dashboardcontroller@faq_index');


Route::get('/getPenghuni/{username}/{tag}', 'dashboardcontroller@getUsers');
Route::get('/getPenghuni', 'dashboardcontroller@getUsers1');
Route::get('/getFinKamar', 'dashboardcontroller@getFinKamar');
Route::get('/getWarning', 'dashboardcontroller@getWarning');
// Route::get('/getRincian', 'dashboardcontroller@getRincian')->middleware('auth:pengurus');
    Route::get('/getRincian', 'dashboardcontroller@getRincian')->middleware('auth:pengurus,pemilik');

Route::get('/getNominalBulanan', 'KeuanganController@getNominalBulanan');
Route::get('/getNominalLangsungLunas', 'KeuanganController@getNominalLangsungLunas');
Route::get('/getNominalPelunasan', 'KeuanganController@getNominalPelunasan');

Route::get('/penghuni_signup', 'penghunicontroller@penghuni_signup');

Route::get('/getKamarKostbyKost', 'penghunicontroller@getKamarKostbyKost');
Route::post('/newPenghuni', 'penghunicontroller@newPenghuni');


Route::get('/okupan', 'penghunicontroller@penghuniindex')->middleware('auth:penghuni');