jQuery(function($) {
  var dates = {
    '2019/1/1': 'some description',
    '2019/1/2': 'some other description'
  };

  $('#datepicker').datepicker({
    beforeShowDay: function(date) {
      var search = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();

      if (search in dates) {
        return [true, 'highlight', (dates[search] || '')];
      }

      return [false, '', ''];
    }
  });
});