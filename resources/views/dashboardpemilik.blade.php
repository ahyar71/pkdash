@extends('layoutpemilik.app')

@section('variabelphp')
       
@endsection

@section('judul', 'Dashboard Overview Kos')

@section('topbanner')
         @foreach($datakost as $rowkos)
          <div class="topbanner-dash" style="background-image: url('{{ URL::asset('/uploads/'.$rowkos->gambar)}}');">
                <div class="info-namakost">
                  @if(!empty($datakost))
                    @foreach($datakost as $rowdatakost)
                       <b> {{$rowdatakost->namakost}}</b>
                    @endforeach
                  @endif
                </div>
                <p style="font-family: Digital-7;">{{date("l, d F Y", strtotime('now'))}}
                </p>
                <div class="info-penjaga">
                    @if(!empty($namapenjaga))
                      Penjaga : 
                      @foreach($namapenjaga as $penjagakost)
                          <b> {{$penjagakost->nama}}</b>
                      @endforeach
                      @endif
                </div>
          </div>
          @endforeach
@endsection



@section('isibodi')
        <!-- Banner Ketersediaan Penjaga Kos -->
          @if(empty($namapenjaga))
          <div class="col-md-12 col-lg-12 info-nonpenjaga">
                @foreach($datakost as $rowdatakost)
                    Kost Belum Memiliki Akun Penjaga Kos
                    <br><button>Buat</button>
                @endforeach
          </div>
          @endif
        <!--  -->
        <div id="menukecil" class="stickykeuangan" style="display: inline-block;">
          <div class="col-xs-6 col-sm-6 col-md-6" style="display: inline-block;">
                  <button class="btn-lg"  data-toggle="modal" data-target="#downloaddata" style="background-color: white;color:orange; border-radius:50px; width: 100%; border: 2px dashed white;">
                    Download
                              <i class="fa fa-arrow-down"></i>
                  </button> 
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6" style="display: inline-block;">
                  <button class="btn-lg"  data-toggle="modal" data-target="#searchdata" style="background-color: white;color:orange; border-radius:50px; width: 100%; display: block; border: 2px dashed white;">
                  Search Trx <i class="fa fa-search"></i>
                  </button> 
          </div>
        </div>

            <div class="col-md-4 col-sm-12">     
                @include('fitur.panel_informasiterkini')

                @include('fitur.panel_kamarkosong')
            </div>
                                
                <div class="col-xs-12 col-sm-12 col-md-8"> 
                      <div class="col-xs-12 col-sm-12 col-md-12"  style="padding:0px;">    
                          @include('fitur.rinciankeuangan')
                      </div>                        
                      <div class="col-xs-12 col-sm-12 col-md-12" style="padding: 0px;"> 
                          @include('fitur.jatuhtempo')
                      </div>              
                      <div class="col-xs-12 col-sm-12 col-md-12" style="padding: 0px;"> 
                          @include('fitur.panel_daftarkamar')
                      </div>
                </div>



@endsection

@section('tambahan')
<!-- Modal -->
        <div class="modal fade" id="downloaddata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          <div class="modal-dialog" role="document">
            <div class="modal-content">
            <form  id="formdownload" class="submit_form" action="{{action('dashboardcontroller@downloadfile')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
              <div class="modal-header">
                <center><h1 style="color:white;" class="modal-title" id="exampleModalLabel"><b>Financial Download</b></h1></center>  
              </div>
              <div class="modal-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Download Berdasarkan...</label>
                    <select required class="form-control" id="dasar" name="dasar">
                      <option selected value="" disabled >Pilih Opsi</option>
                      <option value="rentang">Rentang Tanggal</option>
                      <option value="bulan">Bulan</option>
                    </select>
                  </div>

                  <div class="myRentang" id="showrentang">
                    <div class="form-group" >
                      <label for="exampleInputEmail1">Dari</label>
                        <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Awal" name="from" style="margin-bottom: 10px;">   
                      <label for="exampleInputEmail1">Ke</label>
                        <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Akhir" name="to">
                    </div>
                  </div>


                  <div class="myBulan" id="showbulan">
                    <div class="form-group">
                      <select name="pilihanbulan" class="form-control" >          
                        <option value="" selected disabled>Pilih Bulan</option>
                        <option value="1">Januari</option>  
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>  
                        <option value="4">April</option>  
                        <option value="5">Mei</option>  
                        <option value="6">Juni</option>  
                        <option value="7">Juli</option>  
                        <option value="8">Agustus</option>  
                        <option value="9">September</option>  
                        <option value="10">Oktober</option>  
                        <option value="11">November</option>  
                        <option value="12">Desember</option>  
                      </select>
                      <select name="pilihantahun" class="form-control" style="margin-bottom: 30px;" >                    
                        <option value="" selected disabled>Pilih Tahun</option>
                        @php $tahunawal = 2019; @endphp
                        @foreach (range(date('Y'), $tahunawal) as $x)
                        <option value="{{$x}}">{{$x}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>  

                  <span >
                    <div class="col-sm-12 col-lg-12 col-md-12">
                      <center>         
                      <button class="btn btn-lg btn-success" style="margin-bottom: 30px; margin-top: 20px;  width: 100%; padding: 20px; background-color: orange;">Download</button>
                  </center>
                  </div>
                    <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                    <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                    </div>
                  </span>   
              </div> 
            </form>
          </div>
        </div>
      </div>

      <div class="modal fade" id="searchdata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <center><h1 style="color:white;" class="modal-title" id="exampleModalLabel"><b>Financial Search</b></h1></center>  
              </div>
              <div class="modal-body">
              <form action="{{'\caritanggalpemilik'}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}} 
                  <label>Dari</label>
                  <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Awal" name="from" required style="margin-bottom: 10px;"> 
                  <label>Ke</label>
                  <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Akhir" name="to" required>                    
                      <button class="btn btn-lg btn-success" style="margin-bottom: 30px; margin-top: 20px;  width: 100%; padding: 20px; background-color: orange;">Search</button>
                    <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
              </form>
              </div>
          </div>
        </div>
      </div>


        @foreach($data_kamar as $rowkamar)
        <!-- Modal -->

        <div class="col-sm-12 col-md-12">
        <div class="modal fade" id="modalEdit{{$rowkamar->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
          </button>

          <div class="modal-dialog" role="document">
            <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Edit Kamar</b></h1></center>
                  </div>
              <div class="modal-body">
                <form action="/kamar/editokamar" method="get">
                  {{csrf_field()}}

                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Kamar</label>
                      <input name="namakamarkost" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{$rowkamar->namakamarkost}}" required value="{{$rowkamar->namakamarkost}}">
                      <input name="idkamar" hidden type="text" value="{{$rowkamar->id}}">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Harga</label>
                      <!-- ini belum  -->
                      <input name="harga" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{$rowkamar->harga}}" value="{{$rowkamar->harga}}">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kelengkapan Fasilitas</label>
                      <select class="form-control" name="kelengkapanfasilitas" required >
                        @if($rowkamar->kasur=='ada')
                      <option selected value="lengkap">Lengkap</option>
                      <option value="kosongan">Kosongan</option>
                        @elseif($rowkamar->kasur=='tidakada')
                      <option value="lengkap">Lengkap</option>
                      <option selected value="kosongan">Kosongan</option>
                      @endif
                    </select>
                    </div>
                      <span>
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                          <button type="submit" class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>   

                  </form>
              </div> 
            </div>
          </div>
        </div>
        </div>
        @endforeach


@endsection

@section('footer')


    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>

    <script>
        //fungsi daftar kamar
        $(document).ready(function(){

          $('.submit_form').on('submit',function(){
            var pass = prompt("Silahkan Ketik \"SAYA YAKIN\" :", "");
            if (pass == 'SAYA YAKIN') {
              return true;
            } else {
              return false;
            }
          });

        var demovalue2 = 'a'; //b
        $("div.myRentang").hide();
        $("div.myBulan").hide();
          $('#dasar').on('change', function(){
            var demovalue = $(this).val();
                if(demovalue2=='a'){
                  demovalue2 = demovalue; 
                }
                if(demovalue2!=demovalue){
                      $("#show"+demovalue2).hide();
                      $("#show"+demovalue).show();
                    demovalue2 = demovalue;
                }
                if(demovalue2==demovalue){
                      $("#show"+demovalue).show();
                    demovalue2 = demovalue;
                }
              }
          );

          $('#dasar').on('submit', function(){
                  $('form > input > display:none').attr("disabled",true);
                  $('form').submit();
          }
          );      
        });
     
        $(document).ready(function(){
          $("#liatkamar").click(function(){
            $("#isikamar").slideToggle("slow");
          });

          $("#liatuang").click(function(){
            $("#isiuang").slideToggle("slow");
          });

          $("#liatpenghuni").click(function(){
            $("#isipenghuni").slideToggle("slow");
          });

          $("#liathuni").click(function(){
            $("#isihuni1").slideToggle("slow");
            $("#isihuni2").slideToggle("slow");
            $("#isihuni3").slideToggle("slow");
          });
        });
          //fungsi detail daftar kamar
          $(document).ready(function(){
          var demovalue2 = 'a'; //b
          $("div.myKamar").hide();
            $('#idkamarkost').on('change', function(){
              var demovalue = $(this).val();
                  if(demovalue2=='a'){
                    demovalue2 = demovalue; 
                  }
                  if(demovalue2!=demovalue){
                        $("#default").hide();
                        $("#default2").hide();
                        $("#show"+demovalue2).hide();
                        $("#buka"+demovalue2).hide();
                        $("#show"+demovalue).show();
                        $("#buka"+demovalue).show();
                      demovalue2 = demovalue;
                  }
                  if(demovalue2==demovalue){
                        $("#default").hide();
                        $("#default2").hide();
                        $("#show"+demovalue).show();
                        $("#buka"+demovalue).show();
                      demovalue2 = demovalue;
                  }
                });
          });
  
  var start = 10;
   $('.scrollfield').scroll(function() {
     
      if($(this)[0].scrollHeight - $(this).scrollTop() == $(this).height() ||$(this)[0].scrollHeight - $(this).scrollTop() < $(this).height()+50) {
        fetchRincian(start);
        start += 10;
      }
     });

  function fetchRincian(start){
       $.ajax({
         url: '/getRincian',
         type: 'get',
         dataType: 'json',
         data: { 
          start: start
          },
         success: function(response){
           if(response['jumlahdata']!='0'){
              var hari=0;
              $.each(response['datarincian'], function(key, value) {
              var datafull = "";
              var tanggal = "";
              var datetanggaltrans = new Date(value['tanggaltransaksi']);
              const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
               
              if(datetanggaltrans.getDate()!=hari){
                  hari = datetanggaltrans.getDate();
                  // Bagian Tanggal Transaksi
                  tanggal += "<tr style=\"width: 100%; background-color: #f5f5f5;\">"+
                                "   <td colspan=\"4\"> "+
                                  "   <h4 style=\" color: #b3b3b3;\">"+
                                    "   <center>";
                  var tanggalhari = ""; 
                  if(datetanggaltrans.getDate()<10){
                    tanggalhari+="0"+datetanggaltrans.getDate();
                  }else{
                    tanggalhari+=datetanggaltrans.getDate();
                  }
                  tanggal+= "  <p>"+ tanggalhari+" "+monthNames[datetanggaltrans.getMonth()] +" "+ datetanggaltrans.getFullYear()+"</p>";
                  tanggal+= "</center>"+"</h4>"+" </td>"+" </tr>";
              }
              // Bagian Gambar Ikon Transaksi
              var gambarikon = "<tr height=\"30\" style=\"padding-bottom: 10px;\">"+
                                    " <!-- Ikon Transaksi -->"+
                                    "   <th rowspan=\"2\" style=\"vertical-align: middle; \"> "+
                                      "     <center>";
              if(value['pilihantransaksi']=='pemasukkan'){
                if(value['jenistransaksi']=='dp'){ 
                      gambarikon+="<img src=\"/uploads/ikondp.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='lunas'){
                      gambarikon+="<img src=\"/uploads/ikonpelunasan.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='lunaslangsung'){
                      gambarikon+="<img src=\"/uploads/lunaslangsung.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='bulanan'){
                      gambarikon+="<img src=\"/uploads/ikonbulanan.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='tambahan'){
                      gambarikon+="<img src=\"/uploads/extendhari.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='lain'||value['jenistransaksi']=='lainnya'){
                      gambarikon+="<img src=\"/uploads/incomelain.png\" width=\"30%\">"
                }
              }
              if(value['pilihantransaksi']=='pengeluaran'){
                if(value['jenistransaksi']=='listrik'){
                      gambarikon+="<img src=\"/uploads/ikonlistrik.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='air'){
                      gambarikon+="<img src=\"/uploads/ikonair.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='gaji'){
                      gambarikon+="<img src=\"/uploads/ikongaji.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='pemeliharaan'){
                      gambarikon+="<img src=\"/uploads/ikonpemeliharaan.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='lain'||value['jenistransaksi']=='lainnya'){
                      gambarikon+="<img src=\"/uploads/outcomelain.png\" width=\"30%\">"
                }
              }
              gambarikon+="</center></th>";

              //  <!-- Jenis Transaksi -->
              var jenistransaksi = 
                  "  <td style=\"vertical-align: bottom;\"><b>"+value['jenistransaksi'].toUpperCase()+
              "  </b></td>";
              // <!--  -->
              
              // <!-- Nominal Transaksi -->
              var totalnomi = Number(value['nominaltransaksi'])+Number(value['nominaltambahan']);
              var nomitrans = 
              "  <td class=\"textnominal\" style=\"vertical-align: bottom; padding-right: 10px\" align=\"right\">"+
                "        <b style=\" color: ";
                if(value['pilihantransaksi']=='pemasukkan'){nomitrans+=" green ";}
                if(value['pilihantransaksi']=='pengeluaran'){nomitrans+=" #F06560 ";}

                nomitrans+= " ; margin-bottom: 10px;\">"+totalnomi+"</b></td></tr>";
              // <!--  -->

              // <!-- Metode Transaksi -->
              var deskripsi =
                "<tr height=\"30\" style=\"padding-bottom: 10px;color: #b3b3b3;\">"+
                  "    <td class=\"textrincian\" align=\"left\" style=\"vertical-align: top; background-color:white;\">";
                
                    if(value['jatuhtempo']){
                      $.each(response['datanotpaid'], function(keynot, valuenot) {
                        if (valuenot['idpenghuni']==value['idpenghuni']){
                        deskripsi+= "<small>";
                          var datetrans = new Date(value['tanggaltransaksi']);
                          var datetempo = new Date(value['jatuhtempo']);
                          deskripsi+= monthNames[datetrans.getMonth()] +" "+ datetrans.getFullYear();
                          deskripsi+= " - "+monthNames[datetempo.getMonth()] +" "+ datetempo.getFullYear();
                        deskripsi+= "</small> <br>";
                        }
                      });
                    }
                  
                    if(value['namakamar']){
                      deskripsi+= "<small>"+value['namakamar']+"</small>";
                    }
                  if(value['jenistransaksi']=='lain'||value['jenistransaksi']=='lainnya'||value['jenistransaksi']=='gaji'||value['jenistransaksi']=='pemeliharaan'){
                    deskripsi+= value['deskripsi'];
                  }

                var metode = " </td><td class=\"ikonmetode\" style=\"vertical-align: top; padding-right: 10px;\" align=\"right\"><b>";
                  if(value['metode'] =='tunai'){
                    metode+="<img src='/uploads/ikontunai.png'>";
                  }else if(value['metode']=='transfer'){
                    metode+="<img src='/uploads/ikontransfer.png'>";
                  }
                  metode+="</b></td></tr>";
                  // <!--  -->



              if(tanggal!=""){
                datafull=tanggal+gambarikon+jenistransaksi+nomitrans+deskripsi+metode;
              }else{
                datafull=gambarikon+jenistransaksi+nomitrans+deskripsi+metode;
              }
              var datarinci = 
                $(datafull).hide().fadeIn(3000);
                $("#datauang").append(datarinci);
              });
                 
            



            }
         },
         error: function(response){
          $("input#nominaltransaksi").val('Silahkan periksa input penghuni, lamasewa, dan jenis penghuni');
         }
        });
  }
    </script>

    

@endsection
