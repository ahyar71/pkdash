<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/login.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css.css')}}">
</head>
<body>  

<div class="container-login100" style="background-image: url({{ URL::asset('/uploads/2.jpg')}});">

    <div class="wrap-login100 p-b-30" style="padding-bottom: 190px;">

@include('flash-message')
<center style="margin-bottom: 40px;font-family: Montserrat-ExtraBold;"><h1>LOGIN</h1></center>
        <form class="login100-form validate-form" action="/validasi" method="post">
            {{csrf_field()}} 


            <div class="wrap-input100 validate-input m-b-10" style="margin-bottom: 10px;" data-validate = "Username is required" >
                <input class="input100" type="text" name="username" placeholder="Username" required>
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-user"></i>
                </span>
            </div>

            <div class="wrap-input100 validate-input m-b-10" style="margin-bottom: 10px;" data-validate = "Password is required" >
                <input class="input100" type="password" name="password" placeholder="Password" required>
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-lock"></i>
                </span>
            </div>

            <div class="container-login100-form-btn p-t-10" style="padding-top: 10px;">
                <button class="login100-form-btn">
                    Login
                </button>
            </div>
        </form>
<div style="margin-top: 20px;">
</div>
    </div>
</div>


<script src="{{asset('js/select2.min.js')}}"></script>
</body>
</html>                                		                            