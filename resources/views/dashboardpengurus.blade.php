@extends('layout.app')

@section('variabelphp')  

@endsection

@section('judul', 'Dashboard Overview Kos')


@section('isibodi')

@extends('fitur.stickydesc')


    <div class="topbanner-dashpenjaga" style="height:30vh;">
          </div>          
    </div> 
<div class="container" style="padding-bottom: 30px;">
  <div class="row" style="margin-top: 20px;"> 
    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">   
        <button class="btn btn-secondary dropdown-toggle headpanel-datadashboard" type="button" data-toggle="modal" data-target="#ModalInfo"  aria-expanded="false">
          Informasi Terkini Kost
        </button>        
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 panel-datadashboard" >   
        <button class="btn btn-secondary" type="button" onclick="location.href='{{ URL('/penghuni')}}'" aria-expanded="false" style="background-color: #ff7b00;">
          <i class="fas fa-users"></i> <br>Data Penghuni  
        </button>        
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 panel-datadashboard" >   
        <button class="btn btn-secondary " type="button" onclick="location.href='{{ URL('/keuangan')}}'" aria-expanded="false" style="background-color: #ff9e43;">
          <i class="fas fa-money"></i> <br>Data Keuangan
        </button>        
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 panel-datadashboard" >   
        <button class="btn btn-secondary " type="button" onclick="location.href='{{ URL('/kamar')}}'" aria-expanded="false" style="background-color: #ff5e00;">
          <i class="fas fa-home"></i> <br>Data Kamar
        </button>        
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 panel-datadashboard" >   
        <button class="btn btn-secondary " type="button" onclick="location.href='{{ URL('/aset')}}'" aria-expanded="false" style="background-color: #ff9028;">
          <i class="fas fa-bed"></i>   <br>Data Aset
        </button>        
    </div>
  </div>

  
  <div class="row" style="margin-top: 20px;"> 
    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12" onclick="window.open('/penghuni')" style="cursor: pointer;background-color:orange; padding:20px;text-align:center;color:white;">   
          Lihat Username dan tag penghuni
    </div>
  </div>

    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12" style="margin-top:20px;margin-bottom: 20px">   
      <div class="modal fade" id="ModalInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-dialog" style="max-width: 1500px;" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Informasi Terkini Kost</b></h1></center>
                  </div>
              <div class="modal-body">
                    <div class="col-md-3 col-sm-12 col-lg-3">
                       @include('fitur.panel_tunaitersedia')
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" >
                            @include('fitur.jatuhtempo')
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3" style="margin-bottom: 100px;" >
                        @include('fitur.panel_informasiterkini')  
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>

</div>

@endsection

@section('tambahan')

@endsection

@section('footer')

    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart-data.js') }}"></script>

    
  <script>
        // slideup/slidedown
        $("#Trigger").click(function () {
            if ($("#Slider").hasClass("slideup"))
                $("#Slider").removeClass("slideup").addClass("slidedown");
            else
                $("#Slider").removeClass("slidedown").addClass("slideup");
        });


        $("#Trigger2").click(function () {
            if ($("#Slider2").hasClass("slideup"))
                $("#Slider2").removeClass("slideup").addClass("slidedown");
            else
                $("#Slider2").removeClass("slidedown").addClass("slideup");
        });

      $(document).ready(function(){

      if ($(window).width() < 960) {
        $('#menu1080').hide();
        $('#menu960').show();
      }
      else {
        $('#menu960').hide();
        $('#menu1080').show();
      }
    });
  </script>
 
@endsection