@extends('layout.app')



@section('judul','Data Kamar')


@section('isibodi')
<div class="container" >
  <div class="row" >
      <div class="col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading" style="background-color: #1ED760; border: 2px solid white; color:white; ">
                Total Kamar Saat ini
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal" style="float: right; border-radius: 50px;">
                    <i class="fa fa-plus"></i>
                </button>
              </div>
              <div class="panel-body" style="text-align:center;">
                <h4 style="color:white;"><b></b></h4>
                <h1>{{count($data_kamar)}}</h1>
              </div>
            </div>

        <div class="panel panel-teal">
          <div class="panel-heading">
            <h3><b>Daftar Kamar</b></h3>
          </div>
          <div class="panel-body">
            <select id="idkamarkost" name="idkamar" required="" class="form-control" style="margin-top: 10px; background-color: #3e89d8; color: white;">
              <option id="pilkamar" value="" selected="" disabled="">Pilih Kamar</option>
                @if(!empty($data_kamar)) 
                    @foreach ($data_kamar as $rowkamar)
                        <option class="form-control" value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                    @endforeach
                @else
                   <option id="kamarnotfound" value="" disabled selected="">Pilihan Kamar Tidak Ada</option>
                @endif
                   </select>
          </div>
        </div>
      </div>
        
      <div class="col-md-6">
        <div id="default" class="default">
          <div class="panel panel-ateal">
            <div class="panel-heading">
              <h3 id="judulkamar"><b>Kamar</b></h3>
              <hr>
            </div>
            <div class="panel-body">
              <div class="detail">
                <center><img src="https://t3.ftcdn.net/jpg/00/64/67/52/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg" alt="default-gambar" width="200px" height="200px"  /></center>
                <h3>Nama Penghuni</h3>
                <h3><b> . . . .</b></h3>
                <h3 class="kosong"><b>BELUM TERDATA</b></h3>
              </div>
            </div>
          </div>
        </div>
        @if(!empty($data_kamar))
        @foreach ($data_kamar as $rowkamar)
        <div id="show{{$rowkamar->id}}" class="myKamar">
          <div class="panel panel-ateal">
            <div class="panel-heading">
              <h3 id="judulkamar"><b>{{$rowkamar->namakamarkost}}</b></h3>
              <hr>
            </div>
            <div class="panel-body">
              <div class="detail">
                <center>
                  @if ($rowkamar->getFotoPenghuni())
                  <a href="{{ URL::asset('/uploads/'.$rowkamar->getFotoPenghuni())}}">
                    <img href ="" alt="default-gambar" src="{{ URL::asset('/uploads/'.$rowkamar->getFotoPenghuni())}}" width="200px" height="200px" style="object-fit: cover;"/>
                  </a>
                  <br>
                  @else
                  <img src="https://t3.ftcdn.net/jpg/00/64/67/52/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg" width="200px" height="200px" />
                  @endif
                </center>
                <h3>Nama Penghuni</h3>
                @if(!empty($rowkamar->getNamaPenghuni()))
                  <h3><b>{{$rowkamar->getNamaPenghuni()}}</b></h3>
                  @elseif(empty($rowkamar->getNamaPenghuni()))
                  <h3><b> . . . .</b></h3>
                @endif
                @if($rowkamar->status=='terisi' &&$rowkamar->getJatuhTempo()!=null)
                  <h3 class="paid"><b>SAMPAI {{date("d F Y",strtotime($rowkamar->getJatuhTempo()))}}</b></h3>
                @elseif($rowkamar->getStatusBayar()=='belumbayar'&&$rowkamar->status=='kosong')
                  <h3 class="notpaid"><b></b></h3>
                @elseif($rowkamar->status=='kosong')
                  <h3 class="notpaidoff"><b>KAMAR KOSONG</b></h3>
                @elseif($rowkamar->getStatusBayar()=='belumbayar')
                  <h3 class="paid"><b>SAMPAI {{date("d F Y",strtotime($rowkamar->getJatuhTempo()))}}</b></h3>
                @else
                  <h3 class="kosong"><b>BELUM MELAKUKAN PEMBAYARAN</b></h3>
                @endif
              </div>
            </div>
          </div>
        </div>
        @endforeach
        @endif
      </div>

      <div class="col-md-3">
          <div id="default2" class="default2">
                    <div class="panel panel-default">
                      <div class="panel-heading" style="background-color: #1ED760; color:white;">
                        Detail Kamar                
                      </div>
                      <div class="panel-body">
                        <div class="input-container">
                          <p style="background-color:orange;color:white;padding:10px;width: 50%">Harga</p>
                          <p style="background-color:#1ED760;color:white;padding:10px;width: 50%">Rp. - </p>
                        </div>
                            <div class="input-container">
                              <i class="fa fa-bed icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
                                <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
                            </div>

                            <div class="input-container">
                              <i class="fa fa-tshirt icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
                                <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
                            </div>

                            <div class="input-container">
                              <i class="fa fa-chair icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
                                <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
                            </div>

                            <div class="input-container">
                              <i class="fa icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;">AC</i>
                                <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
                            </div>

                            <div class="input-container">
                              <i class="fa fa-toilet icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
                                <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
                            </div>

                            <div class="input-container">
                              <i class="fa fa-bed icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
                                <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
                            </div>

                          
                    </div>
                  </div>
                </div>

              @foreach($data_kamar as $rowkamar)
                <div id="buka{{$rowkamar->id}}" class="myKamar">
                  <div class="panel panel-default">
                      <div class="panel-heading" style="background-color: #1ED760; color:white;">
                      Detail Kamar
                    </div>
                    <div class="panel-body">

                      <div class="input-container">
                        <p style="background-color:orange;color:white;padding:10px;width: 50%">Harga</p>
                        <p style="background-color:#1ED760;color:white;padding:10px;width: 50%"><b>Rp. {{$rowkamar->harga}}</b></p>
                      </div>

                      @foreach($data_aset as $rowaset)
                        @if($rowaset->idkamar==$rowkamar->id)
                            <div class="input-container">
                              <i class="fa icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;">{{$rowaset->nama}}</i>
                              <i class="fa fa-check icon" style="font-size: 20px; background-color: #1ED760; width: 50%;"></i>
                            </div>
                        @endif
                      @endforeach


                        <div class="input-container">
                          <span style=" padding: 20px; width: 100%; background: orange;font-family:'Montserrat', sans-serif;color:white;">Tambah Fasilitas Kamar<a href="/aset"><i class="fa fa-plus icon" style="font-size: 20px; background-color: grey; width: 100%;"></i></a></span>
                        </div>
                        
                        <div class="input-container">
                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalEdit{{$rowkamar->id}}" style="float: right; border-radius: 50px; width:100%;">
                                Edit Kamar  <i class="fa fa-pen"></i>
                            </button>
                        </div>

                        <div class="input-container">
                            @if($rowkamar->status=='kosong')
                            @php $arraydata = ['idkamar'=>$rowkamar->id];@endphp
                          <form class="delete_form" action="{{action('kamarcontroller@destroy',$arraydata)}}" method="get" style="width: 100%;">
                            {{csrf_field()}}
                            <center>
                              <button type="submit" class="btn btn-danger" onclick="" style="float: right; margin-left:4px;width: 100%; border-radius: 50px; " >                      
                                Hapus <i class="fa fa-trash"></i>
                              </button>
                            </center>
                          </form>
                          @endif
                        </div>

                        <div class="input-container">
                          <span style=" padding: 20px; width: 100%; background: orange; width: 100%; font-family:'Montserrat', sans-serif;color:white;">
                            <center>Jadwal Pemeliharaan Aset</center>

                          @foreach($data_aset as $rowaset)
                                @if($rowaset->idkamar==$rowkamar->id && $rowaset->jangkaservice!=null)
                                 @if((strtotime("+7 day")>=strtotime('+'.$rowaset->jangkaservice.' months',strtotime($rowaset->waktuservis)))&&(strtotime("-7 days"))<=strtotime('+'.$rowaset->jangkaservice.' months',strtotime($rowaset->waktuservis)))
                                 <div style="font-size: 100%; background-color: red; width: 100%; color: white; padding: 20px;">
                                    <center>
                                      @php $arrayservis = ['id'=>$rowaset->id]; @endphp
                                      <form class="sudahservis_form" action="{{action('kamarcontroller@sudahservis',$arrayservis)}}" method="get">
                                        {{csrf_field()}}
                                        Saatnya Pemeliharan, Klik Disini Apabila Sudah <br>
                                        {{strtoupper($rowaset->nama)}} - {{date('d F Y',strtotime("+".$rowaset->jangkaservice."  months",strtotime($rowaset->waktuservis)))}}
                                        <button type="submit" style="width: 100%; margin-top: 10px;" class="btn btn-danger" onclick="" style="float: left; margin-left:4px;" >                      
                                          Hapus <i class="fa fa-trash"></i>
                                        </button>
                                      </form>
                                      ............................................
                                    </center>
                                  </div>
                                 @else
                                  <div style="font-size: 100%; background-color: grey; width: 100%; color: white; padding: 20px;">
                                    <center>
                                      {{strtoupper($rowaset->nama)}} - {{date('d F Y',strtotime("+".$rowaset->jangkaservice."  months",strtotime($rowaset->waktuservis)))}}
                                    </center>
                                  </div>
                                  @endif
                                @endif
                          @endforeach
           
                          </span>
                        </div>


              </div>
            </div>

          </div>


        @endforeach
      </div>


      </div>
    </div>
      

@endsection
@section('tambahan')
        <!-- Modal -->
        <div class="col-sm-12 col-md-12">
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
          </button>
          <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Tambah Kamar</b></h1></center>
                  </div>
              <div class="modal-body">
                <form action="/kamar/create" id="formcari" method="POST">
                  {{csrf_field()}}
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Kamar</label>
                <input name="namakamarkost" type="text" required class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Kamar">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Harga</label>
                <input name="harga" type="number" class="form-control" required  id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Harga">
              </div>
              <!-- <div class="form-group">
                <label for="exampleInputEmail1">Kelengkapan Fasilitas</label>
                <select required class="form-control" name="kelengkapanfasilitas">
                  <option selected value="" disabled >Pilih Kelengkapan</option>
                <option value="lengkap">Lengkap</option>
                <option value="kosongan">Kosongan</option>
              </select>
              </div> -->
              
                      <span >
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                          <button class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>   
            </form>
            </div>
          </div>
        </div>
    </div>
  </div>

        @foreach($data_kamar as $rowkamar)
        <!-- Modal -->

        <div class="col-sm-12 col-md-12">
        <div class="modal fade" id="modalEdit{{$rowkamar->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
          </button>

          <div class="modal-dialog" role="document">
            <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Edit Kamar</b></h1></center>
                  </div>
              <div class="modal-body">
                <form action="/kamar/edit" method="get">
                  {{csrf_field()}}

                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Kamar</label>
                      <input name="namakamarkost" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{$rowkamar->namakamarkost}}" required value="{{$rowkamar->namakamarkost}}">
                      <input name="idkamar" hidden type="text" value="{{$rowkamar->id}}">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Harga</label>
                      <!-- ini belum  -->

                      @php if($rowkamar->status=="terisi"){ @endphp
                      <br>
                      <div style="background-color:grey;color:white;padding:10px;text-align:center;">Kamar Telah Terisi, Ubah Harga terlebih dahulu sebelum input data pembayaran penghuni</div>
                      <input name="harga" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{$rowkamar->harga}}" value="{{$rowkamar->harga}}">
                       @php }else if($rowkamar->status=="kosong") { @endphp
                      <input name="harga" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{$rowkamar->harga}}" value="{{$rowkamar->harga}}">
                      @php } @endphp

                    </div>
                      <span>
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                          <button type="submit" class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>   

                  </form>
              </div> 
            </div>
          </div>
        </div>
        </div>
        @endforeach
@endsection

@section('footer')


    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>



  <script>


    //fungsi detail daftar kamar
    $(document).ready(function(){
    var demovalue2 = 'a'; //b
    $("div.myKamar").hide();
      $('#idkamarkost').on('change', function(){
        var demovalue = $(this).val();
            if(demovalue2=='a'){
              demovalue2 = demovalue; 
            }
            if(demovalue2!=demovalue){
                  $("#default").hide();
                  $("#default2").hide();
                  $("#show"+demovalue2).hide();
                  $("#buka"+demovalue2).hide();
                  $("#show"+demovalue).show();
                  $("#buka"+demovalue).show();
                demovalue2 = demovalue;
            }
            if(demovalue2==demovalue){
                  $("#default").hide();
                  $("#default2").hide();
                  $("#show"+demovalue).show();
                  $("#buka"+demovalue).show();
                demovalue2 = demovalue;
            }
          }
      );
    });

    $(document).ready(function() {
      $('.delete_form').on('submit',function(){
        if(confirm("Yakin kamar mau di hapus?"))
        {
          return true;
        }
        else{
          return false;
        }
      });
    });

     $(document).ready(function() {
      $('.sudahservis_form').on('submit',function(){
        var pass = prompt("Sudah Servis? Silahkan Ketik \"SAYA YAKIN\" :", "");
        if (pass == 'SAYA YAKIN') {
          return true;
        } else {
          return false;
        }
      });
    });
    

  </script>

  
@endsection