

@component('mail::message')
# Hi {{$nama}}

Tagihanmu Sudah Mau Deadline

@component('mail::table')
| Nama          | Nama Kamar    |   Tagihan    |
| ------------- |:-------------:| ------------:|
| {{$nama}}      | {{$namakamar}}      | {{$nominal}}      |
@endcomponent

@component('mail::panel')
Jatuh Tempo : {{date('F j, Y',$jatuhtempo)}}
@endcomponent

@component('mail::button', ['url' => 'http://localhost:8000', 'color' => 'green'])
Tanya Pengelola ?
@endcomponent

@endcomponent