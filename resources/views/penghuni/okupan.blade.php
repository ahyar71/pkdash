@extends('layout.apppenghuni')

@section('variabelphp')
@endsection


@section('judul','Premiere Friends - Your Friends in Kostan Need')


@section('isibodi')

<div class="container" style="padding-bottom: 40px;" >
  <div class="row">
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="jadwalBayar" id ="bannerBayar">
          
      </div>
    </div>
  </div>
</div>
@endsection

@section('tambahan')

@endsection

@section('footer')

    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tes.js') }}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


  <script type="text/javascript">
  $(document).ready(function () {
    var wideBayar = 
          "<div class=\"panel panel-ateal\">"+
            "<div class=\"panel-heading\">"+
              "<h3 id=\"judulkamar\"><b>Overview</b></h3>"+
              "<hr>"+
              "</div>"+
              "<div class=\"panel-body\">"+
                "<div class=\"detail\">"+
                  "<center>"+
                    "@if($data_user->fotonik)"+
                    "<a href=\"{{ URL::asset('/uploads/'.$data_user->fotonik)}}\"><img href =\"\" src=\"{{ URL::asset('/uploads/'.$data_user->fotonik)}}\" width=\"200px\" height=\"200px\" style=\"object-fit: cover;\"/></a>"+
                    "<br>"+
                    "@else"+
                    "<img src=\"https://t3.ftcdn.net/jpg/00/64/67/52/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg\" width=\"200px\" height=\"200px\" />"+
                    "@endif"+
                    "</center>"+
                    "<div class=\"\" style=\"text-align: center; margin-top:10px;font-size:1.5em;\">"+
                      "<i class=\"fa fa-user\"></i>"+
                      "@if(!empty($data_user->namapenghuni))"+
                      " {{$data_user->namapenghuni}}"+
                      "@elseif(empty($data_user->namapenghuni))"+
                      ". . . ."+
                      "@endif"+
                      "</div>"+
                      "<div class=\"\" style=\"text-align: center;border-radius:10px;background-color:#6be696; padding:10px;margin-top:10px;font-size:1.2em;\">"+
                        "<h1 style=\"margin-bottom: 0px;color:white;\">"+
                          
                          " Tanggal Masuk {{$data_user->tanggalmasuk}}</h1>"+
                          
                          
                          " {{$sisahari}} Hari</h1>"+
                          " Menuju Pembayaran Selanjutnya<br>"+
                          "   @foreach ($data_kamar as $rowkamar)"+
                          "     @if(!empty($rowkamar->getJatuhTempo()))"+
                          "      {{date("d F Y",strtotime($rowkamar->getJatuhTempo()))}}"+
                          "    @elseif(empty($rowkamar->getJatuhTempo()))"+
                          "        . . . .{{$data_kamar}}"+
                          "     @endif"+
                          "     <h2 style=\"padding-top:10px;color:white;margin-top: 0px;\">Rp. {{$rowkamar->getNominal()}}</h2>"+
                          "   @endforeach"+
                      " </div>"+
                  
              "</div>"+
            "</div>"+
          "</div>";
        $("#bannerBayar").empty();
        $("#bannerBayar").append(wideBayar);
      
  });


  $(document).ready(function () {
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      } 
    });
  }
  });

  </script>
  

@endsection