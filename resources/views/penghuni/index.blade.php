@extends('layout.app')

@section('variabelphp')
@endsection


@section('judul','Data Penghuni')


@section('isibodi')

<div class="container" style="padding-bottom: 40px;" >
          <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #1ED760; border: 2px solid white; color:white; ">
                  Total Penghuni Saat ini
                  @php if($tot_kosong!=0){ @endphp
                  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#ModalTambah" style="float: right;border-radius: 50px;">
                      <i class="fa fa-plus"></i>
                  </button>
                  @php } @endphp
                </div>
                <div class="panel-body" style="text-align:center;">
                  <h4 style="color:white;"></h4>
                  <h1>{{count($data_penghuni)}}</h1><br>
                </div>
                  @php if($tot_kosong==0){ @endphp
                <div class="panel-heading" style="background-color: #FED049; border: 2px solid white; color:white; ">
                  Yes! Kamar Kost Sudah Penuh
                </div>
                @php } @endphp
              </div>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-8">
              <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #1ED760; border: 2px solid white; color:white;">
                  Daftar Penghuni
                </div>
                <div class="panel panel-cteal col-md-3 col-sm-4 col-xs-9" style="float: right;">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                      <div class="square-orens"></div><span class="teksorens"><small>Edit</small></span>
                    </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                      <div class="square-merah"></div><span class="teksmerah"><small>Hapus</small></span>
                    </div>
                </div>

                <div class="panel-body" style="border-bottom:2px solid orange; padding: 10px;">

                    

                  <input class="form-control formcari" id="search" style="margin-bottom: 20px;" type="text"  value="" placeholder="Pencarian Penghuni">
                  
                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding: 0px;">
                    <div class="listpenghuni">
                      <table class="table table-dark">
                        <thead style="position: sticky; top:0; z-index: 4;">
                          <th class="col-lg-4 col-md-4 col-sm-4">Nama</th>
                          <th class="col-lg-3 col-md-3 col-sm-3">Kamar</th>
                          <th class="col-lg-2 col-md-2 col-sm-2">No. Telfon</th>
                          <th class="col-lg-4 col-md-4 col-sm-4"></th>
                        </thead>
                        <tbody id="penghuni" style="overflow-y: hidden;">
                          @php $no=0; if(count($data_penghunisemua)!=0){ @endphp
                          @foreach($data_penghunisemua as $rowpenghuni) @php $no++ @endphp
                              <tr>
                                <td class="col-lg-4 col-md-4 col-sm-3">{{$rowpenghuni->namapenghuni}}
                                @if($rowpenghuni->tanggalkeluar==null)
                                  <i class="fa fa-check" aria-hidden="true"></i>
                                @elseif($rowpenghuni->tanggalkeluar!=null)
                                  <i class="fa fa-times" aria-hidden="true"></i>
                                @endif
                                </td>
                                <td class="col-lg-3 col-md-3 col-sm-3">{{$rowpenghuni->getKamarNama()}}</td>
                                <td class="col-lg-2 col-md-2 col-sm-2"><a href="{{ url('https://wa.me/'.$rowpenghuni->nopenghuni)}}">{{$rowpenghuni->nopenghuni}}</a></td>
                                <td class="col-md-3 col-sm-3">
                                <center>
                                  <div class="col-lg-4 col-md-4 col-sm-4" >
                                  <button type="button" class="btn btn-warning" data-toggle="modal" style="float: left; margin-top: 5px; padding-left:8px; padding-right: 20px;" data-target="{{'#exampleModal'.$rowpenghuni->id}}"><i class="fa fa-user" ></i></button>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-4">                                  
                                      <button type="submit" class="btn btn-success" style="float: left;  margin-top: 5px; padding-left:8px; padding-right: 20px;" onclick="reviewgo({{$rowpenghuni->nopenghuni}},'{{$rowpenghuni->namapenghuni}}')"><i class="fa fa-pen"></i></button>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-4">
                                    @if($rowpenghuni->tanggalkeluar==null)
                                        <button type="submit" class="btn btn-danger" data-toggle="modal"  data-target="{{'#deleteModal'.$rowpenghuni->id}}"  style="float: left;  margin-top: 5px; padding-left:8px; padding-right: 20px;" ><i id="ikon" class="fa fa-sign-out-alt"></i></button>
                                    @endif
                                  </div>
                                </center>
                                </td>
                              </tr>
                          @endforeach
                        </tbody>
                          @php }else{ @endphp
                              <tr style="color: black; text-align: center; border:none;"><td>Data tidak ditemukan</td></tr>
                          @php } @endphp
                      </table>
                    </div>
                </div>
                </div>
              </div>
            </div><!--/.col-->
        </div>
      </div>
@endsection

@section('tambahan')
        <!-- --------------------------------------------------------------------------------------------------------------------------->
        @php if(count($data_penghunisemua)!=0){ @endphp
        @foreach($data_penghunisemua as $rowpenghuni)
            <div class="modal fade" id="{{'exampleModal'.$rowpenghuni->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Edit Penghuni</b></h1></center>
                  </div>
                  <div class="modal-body">
                  <form action="{{action('penghunicontroller@edit')}}" id="formcari" method="post" class="ubahan" enctype="multipart/form-data">
                      {{csrf_field()}}
                        
                          <!--1. Kolom Input Nama Penghuni-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Penghuni</label><br>
                                @if($rowpenghuni->tanggalkeluar==null)
                                <input type="text" name="namapenghuni" maxlength="70" class="form-control" value="{{$rowpenghuni->namapenghuni}}" required>
                                <input type="text" name="idpenghuni" hidden value="{{$rowpenghuni->id}}" required>
                                <input type="text" name="status" value="terisi" hidden>
                                @elseif($rowpenghuni->tanggalkeluar!=null)                                
                                <div class="form-group"> 
                                  <div class="input-container">
                                    <i class="fa fa-user icon" style="background-color: #a6a6a6;"></i>
                                    <input class="input-field" type="text" value="{{$rowpenghuni->namapenghuni}}" disabled>
                                        </div>
                                </div>
                                @endif
                              </div>
                          <!--2. Kolom Input Kamar Penghuni-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kamar Hunian</label><br>
                                @if($rowpenghuni->tanggalkeluar==null)
                                  <div class="alert bg-danger" id="alertpindah" role="alert" ><center>
                                  <em class="fa fa-lg fa-warning">&nbsp;</em><br><b>Perhatian!<br><small> Kamar Penghuni : </small> {{$rowpenghuni->getKamarNama()}}</b></center></div>

                                  <select id="idkamar" name="idkamar" required="" class="form-control" style="margin-top: 8px; background-color: #3e89d8; color: white;">
                                    <option  class="form-control" disabled value="" style="background-color: red;color:white">Hanya untuk penghuni pindah kamar</option>
                                      @if(!empty($data_kamar))
                                        @foreach ($data_kamar as $rowkamar)
                                            @if($rowpenghuni->getStatusKamar()=='terisi')
                                                @if($rowkamar->id==$rowpenghuni->idkamar)
                                                <option  class="form-control" selected value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                                            @endif
                                            @elseif($rowpenghuni->getStatusKamar()=='kosong')                                   
                                                @if($rowkamar->id==$rowpenghuni->idkamar)
                                                  <option  class="form-control" selected value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                                                @else
                                                  @if($rowkamar->status=='kosong')
                                                      <option  class="form-control" value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                                                  @endif
                                                @endif
                                            @endif
                                        @endforeach
                                      @else
                                    <option id="kamarnotfound" value="" selected="">Pilihan Kamar Tidak Ada</option>
                                  @endif
                                </select>
                                @elseif($rowpenghuni->tanggalkeluar!=null)
                                <div class="form-group"> 
                                  <div class="input-container">
                                    <i class="fa fa-home icon" style="background-color: #a6a6a6;"></i>
                                    <input class="input-field" type="text" value="{{$rowpenghuni->getKamarNama()}}" disabled>
                                  </div>
                                </div>
                                @endif
                              </div>

                      <!--5. Kolom Input IP Penghuni-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Masuk</label>  
                    <span class="pull-right"><input type="checkbox" id="editmasuk" name="editmasuk"> <label>Edit Tanggal</label></span>
                        <div class="input-container">
                          <i class="fa icon" style="background-color: green;">IN</i>
                          <input class="input-field" name="ubaholdtanggalmasuk" type="text" value="{{date("d F Y",strtotime($rowpenghuni->tanggalmasuk))}}" disabled>

                          <input class="input-field" name="ubahtanggalmasuk" type="date" value="{{date("d F Y",strtotime($rowpenghuni->tanggalmasuk))}}">
                        </div>
                      </div>
                      <!--5. Kolom Input Tanggal Keluar Penghuni-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Keluar</label>  
                        <div class="input-container">
                          <i class="fa icon">OUT</i>
                          <input class="input-field" type="text" value="@php if($rowpenghuni->tanggalkeluar!=null){
                                echo date("d F Y",strtotime($rowpenghuni->tanggalkeluar));
                                }else if($rowpenghuni->tanggalkeluar==null){
                                echo "None";
                                } @endphp                          
                          " disabled>
                        </div> 
                        <br>               
                      </div>

                      <!--5. Kolom Input Tanggal Keluar Penghuni-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Username & Tag</label>  
                        <div class="input-container">
                          <input class="input-field" type="text" value="{{$rowpenghuni->username}}" disabled>
                          <input class="input-field" type="text" value="{{"#".$rowpenghuni->tag}}" disabled>
                        </div> 
                        <br>               
                      </div>

                      <!--2. Kolom Input NIK Penghuni-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">NIK (KTP/SIM/Identitas)</label>       
                        <br>          
                                @if($rowpenghuni->tanggalkeluar==null)
                                <input type="number" name="nik" minlength="8" maxlength="30" value="{{$rowpenghuni->nik}}" required class="form-control">                            
                                @elseif($rowpenghuni->tanggalkeluar!=null)
                                <div class="form-group"> 
                                  <div class="input-container">
                                    <i class="fa fa-user icon" style="background-color: #a6a6a6;"></i>
                                    <input class="input-field" type="text" value="{{$rowpenghuni->nik}}" disabled>
                                        </div>
                                </div>
                                @endif
                              </div>
                          <!--2. Kolom Input Foto Penghuni-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Foto KTP</label> <br>
                        <div class="polaroid">
                                <a href="{{ URL::asset('/uploads/'.$rowpenghuni->fotonik)}}"><img src="{{ URL::asset('/uploads/'.$rowpenghuni->fotonik)}}" width="300px" height="150px"  style="object-fit: cover;"/></a>
                                <div class="container-pol">Klik Foto untuk memperbesar</div>
                              </div>
                                @if($rowpenghuni->tanggalkeluar==null)
                                <input type="file" name="filefoto" value="{{$rowpenghuni->fotonik}}" id="filefoto" style="margin-top: 10px;">
                                @endif
                                <input type="text" name="fileexist" hidden value="{{$rowpenghuni->fotonik}}" id="fileexist" style="margin-top: 10px;">
                              </div>


                          <!--4. Kolom Input MAC Address-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nomor Telepon</label> <br>
                                @if($rowpenghuni->tanggalkeluar==null)
                                <input type="number" name="nopenghuni" value="{{$rowpenghuni->nopenghuni}}" required class="form-control" maxlength="15">                        
                                @elseif($rowpenghuni->tanggalkeluar!=null)
                                <div class="form-group"> 
                                  <div class="input-container">
                                    <i class="fa fa-phone icon" style="background-color: #a6a6a6;"></i>
                                    <input class="input-field" type="text" value="{{$rowpenghuni->nopenghuni}}" disabled>
                                        </div>
                                </div>
                                @endif
                              </div>
                          <!--4. Kolom Input MAC Address-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Pekerjaan</label> <br>
                                @if($rowpenghuni->tanggalkeluar==null)
                                <input type="text" name="pekerjaan" value="{{$rowpenghuni->pekerjaan}}" required class="form-control" maxlength="70">                   
                                @elseif($rowpenghuni->tanggalkeluar!=null)
                                <div class="form-group"> 
                                  <div class="input-container">
                                    <i class="fa fa-suitcase icon" style="background-color: #a6a6a6;"></i>
                                    <input class="input-field" type="text" value="{{$rowpenghuni->pekerjaan}}" disabled>
                                        </div>
                                </div>
                                @endif
                              </div>
                          <!--4. Kolom Input MAC Address-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email Penghuni</label> <br>
                                @if($rowpenghuni->tanggalkeluar==null)
                                <input type="email" name="email" value="{{$rowpenghuni->email}}" class="form-control" maxlength="100">     
                                @elseif($rowpenghuni->tanggalkeluar!=null)
                                <div class="form-group"> 
                                  <div class="input-container">
                                    <i class="fa fa-envelope icon" style="background-color: #a6a6a6;"></i>
                                    <input class="input-field" type="text" value="{{$rowpenghuni->email}}" disabled>
                                        </div>
                                </div>
                                @endif
                              </div>
                          <!--5. Kolom Input IP Address-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kendaraan</label> <br>
                                @if($rowpenghuni->tanggalkeluar==null)
                                <select name="jeniskendaraan" class="form-control">
                                  <option value="mobil" @if($rowpenghuni->jeniskendaraan=='mobil') selected @endif>Mobil</option>
                                  
                                  <option value="motor" @if($rowpenghuni->jeniskendaraan=='motor') selected @endif>Motor</option>
                                  
                                  <option value="none" @if($rowpenghuni->jeniskendaraan=='none') selected @endif>Tidak Ada</option>
                                  
                                </select>                            
                                @elseif($rowpenghuni->tanggalkeluar!=null)
                                <div class="form-group"> 
                                  <div class="input-container">
                                    <i class="fa fa-car icon" style="background-color: #a6a6a6;"></i>
                                    <input class="input-field" type="text" value="{{$rowpenghuni->jeniskendaraan}}" disabled>
                                        </div>
                                </div>
                                @endif
                              </div>
                          <!--5. Kolom Input IP Address-->
                      <div class="form-group">
                        <label for="exampleInputEmail1">Plat Kendaraan</label> <br>
                                @if($rowpenghuni->tanggalkeluar==null)
                                <input type="text" name="platkendaraan" value="{{$rowpenghuni->platkendaraan}}" class="form-control" maxlength="20">
                                @elseif($rowpenghuni->tanggalkeluar!=null)
                                <div class="form-group"> 
                                  <div class="input-container">
                                    <i class="fa icon" style="background-color: #a6a6a6;">1 2 3</i>
                                    <input class="input-field" type="text" value="{{$rowpenghuni->platkendaraan}}" disabled>
                                        </div>
                                </div>
                                @endif
                              </div>
                          
                          <!--8. Kolom Input Deskripsi Barang-->
                      <div class="form-group">
                          <label for="exampleInputEmail1">Catatan</label> <br>
                            @if($rowpenghuni->tanggalkeluar==null)
                            <textarea name="deskripsi" required class="form-control" size="3" maxlength="300">{{$rowpenghuni->deskripsi}}</textarea>
                            @elseif($rowpenghuni->tanggalkeluar!=null)
                                <div class="form-group"> 
                                  <div class="input-container">
                                    <i class="fa fa-book icon" style="background-color: #a6a6a6;"></i>
                                    <input class="input-field" type="text" value="{{$rowpenghuni->deskripsi}}" disabled>
                                        </div>
                                </div>
                            @endif
                      </div>
                      <!--Tombol Submit dan Batal-->
                      <span >
                                @if($rowpenghuni->tanggalkeluar==null)
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                          <button class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                          @endif
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; margin-top:10px;background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>   
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>
        @endforeach
        
        @foreach($data_penghunisemua as $rowpenghuni)
            <div class="modal fade" id="{{'deleteModal'.$rowpenghuni->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Pengeluaran Penghuni</b></h1></center>
                  </div>
                  <div class="modal-body">
                      
                      <center><h4>Kamu Akan Menghapus Data Penghuni : </h4><h1><b style="color:white;">{{$rowpenghuni->namapenghuni}}</b></h1></center>
                      
                   @php $arraydata = ['idkamar'=>$rowpenghuni->idkamar,'idpenghuni'=>$rowpenghuni->id]; @endphp
                  <form class="delete_form" action="{{action('penghunicontroller@destroy',$arraydata)}}" method="get">
                  {{csrf_field()}}
                        
                      <!--Tombol Submit dan Batal-->
                      <span>
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                          <button class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; margin-top:10px;background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>   
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>
        @endforeach
        
        @php }else{} @endphp

        <!-- Modal -->
        <div class="modal fade" id="ModalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Tambah Penghuni</b></h1></center>
                  </div>
              <form action="{{action('penghunicontroller@create')}}" class="ubahan" method="post" enctype="multipart/form-data">
              <div class="modal-body">

                  {{csrf_field()}}
                   <!--1. Kolom Input Nama Penghuni-->
                  <div class="form-group">
                  <label for="exampleInputEmail1">Nama Penghuni</label>                    
                  <input type="text" name="namapenghuni" maxlength="70" class="form-control" required >                      
                  </div>
                   <!--1. Kolom Input Kamar Penghuni-->
                  <div class="form-group">
                      <label for="exampleInputEmail1">Kamar Hunian</label>  
                      <input type="text" name="status" value="terisi" hidden>
                        <select id="idkamarkost" name="idkamar" required="" class="form-control" style="margin-top: 10px; background-color: #3e89d8; color: white;">
                          <option id="pilkamar" value="" selected="" disabled="">Pilih Kamar</option>
                          @if(!empty($data_kamar)) @php $counter=0 @endphp
                              @foreach ($data_kamar as $rowkamar)
                                  @if($rowkamar->status=='kosong') 
                                    @php $counter++ @endphp <!-- hitung kamar kosong -->
                                    <option class="form-control"  value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                                  @endif
                              @endforeach
                              @if($counter==0)
                                  <option class="form-control" disabled selected >Kamar Kos Penuh</option>
                              @endif
                          @else
                                <option id="kamarnotfound" value="" disabled selected="">Pilihan Kamar Tidak Ada</option>
                          @endif
                        </select>
                  </div>
                   <!--2. Kolom Input NIK Penghuni-->
                  <div class="form-group">
                    <label for="exampleInputEmail1">NIK (KTP/SIM)</label> 
                    <input type="number" name="nik" minlength="8" maxlength="30" required class="form-control">                     
                  </div>
                   <!--2. Kolom Input Username Penghuni-->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Username</label> 
                    <input type="text" name="username" minlength="3" maxlength="30" required class="form-control">                     
                  </div>
                  <!--2. Kolom Input Foto Penghuni-->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Foto KTP</label>  
                    <span class="pull-right"><input type="checkbox" id="cekktp" name="cekktp"> <label>Foto Nanti</label></span>
                    <input class="form-control" style="background-color: #1ED760; border: 2px dashed white; color:white; font-weight: bold; width: 100%;" type="file" name="filefoto" id="filefoto">
                    <input type="text" name="fileexist" hidden value="none.jpg" id="fileexist" style="margin-top: 10px;">
                  </div>
                  <!--4. Kolom Input No HP Penghuni-->
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nomor Telepon</label>  
                      <input type="number" name="nopenghuni" required class="form-control" maxlength="15">
                      <label>Contoh: 62812xxxxxxx</label>
                  </div>
                   <!--4. Kolom Input Pekerjaan Penghuni-->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Pekerjaan</label>                          
                    <input type="text" name="pekerjaan" required class="form-control" maxlength="70">
                   </div>
                   <!--4. Kolom Input Email Penghuni-->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <span class="pull-right"><input type="checkbox" id="cekemail" name="cekemail"> <label>Tidak punya</label></span>
                    <input type="email" name="email" class="form-control" maxlength="100">
                  </div>
                   <!--5. Kolom Input Kendaraan Penghuni-->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Kendaraan</label>
                    <select id="jeniskendaraan" name="jeniskendaraan" required class="form-control">
                              <option value="mobil" selected>Mobil</option>
                              <option value="motor" selected>Motor</option>
                              <option value="none" selected>Tidak Ada</option>
                    </select>
                  </div>
                   <!--5. Kolom Input Plat Kendaraan Penghuni-->
                  <div class="form-group plat">
                    <label for="exampleInputEmail1">Plat Kendaraan</label>
                    <input type="text" name="platkendaraan" id="plat" class="form-control" maxlength="20">
                  </div>
                   <!--5. Kolom Input Tanggal Masuk Penghuni-->
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Masuk</label>
                    <input type="date" name="tanggalmasuk" required class="form-control" maxlength="15" value="{{date("Y-m-d",strtotime("now"))}}">
                  </div>
                   <!--8. Kolom Input Keterangan Lain Penghuni-->                  
                  <div class="form-group">
                    <label for="exampleInputEmail1">Catatan</label>
                    <textarea name="deskripsi" required class="form-control" size="3" maxlength="300"></textarea>
                  </div>
                </div>  
                      <!--Tombol Submit dan Batal-->
                      <span >
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                          <button class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>   
            </form>
            </div>
          </div>
        </div>

@endsection

@section('footer')

    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tes.js') }}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


  <script>
  function reviewgo(nomort,nama) {
    window.open("https://api.whatsapp.com/send?phone="+nomort+"&text=Assalamualaikum%2C%20Selamat%20pagi%0APerkenalkan%20saya%20Lina%2C%20pemilik%20Kost%20Setia%20Premiere.%20Mohon%20maaf%20mengganggu%20waktunya.%20Kami%20Ingin%20menyampaikan%20informasi%20bahwa%20mulai%20hari%20ini%20mbak%20yati%20dan%20kang%20asep%20sudah%20tidak%20bekerja%20sebagai%20penjaga%20kosan%20lagi.%20Sehingga%20untuk%20sementara%20waktu%2C%20kami%20sendiri%20yang%20akan%20mengelola%20kost.%20%0A%0AAgar%20lebih%20memudahkan%20mengenali%20penghuni%20kost.%20Mohon%20untuk%20mengirimkan%20kepada%20kami%20%2AFoto%20Selfie%2A%20atau%20%2AFoto%20Diri%2A%20di%20chat%20ini%20ya.%0A%0AJika%20ada%20keperluan%2C%20silahkan%20hubungi%20nomor%20ini%20baik%20melalui%20telpon%20maupun%20whatsapp%20chat%20ini.%20%28081212800534%29%0A%0ATerima%20kasih%20atas%20kerjasamanya%2C%0AMohon%20dimaafkan%20apabila%20ada%20kesalahan%20dari%20penjaga%20kost%20kami%20selama%20ini.%0ASelamat%20beraktivitas.%20Wassalamualaikum.%0A%0A-----%0APengelola%20Kost%20Setia%20Premiere%0AJalan%20Setia%201%20no.%2010%2C%20Jatiwaringin%2C%20Pondok%20Gede%2C%20Kota%20Bekasi%0Apremierekost.com%20%7C%20instagram.com%2Fpremierekost");
  }
  </script>

  <script>
    //fungsi search

    function teswa(){
      var eco = encodeURI(document.getElementById("komplain").value);
      window.open('https://api.whatsapp.com/send?phone=6281213073571&text='+eco);
    }

    $(function () {
      
              $('input[name="ubahtanggalmasuk"]').hide();
              $('input[name="ubahtanggalmasuk"]').prop('disabled',true);
              
      //show it when the checkbox is clicked
      $('input[name="cekktp"]').on('click', function () {
          if ($(this).prop('checked')) {
              $('input[name="filefoto"]').fadeOut();
          } else {
              $('input[name="filefoto"]').fadeIn();
          }
      });

      $('input[name="cekemail"]').on('click', function () {
          if ($(this).prop('checked')) {
              $('input[name="email"]').fadeOut();
          } else {
              $('input[name="email"]').fadeIn();
          }
      });


      $('input[name="editmasuk"]').on('click', function () {
          if ($(this).prop('checked')) {
              $('input[name="ubaholdtanggalmasuk"]').prop('required',false);
              $('input[name="ubaholdtanggalmasuk"]').hide(); 
              $('input[name="ubahtanggalmasuk"]').fadeIn();
              $('input[name="ubahtanggalmasuk"]').prop('disabled',false);
              $('input[name="ubahtanggalmasuk"]').prop('required',true);
          } else {
              $('input[name="ubahtanggalmasuk"]').prop('disabled',true);
              $('input[name="ubahtanggalmasuk"]').prop('required',false);
              $('input[name="ubahtanggalmasuk"]').hide(); 
              $('input[name="ubahtanggalmasuk"]').val(null);
              $('input[name="ubaholdtanggalmasuk"]').fadeIn();
              $('input[name="ubaholdtanggalmasuk"]').prop('required',true);
          }

      });


      //show it when the option is choosen
      $('#jeniskendaraan').on('change', function () {
          var jenis = $(this).val();
          if(jenis=='mobil'||jenis=='motor'){
              $('div.plat').fadeIn();      
              $("#plat").prop('required',true);       
          }else{
              $('div.plat').fadeOut(); 
              $("#plat").prop('required',false); 
          }
      });

    });

    $(document).ready(function(){
      $('div.plat').hide(); 
      $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#penghuni tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });

// -------------------------------------------------------------------------------------------------------------------------


    $(document).ready(function() {
      $('.delete_form').on('submit',function(){
        var pass = prompt("Silahkan Ketik \"SAYA YAKIN\" :", "");
        if (pass == 'SAYA YAKIN') {
          return true;
        } else {
          return false;
        }
      });
    });

    $(document).ready(function() {
      $('.ubahan').on('submit',function(){
        var pass = prompt("Silahkan Ketik \"SAYA YAKIN\" :", "");
        if (pass == 'SAYA YAKIN') {
          return true;
        } else {
          return false;
        }
      });
    });
// -------------------------------------------------------------------------------------------------------------------------
  </script>


@endsection