<div class="panel panel-default">
  <div class="panel-heading" style="background-color: #1ED760; border: 2px solid white; color:white;">
              <center>Kamar Kosong</center>
  </div>
    <div style="padding:20px; background-color: #6AB180; color:white;border-radius: 10px; background-color:white; color:orange;">
      <div class="panel-body"  style="border:2px solid #6AB180; border-radius: 10px; margin-top: 10px;">
          @if($persenkamarterisi!=0)
            <div class="easypiechart" id="easypiechart-orange" data-percent="<?=$persenkamarterisi?>"><span class="percent" style="color:#6AB180;">{{$tot_kosong}} <font size="2" >/  {{count($data_kamar)}}</font></span></div>
          @elseif($persenkamarterisi==0&&count($data_kamar)!=0)
            <div class="easypiechart" id="easypiechart-orange" data-percent="<?=$persenkamarterisi?>" ><span class="percent">FULL</span></div>
          @elseif($persenkamarterisi==0&&count($data_kamar)==0)
            <div class="easypiechart" id="easypiechart-orange" data-percent="<?=$persenkamarterisi?>" ><span class="percent">No Data</span></div>
          @endif
      <div class="scrollfield">
        <div class="panel_kamarkosong">
            <span class="judul_kamarkosong"><b>Daftar Kamar</b></span>
          @foreach($data_kamar as $rowkamar)
            @if($rowkamar->status=='kosong')
              <div class="isi_kamarkosong">
                <img src="{{ URL::asset('/uploads/ikonkamarkosong.png')}}">
                <p>
                      {{$rowkamar->namakamarkost}}
                </p>
              </div>
            @endif
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>