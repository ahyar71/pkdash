<div class="panel panel-default">    
<div class="panel-body" style="background-color: #1ED760; min-height: 60vh;">
  <div class="scrollfield" id="scrollDataKeuangan" style="height:100%;">   
  <table style=" border:0px; margin-top: 10px; background-color: white;" width="100%"> 
      <col width="40">
      <col width="70">
      <col width="50">
      <tbody id="datauang">
        @php $count=0; $hari=0; @endphp
        @if(!empty($data_keuanganlimit))                                    
          @foreach($data_keuanganlimit as $rowcari)                         
            @if($rowcari->statustransaksi=='sudah') 
              @php $count++ @endphp     
                <!-- Row Tanggal Transaksi -->
                @if(date("d",strtotime($rowcari->tanggaltransaksi))!=$hari)
                  @php $hari = date("d",strtotime($rowcari->tanggaltransaksi)); @endphp
                    <tr style="width: 100%; background-color: #f5f5f5;">
                      <td colspan="4">
                        <h4 style=" color: #b3b3b3;">
                          <center>
                            {{date("d F Y",strtotime($rowcari->tanggaltransaksi))}}<br>
                            <p hidden>{{$rowcari->tanggaltransaksi}} </p>
                          </center>
                        </h4>
                      </td>
                    </tr>
                @endif
                <!--  -->

                <!-- Rincian Transaksi -->
                <tr height="30" style="padding-bottom: 10px;">
                    <!-- Ikon Transaksi -->
                      <th rowspan="2" style="vertical-align: middle; "> 
                        <center>
                          @if($rowcari->pilihantransaksi=='pemasukkan')
                            @if($rowcari->jenistransaksi=='dp')
                              <img src="{{ URL::asset('/uploads/ikondp.png')}}" width="30%">
                            @elseif($rowcari->jenistransaksi=='lunas')
                              <img src="{{ URL::asset('/uploads/ikonpelunasan.png')}}" width="30%">
                            @elseif($rowcari->jenistransaksi=='lunaslangsung')
                              <img src="{{ URL::asset('/uploads/lunaslangsung.png')}}" width="30%">
                            @elseif($rowcari->jenistransaksi=='bulanan')
                              <img src="{{ URL::asset('/uploads/ikonbulanan.png')}}" width="30%">
                            @elseif($rowcari->jenistransaksi=='tambahan')
                              <img src="{{ URL::asset('/uploads/extendhari.png')}}" width="30%">
                            @elseif($rowcari->jenistransaksi=='lain'||$rowcari->jenistransaksi=='lainnya')
                              <img src="{{ URL::asset('/uploads/incomelain.png')}}" width="30%">
                            @endif
                          @endif
                          @if($rowcari->pilihantransaksi=='pengeluaran')
                            @if($rowcari->jenistransaksi=='listrik')
                              <img src="{{ URL::asset('/uploads/ikonlistrik.png')}}" width="30%">
                            @elseif($rowcari->jenistransaksi=='air')
                              <img src="{{ URL::asset('/uploads/ikonair.png')}}" width="30%">
                            @elseif($rowcari->jenistransaksi=='gaji')
                              <img src="{{ URL::asset('/uploads/ikongaji.png')}}" width="30%">
                            @elseif($rowcari->jenistransaksi=='pemeliharaan')
                              <img src="{{ URL::asset('/uploads/ikonpemeliharaan.png')}}" width="30%">
                            @elseif($rowcari->jenistransaksi=='lain'||$rowcari->jenistransaksi=='lainnya')
                              <img src="{{ URL::asset('/uploads/outcomelain.png')}}" width="30%">
                            @endif
                          @endif
                        </center>
                      </th>
                    <!--  -->

                    <!-- Jenis Transaksi -->
                      <td style="vertical-align: bottom;"><b>{{strtoupper($rowcari->jenistransaksi)}} 
                        @if($rowcari->nominaltambahan!=0)
                          + Denda/Tambahan
                        @endif
                      </b></td>
                    <!--  -->
                    <!-- Nominal Transaksi -->
                      <td class="textnominal" style="vertical-align: bottom; padding-right: 10px" align="right">
                            <b style=" color: 
                                @if($rowcari->pilihantransaksi=='pemasukkan')
                                    green
                                  @elseif($rowcari->pilihantransaksi=='pengeluaran')
                                    #F06560
                                @endif
                                ; margin-bottom: 10px;">{{number_format($rowcari->nominaltransaksi+$rowcari->nominaltambahan)}}</b>
                      </td>
                    <!--  -->
                </tr>
                <tr height="30" style="padding-bottom: 10px;color: #b3b3b3;">
                <!-- Metode Transaksi -->
                    <td class="textrincian" align="left" style="vertical-align: top; background-color:white;">
                      @if(!empty($rowcari->jatuhtempo))
                        <small>
                          @foreach ($data_keuangan_notpaid as $rownotpaid)
                              @if ($rownotpaid->idpenghuni==$rowcari->idpenghuni)
                              {{date("M Y",strtotime($rownotpaid->tanggaltransaksi))}} - 
                              {{date("M Y",strtotime($rownotpaid->jatuhtempo))}} 
                              @endif
                          @endforeach
                        </small> <br>
                        @endif
                        <small>
                          {{$rowcari->getKamarKost()}}
                        </small>
                        @if($rowcari->jenistransaksi=='lain'||$rowcari->jenistransaksi=='lainnya'||$rowcari->jenistransaksi=='gaji'||$rowcari->jenistransaksi=='pemeliharaan')
                          {{$rowcari->deskripsi}}
                        @endif
                    </td> 
                    <td class="ikonmetode" style="vertical-align: top; padding-right: 10px;" align="right">
                      <b>
                          @if($rowcari->metode =='tunai')
                          <img src="{{ URL::asset('/uploads/ikontunai.png')}}" >
                          @elseif($rowcari->metode=='transfer')
                          <img src="{{ URL::asset('/uploads/ikontransfer.png')}}" >
                          @endif
                          </b>
                    </td>  
                <!--  -->
                </tr>

                <!--  -->
            @endif
          @endforeach 
          @endif
        </tbody>
      </table>
  </div> <!-- scrollfield -->
</div> <!-- panelbody -->
</div> <!-- paneldefault -->

