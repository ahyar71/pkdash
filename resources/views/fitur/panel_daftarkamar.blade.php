<div class="panel-heading" id="liathuni" style="background-color: #1ED760; border: 2px solid white; color:white;">
  <center>
    Daftar Kamar
    <span id="liathuni" class="pull-right"><em class="fa fa-toggle-up"></em></span>
  </center> 
</div>   

<!-- List kamar dari kost -->
<div class="col-xs-12 col-sm-12 col-md-12" style=" margin-bottom: 40px; " >
      <div id="isihuni1" class="panel-body" style="display: none; padding-top: 0px;">
        <div class="panel panel-default">
          <div class="panel-body">
            <select id="idkamarkost" name="idkamar" required="" class="form-control" style="margin-top: 0px; background-color: #3e89d8; color: white;">
              <option id="pilkamar" value="" selected="" disabled="">Pilih Kamar</option>
                @if(!empty($data_kamar)) 
                    @foreach ($data_kamar as $rowkamar)
                        <option class="form-control" value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                    @endforeach
                @else
                   <option id="kamarnotfound" value="" disabled selected="">Pilihan Kamar Tidak Ada</option>
                @endif
            </select>
          </div>
        </div>
      </div>
</div>
<!--  -->

<!-- Detail dari Kamar -->
<div id="isihuni3" class="panel-body" style="display: none; margin-top:0px;">
  <div class="col-xs-12 col-sm-12 col-md-12"  style="padding: 0px;">
    <!-- Default Tampilan Detail Data Kamar -->
    <div id="default" class="default">
      <div class="panel panel-ateal">
        <div class="panel-heading">
          <h3 id="judulkamar"><b>Kamar</b></h3>
          <hr>
        </div>
        <div class="panel-body">
          <div class="detail">
            <center><img src="https://t3.ftcdn.net/jpg/00/64/67/52/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg" width="200px" height="200px" /></center>
            <h3>Nama Penghuni</h3>
            <h3><b> . . . .</b></h3>
            <h3 class="kosong"><b>BELUM TERDATA</b></h3>
          </div>
        </div>
      </div>
    </div>
    <!--  -->

    <!-- Tampilan Masing-Masing Kamar -->
   @if(!empty($data_kamar))
    @foreach ($data_kamar as $rowkamar)
    <div id="show{{$rowkamar->id}}" class="myKamar">
      <div class="panel panel-ateal">
        <div class="panel-heading">
          <h3 id="judulkamar"><b>{{$rowkamar->namakamarkost}}</b></h3>
          <hr>
        </div>
        <div class="panel-body">
          <div class="detail">
            <center>
              @if ($rowkamar->getFotoPenghuni())
              <a href="{{ URL::asset('/uploads/'.$rowkamar->getFotoPenghuni())}}"><img href ="" src="{{ URL::asset('/uploads/'.$rowkamar->getFotoPenghuni())}}" width="200px" height="200px" style="object-fit: cover;"/></a>
              <br>
              @else
              <img src="https://t3.ftcdn.net/jpg/00/64/67/52/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg" width="200px" height="200px" />
              @endif
            </center>
            <h3>Nama Penghuni</h3>
            @if(!empty($rowkamar->getNamaPenghuni()))
              <h3><b>{{$rowkamar->getNamaPenghuni()}}</b></h3>
              @elseif(empty($rowkamar->getNamaPenghuni()))
              <h3><b> . . . .</b></h3>
            @endif
            @if($rowkamar->status=='terisi' &&$rowkamar->getJatuhTempo()!=null)
              <h3 class="paid"><b>SAMPAI {{date("d F Y",strtotime($rowkamar->getJatuhTempo()))}}</b></h3>
            @elseif($rowkamar->getStatusBayar()=='belumbayar'&&$rowkamar->status=='kosong')
              <h3 class="notpaid"><b></b></h3>
            @elseif($rowkamar->status=='kosong')
              <h3 class="notpaidoff"><b>KAMAR KOSONG</b></h3>
            @elseif($rowkamar->getStatusBayar()=='belumbayar')
              <h3 class="paid"><b>SAMPAI {{date("d F Y",strtotime($rowkamar->getJatuhTempo()))}}</b></h3>
            @else
              <h3 class="kosong"><b>BELUM MELAKUKAN PEMBAYARAN</b></h3>
            @endif
          </div>
        </div>
      </div>
    </div>
    @endforeach
    @endif
    <!--  -->

    <!-- Tampilan Default Detail Fasilitas Kamar -->
    <div id="default2" class="default2">
      <div class="panel panel-default">
        <div class="panel-heading" style="background-color: orange; color:white;">
          Detail Kamar                
        </div>
        <div class="panel-body">
          <div class="input-container">
            <p style="background-color:orange;color:white;padding:10px;width: 50%">Harga</p>
            <p style="background-color:#edebeb;color:orange;padding:10px;width: 50%">Rp. - </p>
          </div>

          <div class="input-container">
            <i class="fa fa-bed icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
              <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
          </div>


          <div class="input-container">
            <i class="fa fa-tshirt icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
              <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
          </div>

          <div class="input-container">
            <i class="fa fa-chair icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
              <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
          </div>

          <div class="input-container">
            <i class="fa icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;">AC</i>
              <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
          </div>

          <div class="input-container">
            <i class="fa fa-toilet icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
              <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
          </div>

          <div class="input-container">
            <i class="fa fa-bed icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;"></i>
              <i class="fa fa-minus icon" style="font-size: 20px; background-color: #d6d6d6; width: 50%;"></i>
          </div>

        </div>
      </div>
    </div>
    <!--  -->

    <!-- Tampilan Fasilitas Masing-Masing Kamar -->
    <div id="isihuni2" class="panel-body" style="display: none;">
     @foreach($data_kamar as $rowkamar)
      <div id="buka{{$rowkamar->id}}" class="myKamar">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: orange; color:white;">
            Detail Kamar
          </div>
          <div class="panel-body">

            <div class="input-container">
              <p style="background-color:orange;color:white;padding:10px;width: 50%">Harga</p>
              <p style="background-color:grey;color:white;padding:10px;width: 50%"><b>Rp. {{$rowkamar->harga}}</b></p>
            </div>

            @foreach($data_aset as $rowaset)
              @if($rowaset->idkamar==$rowkamar->id)
                  <div class="input-container">
                    <i class="fa icon" style="font-size: 20px; background-color: #a6a6a6; width: 50%;">{{$rowaset->nama}}</i>
                    <i class="fa fa-check icon" style="font-size: 20px; background-color: #1ED760; width: 50%;"></i>
                  </div>
              @endif
            @endforeach


              <div class="input-container">
                <span style=" padding: 20px; width: 100%; background: orange;">Tambah Fasilitas Kamar<a href="/aset"><i class="fa fa-plus icon" style="font-size: 20px; background-color: grey; width: 100%;"></i></a></span>
              </div>

              <div class="input-container">
                <span style=" padding: 20px; width: 100%; background: orange; width: 100%">
                  <center>Jadwal Pemeliharaan Aset</center>

                @foreach($data_aset as $rowaset)
                      @if($rowaset->idkamar==$rowkamar->id && $rowaset->jangkaservice!=null)
                       @if((strtotime("+7 day")>=strtotime('+'.$rowaset->jangkaservice.' months',strtotime($rowaset->waktuservis)))&&(strtotime("-7 days"))<=strtotime('+'.$rowaset->jangkaservice.' months',strtotime($rowaset->waktuservis)))
                       <div style="font-size: 100%; background-color: red; width: 100%; color: white; padding: 20px;">
                          <center>
                            @php $arrayservis = ['id'=>$rowaset->id]; @endphp
                            <form class="sudahservis_form" action="{{action('kamarcontroller@sudahservis',$arrayservis)}}" method="get">
                              {{csrf_field()}}
                              Saatnya Pemeliharan, Klik Disini Apabila Sudah <br>
                              {{strtoupper($rowaset->nama)}} - {{date('d F Y',strtotime("+".$rowaset->jangkaservice."  months",strtotime($rowaset->waktuservis)))}}
                              <button type="submit" style="width: 100%; margin-top: 10px;" class="btn btn-danger" onclick="" style="float: left; margin-left:4px;" >                      
                                Hapus <i class="fa fa-trash"></i>
                              </button>
                            </form>
                            ............................................
                          </center>
                        </div>
                       @else
                        <div style="font-size: 100%; background-color: grey; width: 100%; color: white; padding: 20px;">
                          <center>
                            {{strtoupper($rowaset->nama)}} - {{date('d F Y',strtotime("+".$rowaset->jangkaservice."  months",strtotime($rowaset->waktuservis)))}}
                          </center>
                        </div>
                        @endif
                      @endif
                @endforeach
 
                </span>
              </div>

              </div>
            </div>
          </div>
        @endforeach
    </div>
    <!-- Akhir Bagian Fasilitas Kamar -->
  </div>
</div>
<!-- Akhir dari detail kamar -->