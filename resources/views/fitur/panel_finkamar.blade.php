
    <div class="panel panel-teal">
          <div class="panel-body" style="background-color: rgb(208, 231, 216); min-height: 60vh;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" >
              <label>Silahkan Isi</label>
              <select id="tahunkamar" name="tahunkamar" required="" class="form-control" style="margin-top: 10px; background-color: #3e89d8; color: white;">
                <option id="piltahun" value="" selected="" disabled="">Pilih Tahun</option>
                <option class="form-control" value="2019">2019</option>
                <option class="form-control" value="2020">2020</option>
                <option class="form-control" value="2021">2021</option>
                <option class="form-control" value="2022">2022</option>
              </select>
              <select id="pilihankamar" name="pilihankamar" required="" class="form-control" style="margin-top: 10px; background-color: #3e89d8; color: white;">
                <option value="" selected="" disabled="">Pilih Kamar</option>
                <option class="form-control" value="all">Semua Kamar</option>
                  @if(!empty($data_kamar)) 
                      @foreach ($data_kamar as $rowkamar)
                          <option class="form-control" value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                      @endforeach
                  @else
                    <option id="kamarnotfound" value="" disabled selected="">Pilihan Kamar Tidak Ada</option>
                  @endif
                    </select>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" >
              <div class="scrollfield" id="result">
                <div class="row" id="nopilihkamar">
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div style="background-color:white;text-align:center;padding:10px 30px 10px 30px;">
                      <p style="font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em">
                          <b>Silahkan Masukkan tahun dan nama kamar yang ingin anda cari</b><br><br>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>



