<div id="menu960" class="stickykeuangan">
          <div id="Trigger" class="col-xs-6 col-sm-6 col-md-6 menudesc">
                <div class="col-md-12 no-padding">
                      <p>
                        {{$tot_kosong}} Kosong 
                      </p>
                 </div>

              <div id="Slider" class="slideup">
                    <!-- content has to be wrapped so that the padding and
                        margins don't effect the transition's height -->
                    <div id="Actual1" style="background-color:white;">
                        @foreach($data_kamar as $rowkamar)
                              @if($rowkamar->status=='kosong')
                                <center><h4 style="color:#ffb82b;">{{$rowkamar->namakamarkost}}</h4></center>
                              @endif
                        @endforeach
                    </div>
                </div>
          </div>
          <div id="Trigger2"  class="col-xs-6 col-sm-6 col-md-6 menudesc">
            <div class="col-md-12 no-padding">
                <div class="col-md-12 no-padding">
                      <p>
                        Rp.  <b> {{number_format($totaluang)}}</b>
                      </p>
                 </div>
             </div>

          <div id="Slider2" class="slideup">
                <div id="Actual2">
                    @foreach ($data_keuanganlimit as $rowuang)
                          @if($rowuang->statustransaksi=='sudah')
                            <center style="border-bottom: 3px solid 
                              @if($rowuang->pilihantransaksi=='pemasukkan')
                              green
                              @elseif($rowuang->pilihantransaksi=='pengeluaran')
                              red
                              @endif
                              ; background-color: white;
                              "><h3 style="color:white; ">
                              <span style="padding: 5px 6px 5px 6px; background-color: 
                              @if($rowuang->pilihantransaksi=='pemasukkan')
                              green
                              @elseif($rowuang->pilihantransaksi=='pengeluaran')
                              red
                              @endif
                              ;
                              " class="pull-left"> 
                              @if($rowuang->pilihantransaksi=='pemasukkan')
                                <i class="panah arrow up" style="border-color: white;"></i>  
                              @elseif($rowuang->pilihantransaksi=='pengeluaran')
                                <i class="panah arrow down" style="border-color: white;"></i>  
                              @endif
                            </span>
                            <font style="color:
                              @if($rowuang->pilihantransaksi=='pemasukkan')
                              green
                              @elseif($rowuang->pilihantransaksi=='pengeluaran')
                              red
                              @endif
                              ;">
                              {{$rowuang->nominaltransaksi}} 
                              </font><font style="color: gray; font-size: 12px;">{{$totaluang-=$rowuang->nominaltransaksi}}</font></h3></center>
                          @endif
                    @endforeach
                </div>
            </div>
          </div>
        </div>