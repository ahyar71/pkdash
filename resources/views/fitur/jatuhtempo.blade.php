

<div class="panel panel-default">
    <div class="panel-body" style="background-color: rgb(163, 230, 186); padding:40px;min-height: 60vh;">
      <div class="scrollfield">
          @php $count=0; @endphp

          
          @foreach($data_aset as $rowaset)
            @if((strtotime("+7 day")>=strtotime('+'.$rowaset->jangkaservice.' months',strtotime($rowaset->waktuservis)))&&(strtotime("-7 days"))<=strtotime('+'.$rowaset->jangkaservice.' months',strtotime($rowaset->waktuservis)))
              @php $count++; @endphp 
              <a href="/kamar"><div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>Waktunya Perawatan {{strtoupper($rowaset->nama)}} Kamar {{$rowaset->getNamaKamar()}}.<br> Segera Perbaharui Data Waktu Pemeliharaan Aset di Halaman Detail Kamar  {{$rowaset->getNamaKamar()}}</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> </a>
            @endif
          @endforeach

          @foreach($data_penghuni as $rowpenghuni)
            @if($rowpenghuni->idkamar!=$rowpenghuni->getIdKamarKeuangan()&&$rowpenghuni->tanggalkeluar==null)
            @php $count++; @endphp  
              <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowpenghuni->namapenghuni}}<b> (Kamar  {{$rowpenghuni->getKamarNama()}} ) <b>Belum Membayar DP, <br> Masukkan <b>Data Pembayaran DP </b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div>                           
            @elseif($rowpenghuni->idkamar==$rowpenghuni->getIdKamarKeuangan()&&$rowpenghuni->id!=$rowpenghuni->getIdPenghuniKeuangan()&&$rowpenghuni->getStatusTransaksi()==null&&$rowpenghuni->tanggalkeluar==null)
            @php $count++; @endphp  
            <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowpenghuni->namapenghuni}}<b> (Kamar   {{$rowpenghuni->getKamarNama()}}) <b>Belum Membayar DP, <br> Masukkan <b>Data Pembayaran DP </b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div>     
            @endif
          @endforeach

          @foreach ($databelumbayar as $rowuang)  
          @if($rowuang->statustransaksi=='dphangus')
            @if((strtotime("now")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-1 days")<=strtotime($rowuang->jatuhtempo))&&($rowuang->jenistransaksi=='lunas'))
            @php $count++; @endphp  
              <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) Pembayaran {{$rowuang->jenistransaksi}} batal, <br> Uang DP <b>Hangus</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> 
            @endif
          @endif

          @if($rowuang->statustransaksi=='batal')
            @if(strtotime("now")>=strtotime($rowuang->jatuhtempo)&&(strtotime("now")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-1 day")<=strtotime($rowuang->jatuhtempo))&&$rowuang->jenistransaksi=='bulanan')
            @php $count++; @endphp  
              <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} batal, <br> Penghuni Kos <b>Mengosongkan Kamar</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> 
            @endif
            @endif 
            @endforeach



          @foreach ($databelumbayar as $rowuang)    
          @if($rowuang->statustransaksi=='belumbayar')  
          
          
          @if((strtotime("-7 day")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-8 days")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp  
            <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Jatuh Tempo Lewat lebih dari 7 Hari (HARI TERAKHIR PERPANJANGAN, MALAM DIKELUARKAN)<br><b>DENDA 10%</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div>    

          @elseif((strtotime("-6 day")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-7 days")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp  
            <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Jatuh Tempo Lewat lebih dari 6 Hari (HARI TERAKHIR PERPANJANGAN, BESOK DIKELUARKAN)<br><b>DENDA 10%</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div>    

          @elseif((strtotime("-5 day")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-6 days")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp  
            <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Jatuh Tempo Lewat lebih dari 5 Hari <br><b>DENDA 10%</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div>    
  
          @elseif((strtotime("-4 day")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-5 days")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp  
            <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Jatuh Tempo Lewat lebih dari 4 Hari <br><b>DENDA 10%</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div>    
            
          @elseif((strtotime("-3 day")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-4 days")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp  
            <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Jatuh Tempo Lewat lebih dari 3 Hari <br><b>DENDA 10%</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div>      
            
          @elseif((strtotime("-2 day")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-3 day")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp 
            <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Jatuh Tempo! Lewat 2 Hari</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> 

          @elseif((strtotime("-1 day")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-2 day")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp 
            <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Jatuh Tempo! Lewat 1 Hari</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> 
    
          @elseif((strtotime("now")>=strtotime($rowuang->jatuhtempo))&&(strtotime("-1 day")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp 
            <div class="alert bg-danger" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Jatuh Tempo!</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> 


          @elseif((strtotime("+3 days")>=strtotime($rowuang->jatuhtempo))&&(strtotime("+2 days")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp                   
            <div class="alert bg-warning" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Akan Jatuh Tempo Dalam 3 Hari!</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> 
            
          @elseif((strtotime("+2 days")>=strtotime($rowuang->jatuhtempo))&&(strtotime("+1 day")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp  
            <div class="alert bg-warning" role="alert" style="padding: 20px;" ><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Akan Jatuh Tempo Dalam 2 Hari!</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> 

          @elseif((strtotime("+1 days")>=strtotime($rowuang->jatuhtempo))&&(strtotime("now")<=strtotime($rowuang->jatuhtempo)))
          @php $count++; @endphp  
            <div class="alert bg-warning" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>{{$rowuang->namapenghuni}}</b> (Kamar {{$rowuang->namakamarkost}}) <b>Pembayaran {{$rowuang->jenistransaksi}} Akan Jatuh Tempo Dalam 1 Hari!</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> 

            @endif
          @endif

          @endforeach


          @if($count==0)
              <div class="alert bg-success" role="alert" style="padding: 20px;"><center><em style="margin-bottom: 20px;" class="fa fa-lg fa-warning">&nbsp;</em><br> <b>Everything is OK</b><a href="#" data-dismiss="alert" class="pull-right"><em class="fa fa-lg fa-close"></em></a></center> </div> 
          @endif



    </div>
    </div>
</div>