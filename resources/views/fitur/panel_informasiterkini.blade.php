<div class="panel panel-default"> 
  <div class="panel-heading judul_panel_informasiterkini" style="background-color: #1ED760; border: 2px solid white; color:white;">
      <center><b>Informasi Terkini</b></center>
  </div>
  <div class="panel-body isi_panel_informasiterkini">
      <div class="col-md-12 no-padding">
        <!-- Total Uang Tunai Tersedia -->
          <div class="row progress-labels">
              <div class="col-sm-3">
                <img src="https://cdn-icons-png.flaticon.com/512/2953/2953363.png" alt="ikon">
              </div>
                <div class="col-sm-9">
                  <font color="white">
                    <b>Rp. {{number_format($totaluang)}}</b>
                  </font>
                  <br>
                  Uang Tunai Tersedia
                </div>
          </div>

          <!-- Total Penghuni Saat ini -->
          <div class="row progress-labels">
              <div class="col-sm-3">
                <img src="https://cdn-icons-png.flaticon.com/512/652/652051.png" alt="ikon">
              </div>
                <div class="col-sm-9">
                  <font color="white">
                    <b>{{$totalpenghuni}}</b>
                  </font>
                  <br>
                  Total Penghuni
                </div>
          </div>

           <!-- Total Kamar Saat ini -->
          <div class="row progress-labels">
              <div class="col-sm-3">
                <img src="https://cdn-icons-png.flaticon.com/512/3731/3731841.png" alt="ikon">
              </div>
                <div class="col-sm-9">
                  <font color="white">
                    <b>{{$totalkamar}}</b>
                  </font>
                  <br>
                  Total Kamar
                </div>
          </div>


           <!-- Total Penghuni yang Akan Jatuh Tempo (>H-3) Saat ini -->
          <div class="row progress-labels">
              <div class="col-sm-3">
                <img src="https://cdn-icons-png.flaticon.com/512/6897/6897039.png" alt="ikon">
              </div>
                <div class="col-sm-9">
                  <font color="white">
                    <b>{{$totalbelumbayar}}</b>
                  </font>
                  <br>
                  Penghuni Jatuh Tempo
                </div>
          </div>

          <!-- Total Jumlah Motor Saat ini untuk mengukur kepadatan parkiran-->
          <div class="row progress-labels">
              <div class="col-sm-3">
                <img src="https://cdn-icons-png.flaticon.com/512/1227/1227030.png" alt="ikon">
              </div>
                <div class="col-sm-9">
                  <font color="white">
                    <b>{{$totalmotor}}</b>
                  </font>
                  <br>
                  Total Motor
                </div>
          </div>

           <!-- Total Jumlah Mobil Saat ini untuk mengukur kepadatan parkiran-->
          <div class="row progress-labels">
              <div class="col-sm-3">
                <img src="https://cdn-icons-png.flaticon.com/512/846/846338.png" alt="ikon">
              </div>
                <div class="col-sm-9">
                  <font color="white">
                    <b>{{$totalmobil}}</b>
                  </font>
                  <br>
                  Total Mobil
                </div>
          </div>
        </div>
      </div>    
  </div>

   



