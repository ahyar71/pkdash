<!DOCTYPE html>

<html>

<head>

	<title>Hi</title>

</head>

<body style="background-color: #E3E3E3; border:2px solid black; padding:0px; margin:0px; ">

	<div style="text-align: right; color:white;"><small>Downloaded at {{date("d F Y h:i:s")}}</small></div>
	<center><h1 style="color:#a8a8a8">Laporan Keuangan</h1></center>	
	<hr style="border: 2px dotted orange">
	<div style="background-color: orange; width: 100%">
		<center><h2 style="color:white;">
		@if($dasar=='rentang')
			({{date("d F Y",strtotime($tanggalawal))}} Hingga {{date("d F Y",strtotime($tanggalakhir))}})
		@elseif($dasar='bulan')
			Bulan 
			@if($pilihanbulan==1)
			Januari
			@elseif($pilihanbulan==2)
			Februari
			@elseif($pilihanbulan==2)
			Februari
			@elseif($pilihanbulan==3)
			Maret
			@elseif($pilihanbulan==4)
			April
			@elseif($pilihanbulan==5)
			Mei
			@elseif($pilihanbulan==6)
			Juni
			@elseif($pilihanbulan==7)
			Juli
			@elseif($pilihanbulan==8)
			Agustus
			@elseif($pilihanbulan==9)
			September
			@elseif($pilihanbulan==10)
			Oktober
			@elseif($pilihanbulan==11)
			November
			@elseif($pilihanbulan==12)
			Desember
			@endif
			 {{$pilihantahun}}
		@endif
		</h2></center>
	</div>		
	<hr style="border: 2px dotted orange">

	<div class="scrollfield">                                
        <table style="margin-top: 10px; " width="100%"> 
            @php $count=0; $hari=0; @endphp
            @if(!empty($data_pencarian))                                   
              @foreach($data_pencarian as $rowcari)                         
                  @if($rowcari->statustransaksi=='sudah') 
                    @php $count++ @endphp     
			                @if(date("d",strtotime($rowcari->tanggaltransaksi))!=$hari)
			                  @php $hari = date("d",strtotime($rowcari->tanggaltransaksi)); @endphp
			                    <tr style=" border:2px solid orange; width: 100%; background-color: orange;">
			                      <td colspan="2">
			                        <h4 style=" color: white;">
			                          <center>
			                            {{date("d F Y",strtotime($rowcari->tanggaltransaksi))}}<br>
			                          </center>
			                        </h4>
			                      </td>
			                    </tr>
			                @endif
			                <tr style=" border:2px solid orange; ">
			                  <td width="40%" style="vertical-align: middle; background-color: 
			                @if($rowcari->pilihantransaksi=='pemasukkan' )
			                      #1ED760
			                @elseif($rowcari->pilihantransaksi=='pengeluaran')
			                      #f9243f
			                @endif
			                      ;">
			                      <center>
			                        <h3 style="padding-bottom: 10px;padding-left: 30px;">
			                          <p style=" color: white;"><b>
			                            <span style="font-size: 2.4vw;"> Rp. </span><span style="font-size: 3vw;"> {{number_format($rowcari->nominaltransaksi)}}</span></b>
			                          </p>
			                        </h3>
			                      </center>
			                      </td>
			                      <td style="color: 
					                @if($rowcari->pilihantransaksi=='pemasukkan' )
					                      #1ED760
					                @elseif($rowcari->pilihantransaksi=='pengeluaran')
					                      #f9243f
					                @endif
					                ">
			                        <h5 style="padding-left: 10px;">
			                          {{$rowcari->tanggaltransaksi}} <br>
			                          {{$rowcari->deskripsi}} | {{$rowcari->metode}}<br>
			                        </h5>
			                      </td>                              
			                    </tr>
			      @endif
			  @endforeach
			@elseif($count==0||$data_pencarian==null||empty($data_pencarian))
              <tr style=" border:2px solid orange; width: 100%; background-color: orange;">
                  <td colspan="2">
                    <h4 style=" color: white;">
                      <center>
                        Data Keuangan Tidak Ditemukan<br>
                      </center>
                    </h4>
                  </td>
                </tr>
        	@endif
		</table>


        @if($data_pencarian==null)
        <p>Data Tidak Ditemukan</p>
        @endif

      </div> <!-- scrollfield -->

</body>

</html>