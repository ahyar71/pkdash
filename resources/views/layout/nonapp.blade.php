<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-36871039-3">
    </script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-36871039-3');
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('judul')</title>
    <meta name="description" content="@yield('metadesc')" itemprop="description">
    <link rel="icon" href="{{ URL::asset('/uploads/iconweb.png')}}" type="image/x-icon">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/login.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/aos.css')}}">
    
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body onload="@yield('eventdibody')" style="padding: 0;">

@section('variabelphp')
@show

<!--HEADER START-->
    <nav class="navbar navbar-custom navbar-fixed-top" id="navigasistick" role="navigation" style="margin-bottom:0px;">
        <div class="container-fluid" style="padding-right: 0px;padding-left: 0px;margin-left: 0px;height: 0px;width: 100%;">
            <div class="navbar-header" >
                <a class="navbar-brand" href="/" ><span style="" ><b>Premiere</b></span>Kost</a>
                <div class="pull-right">
                    <div class="dropdown" id="menuHP">
                      <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="modal" data-target="#ModalMenu"  aria-expanded="false" style="border-radius: 50px;"><i class="fa fa-bars"></i>
                      </button>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </nav>

    <div class="col-sm-12 col-lg-12">
      <div class="modal fade" id="ModalMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: 2px dashed white">
              <center><h1 style="color:white;" ><b>Menu</b></h1></center>
            </div>
            <div class="modal-body">
              <a class="dropdown-item" href="/" style=" text-align: center; margin-bottom: 10px;  background-color: #ffd828;box-shadow:0px 15px 25px #ffd828;">
                <label style="font-size: 25px;">Homepage</label>
              </a>
              <a class="dropdown-item" href="/profil_page" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <label style="font-size: 25px;">Profil Kost</label>
              </a>
              <a class="dropdown-item" href="/#MapLocation" style=" text-align: center; margin-bottom: 10px;   background-color: #ff9028;box-shadow:0px 15px 25px #ff9028;">
                <label style="font-size: 25px;">Lokasi</label>
              </a>
              <a class="dropdown-item" href="/#SocialMedia" style=" text-align: center; margin-bottom: 10px;   background-color: #ff7b00;box-shadow:0px 15px 25px #ff7b00;">
                <label style="font-size: 25px;">Social Media</label>
              </a>
              <a class="dropdown-item" href="/faq_page" style=" text-align: center; margin-bottom: 10px;   background-color: #d46700;box-shadow:0px 15px 25px #d46700;">
                <label style="font-size: 25px;">FAQ</label>
              </a>
              <a class="dropdown-item" href="/et_page" style=" text-align: center; margin-bottom: 10px;   background-color: #b35701;box-shadow:0px 15px 25px #b35701;">
                <label style="font-size: 25px;">e-Tagihan</label>
              </a>
              <a class="dropdown-item" href="/#DevProfil" style=" text-align: center; margin-bottom: 10px;   background-color: #a55204;box-shadow:0px 15px 25px #a55204;">
                <label style="font-size: 25px;">Dev Profile</label>
              </a>
              </div>
          </div>
        </div>
      </div>
    </div>
        
        

<!--HEADER END-->

    @include('flash-message')

    @yield('content')


    @section('isibodi')
    @show


    @section('tambahan')
    @show

    @section('footer')
    @show

    <script>
    function teswa(){
      var eco = encodeURI(document.getElementById("komplain").value);
      window.open('https://api.whatsapp.com/send?phone=6281213073571&text='+eco);
    }
    </script>

    
</body>
</html>