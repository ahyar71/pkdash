<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('judul')</title>
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ URL::asset('/uploads/iconweb.png')}}" type="image/x-icon">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/normalize.css') }}" rel="stylesheet">
    <link href="{{ asset('css/component.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>

</head>
<body onload="@yield('eventdibody')">

@section('variabelphp')
@show

<!--HEADER START-->
    <nav class="navbar navbar-def navbar-fixed-top" role="navigation" style="
  border-bottom: 10px solid white;
  border-bottom-right-radius: 30px;
  border-bottom-left-radius: 30px; height: 70px;
  ">
        <div class="container-fluid" >
            <div class="navbar-header">
                <a class="navbar-brand" href="/"><span style="color:white;"><b>Premiere</b></span>Kost</a>
                <div class="pull-right">
                    <div class="dropdown" id="menuHP">
                      <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="modal" data-target="#ModalMenu"  aria-expanded="false" style="border-radius: 50px;"><i class="fa fa-bars"></i>
                      </button>

                      
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </nav>


    <div class="col-sm-12 col-lg-12" style=" margin-top: 60px;padding-left:0px;padding-right:0px;">
      <div class="modal fade" id="ModalMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="border-bottom: 2px dashed white">
                    <center><h1 style="color:white;" ><b>Halo {{$data_user->nama}}</b></h1></center>
                  </div>
              <div class="modal-body">
                        <a class="dropdown-item" href="/login" style=" text-align: center; margin-bottom: 10px;  background-color: #128039;"><label style="font-size: 25px;">Dashboard</label></a>
                        <a class="dropdown-item" href="/kamar" style=" text-align: center; margin-bottom: 10px;   background-color: #128039;"><label style="font-size: 25px;">Kamar</label></a>
                        <a class="dropdown-item" href="/penghuni" style=" text-align: center; margin-bottom: 10px;   background-color: #128039;"><label style="font-size: 25px;">Penghuni</label></a>
                        <a class="dropdown-item" href="/keuangan" style=" text-align: center; margin-bottom: 10px;   background-color: #128039;"><label style="font-size: 25px;">Keuangan</label></a>
                        <a class="dropdown-item" href="/aset" style=" text-align: center; margin-bottom: 10px;   background-color: #128039;"><label style="font-size: 25px;">Aset Kost</label></a><div class="divider"></div>
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#ModalAdu" style="background-color: orange; color:white; text-align: center;"><label style="font-size: 25px;">Pengaduan</label></a><div class="divider"></div>
                        <a class="dropdown-item" href="/logout" style="border: 2px dashed white; text-align: center; margin-bottom: 10px;   background-color: #1ED760;"><label>Logout</label></a>
              </div>
            </div>
          </div>
        </div>

      <div class="modal fade" id="ModalAdu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Trouble</b></h1></center>
                  </div>
              <div class="modal-body">
                      <!--Tombol Submit dan Batal-->
                      <span >
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                            <textarea class="komplain form-control" id="komplain" style="margin-bottom: 20px;"></textarea>
                            <button class="btn btn-lg btn-success" onclick="teswa()"  style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>  
            </div>
          </div>
        </div>
      </div>
        
        

<!--HEADER END-->

        @include('flash-message')

        @yield('content')


    @section('isibodi')
    @show


    @section('tambahan')
    @show

    @section('footer')
    @show

    <script>
    function teswa(){
      var eco = encodeURI(document.getElementById("komplain").value);
      window.open('https://api.whatsapp.com/send?phone=6281213073571&text='+eco);
    }
    </script>

    
</body>
</html>