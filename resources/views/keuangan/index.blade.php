@extends('layout.app')


@section('variabelphp')
@endsection

@section('judul','Data Keuangan')

@section('isibodi')		
		  

      <div id="menukecil" class="stickykeuangan" style="display: inline-block;">
          <div class="col-xs-4 col-sm-4 col-md-4" style="display: inline-block;">
                  <button class="btn-lg"  data-toggle="modal" data-target="#downloaddata" style="background-color: white;color:orange; border-radius:50px; width: 100%; border: 2px dashed white;">
                              <i class="fa fa-arrow-down"></i>
                  </button> 
          </div>
          <div class="col-xs-4 col-sm-4 col-md-4" style="display: inline-block;">            
                  <button class="btn-lg" data-toggle="modal" data-target="#tambahKeuangan" style="background-color: white;color:orange; border-radius:50px; width: 100%; padding: 20px; border: 2px dashed orange;">
                  <i class="fa fa-plus"></i>
                  </button> 
          </div>
          <div class="col-xs-4 col-sm-4 col-md-4" style="display: inline-block;">
                  <button class="btn-lg"  data-toggle="modal" data-target="#searchdata" style="background-color: white;color:orange; border-radius:50px; width: 100%; display: block; border: 2px dashed white;">
                  <i class="fa fa-search"></i>
                  </button> 
          </div>
      </div>

      
      
<div class="container" style="padding:0px;" >
			<div class="row">
            <div id="menubesar">
              <div class="col-xs-12 col-sm-12 col-md-4" style="padding:0px;" >
                <div class="panel panel-default" style="background-color: white;">
                  <button class="btn-lg"  data-toggle="modal" data-target="#downloaddata" style="background-color: white; border: 2px dashed white; color:orange; width: 100%;">
                  Download Laporan Keuangan
                  </button> 
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-4" style="padding:0px;" >
                <div class="panel panel-default" style="background-color: orange;">
                  <button class="btn-lg"  data-toggle="modal" data-target="#tambahKeuangan" style="background-color: orange;color:white; width: 100%; display: block; border: 2px dashed white;">
                  Tambah Data Keuangan
                  </button> 
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-4" style="padding:0px;" >
                <div class="panel panel-default" style="background-color: white;">
                  <button class="btn-lg"  data-toggle="modal" data-target="#searchdata" style="background-color: white; border: 2px dashed white; color:orange; border-top-left-radius: 20px;border-top-right-radius: 20px; width: 100%;">
                  Cari berdasarkan tanggal
                  </button> 
                </div>
              </div>      
            </div>
        
        
				<div class="col-xs-12 col-sm-12 col-md-4" style="padding:0px;">
          <div id="but_showall"  onclick="lihat_semua()" class="col-xs-12 col-sm-12 col-md-12" style="cursor:pointer;background-color: rgb(255, 194, 80); text-align:center; padding: 10px 10px 10px 10px; color:rgb(255, 255, 255);font-size:20px;display:none;">
            <b>Kembali</b>
          </div>
          <div id="but_rinciankeuangan"  onclick="lihat_rinciankeuangan()" class="col-xs-12 col-sm-12 col-md-12" style="cursor:pointer;background-color: #1ED760; text-align:center; padding: 30px 10px 30px 10px; color:white;font-size:20px;">
            <b>Rincian Keuangan</b>
          </div>
          <div id="but_jatuhtempo"  onclick="lihat_jatuhtempo()" class="col-xs-12 col-sm-12 col-md-12" style="cursor:pointer;background-color: rgb(163, 230, 186); text-align:center;padding: 30px 10px 30px 10px; color:white;font-size:20px;">
            <b>Jatuh Tempo</b>
          </div>
          <div id="but_panel_finkamar"  onclick="lihat_finkamar()" class="col-xs-12 col-sm-12 col-md-12" style="cursor:pointer;background-color: rgb(208, 231, 216); text-align:center;padding: 30px 10px 30px 10px; color:white;font-size:20px;">
            <b>Review Finansial Kamar</b>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-8" style="padding:0px;">
          <div id="pan_welcome" class="panel panel-teal col-xs-12 col-sm-12 col-md-12" style="padding:0px;">
            <div class="panel-body" style="background-color: rgb(255, 194, 80); min-height: 60vh;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: flex;justify-content: center;align-items: center;height: 40vh;text-align:center;">
                Hai pengelola kost! <br>
                Silahkan pilih kategori yang ingin kamu ketahui di menu hijau.
              </div>
            </div>
          </div>  

          <div id="pan_rinciankeuangan" class="col-xs-12 col-sm-12 col-md-12" style="padding:0px;display:none;">
            @include('fitur.rinciankeuangan')
          </div>
          <div id="pan_jatuhtempo" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;display:none;">
            @include('fitur.jatuhtempo')
          </div>      
          <div id="pan_panel_finkamar" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px;display:none;">
            @include('fitur.panel_finkamar')
          </div>
        </div>
			</div>

    </div>
	



@endsection

@section('tambahan')
	<!-- Modal -->
        <div class="modal fade" style="background-color: #1ED760;"  id="tambahKeuangan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <div class="modal-dialog" role="document" style="width: 100%;">
            <div class="modal-content" style="background-color: #1ED760; border:none; width: 100%;">
              <form id="tambah_keuangan" class="submit_form" action="{{action('KeuanganController@create')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Tambah Data Keuangan</b></h1></center>
                  </div>
                  <div class="modal-body">                  
    	              <div class="form-group">
    	                <label for="exampleInputEmail1" style="color:white;"><b>Tanggal Transaksi</b></label>
    	                <input required name="tanggaltransaksi" id="tanggaltransaksi" type="date" class="form-control" id="exampleInputEmail1" value="{{date('Y-m-d', strtotime('now'))}}" aria-describedby="emailHelp" >
    	              </div>
  	              <div class="form-group">
                      <label for="exampleInputEmail1" style="color:white;"><b>Pilihan Transaksi</b></label>
  	                <select name="pilihantransaksi"  id="pilihan" class="form-control" required style="color:gray; text-decoration: none;">
          						<option value="" selected disabled >Pilih Transaksi</option>
  	                	<option value="pemasukkan">Pemasukkan</option>
  	                	<option value="pengeluaran">Pengeluaran</option>
  	                </select>
  	              </div>
    		          <div class="form-group">
		              	<div id="pilihpemasukkan" class="divpilih" style="margin-bottom: 10px;">
                      <label for="exampleInputEmail1" style="color:white;"><b>Jenis Transaksi</b></label>
			                <select name="jenistransaksi" id="jenispemasukkan" class="form-control">
			                	<option value="" selected disabled>Pilih Transaksi</option>
			                	<option value="dp">Pembayaran DP</option>
			                	<option value="jenislunas">Pembayaran Lunas</option>
			                	<option value="bulanan">Pembayaran Bulanan</option>
			                	<option value="tambahan">Extend / Harian / Mingguan</option>
                        <option value="lain">Lainnya</option>
			                </select>			 
		          		  </div>
  		          		<div id="pilihpengeluaran" class="divpilih" style="margin-bottom: 10px;">
                      <label for="exampleInputEmail1" style="color:white;"><b>Jenis Transaksi</b></label>
  			                <select name="jenistransaksi" id="jenispengeluaran" class="form-control" >
  			                	<option value="" selected disabled>Pilih Transaksi</option>
  			                	<option value="listrik">Listrik</option>
  			                	<option value="air">Air</option>
  			                	<option value="gaji">Gaji Penjaga</option>
  			                	<option value="pemeliharaan">Pemeliharaan Kos</option>
  			                	<option value="lain">Lain-lain</option>
  			                </select>
  		            	</div>
  	                <div  id="showdp" class="myDP">
  		                <select name="listdp" id="listdp" class="form-control">
  		                	<option value="" selected disabled>Pilih Penghuni</option>
  		                	@foreach($data_penghuni as $rowpenghuni)            
  			                	@if($rowpenghuni->idkamar!=$rowpenghuni->getIdKamarKeuangan())
  			                		<option value="{{$rowpenghuni->id}}">
  			                			{{$rowpenghuni->id}} {{$rowpenghuni->namapenghuni}} (Rp. {{number_format($rowpenghuni->getHargaKamar())}}/bulan)
  			                		</option>
  			                	@elseif($rowpenghuni->idkamar==$rowpenghuni->getIdKamarKeuangan()&&$rowpenghuni->id!=$rowpenghuni->getIdPenghuniKeuangan()&&$rowpenghuni->getStatusTransaksi()==null)
  			                	<option value="{{$rowpenghuni->id}}">
                            {{$rowpenghuni->id}} {{$rowpenghuni->namapenghuni}} (Rp. {{number_format($rowpenghuni->getHargaKamar())}}/bulan)
  			                		</option>
  			                	@endif
  			                @endforeach
  		                </select>
  		                <small style="color:red;">DP Rp. 200.000 (3 Hari), DP > Rp. 200.000 - 800.000 (7 Hari)</small>
    	            	</div>
  		            	<div  id="showjenislunas" class="myjenisLunas"  style="margin-bottom: 10px;">
  			                <select name="jenislunas" id="jenislunas" class="form-control" >
  			                	<option value="" selected disabled>Pilih Jenis Pelunasan</option>
  			                	<option value="lunas">Sudah DP</option>
  			                	<option value="lunaslangsung">Lunas Langsung</option>
  			                </select>
  		            	</div>

  	                <div id="showlunaslangsung" class="mylangsungLunas">
  		                <select name="listlangsunglunas" id="showselectlangsunglunas" class="form-control">
  		                	<option value="" selected disabled>Pilih Penghuni</option>
  		                	@foreach($data_penghuni as $rowpenghuni)
  			                	@if($rowpenghuni->idkamar!=$rowpenghuni->getIdKamarKeuangan())
  			                		<option value="{{$rowpenghuni->id}}">
  			                			{{$rowpenghuni->namapenghuni}} (Rp. {{number_format($rowpenghuni->getHargaKamar())}}/bulan)
  			                		</option>
  			                	@elseif($rowpenghuni->idkamar==$rowpenghuni->getIdKamarKeuangan()&&$rowpenghuni->id!=$rowpenghuni->getIdPenghuniKeuangan()&&$rowpenghuni->getStatusTransaksi()==null)
  			                	<option value="{{$rowpenghuni->id}}">
  			                			{{$rowpenghuni->namapenghuni}} (Rp. {{number_format($rowpenghuni->getHargaKamar())}}/bulan)
  			                		</option>
  			                	@endif
  			                @endforeach
  		                </select>
    	            	</div>
  		                <div  id="showlunas" class="myLunas">
  			                <select id="showselectlunas" name="listlunas" class="form-control">
  				                	<option value="" selected disabled>Pilih Penghuni</option>
  				                	@php $param='';@endphp
  				                	@foreach($data_keuangan as $rowkeuangan)
  						                	@if($rowkeuangan->jenistransaksi=='lunas' && $rowkeuangan->statustransaksi=='belumbayar')
  						                		 @php $param = $rowkeuangan->idpenghuni; @endphp
  							                	<option value="{{$rowkeuangan->id}}">
  									              		{{$rowkeuangan->getNamaPenghuni()}} Kurang Rp. 
  									             		{{number_format($rowkeuangan->nominaltransaksi)}} <br>
  								                </option>
  						                	@endif
  				                	@endforeach
  			                </select>
  		            	 </div>
  		                <div id="showbulanan" class="myBulanan">
  			                <select id="showselectbulanan" name="listbulanan" class="form-control" >
  				                	<option value="" selected disabled>Pilih Penghuni</option>
  				                	@foreach($data_keuangan as $rowkeuangan)
  					                	@if($rowkeuangan->jenistransaksi=='bulanan' && $rowkeuangan->statustransaksi=='belumbayar')
  						                		<option value="{{$rowkeuangan->id}}">
  							                		{{$rowkeuangan->getNamaPenghuni()}} (Rp. {{number_format($rowkeuangan->getHargaKamar())}}/bulan + Denda {{number_format($rowkeuangan->nominaltambahan)}}) jatuh tempo {{date("d-F-Y",strtotime($rowkeuangan->jatuhtempo))}}
  						                		</option>					                		
  					                	@endif
  				                	@endforeach
  			                </select>		      
    		            	</div>
  		            	   <div id="showtambahan" class="myTambahan">
    			                <select id="showselecttambahan" name="listtambahan" class="form-control" >
                              <option value="" selected disabled>Pilih Penghuni</option>
                              
                              @foreach($data_penghuni as $rowpenghuni)
                                @if($rowpenghuni->idkamar==$rowpenghuni->getIdKamarKeuangan()&&$rowpenghuni->id!=$rowpenghuni->getIdPenghuniKeuangan()&&$rowpenghuni->getStatusTransaksi()==null)
                                <option value="{{"baru|".$rowpenghuni->id}}">
                                    {{$rowpenghuni->namapenghuni}} (Rp. {{number_format($rowpenghuni->getHargaKamar())}}/bulan)
                                  </option>
                                @endif      
                              @endforeach

    				                	@foreach($data_keuangan as $rowkeuangan)
    					                	@if($rowkeuangan->jenistransaksi=='bulanan' && $rowkeuangan->statustransaksi=='belumbayar')
    						                		<option value="{{"lama|".$rowkeuangan->id}}">
    							                		{{$rowkeuangan->getNamaPenghuni()}} ( Jatuh tempo {{date("d-F-Y",strtotime($rowkeuangan->jatuhtempo))}} )
                                    </option>		 
    					                	@endif
                              @endforeach
                                       		
    			               </select>
    			               <select id="showselecttambahanhari" name="lamahari" class="form-control"  style="margin-top:10px;">
    				               	<option value="" selected disabled>Pilih Hari</option>
    			                	@for ($i = 1; $i <= 30; $i++)
      								        <option value="{{$i}}">{{$i}} Hari</option>
            								@endfor
    			               </select>   
  		            	   </div>
    		            	<div id="lama" class="myLama form-group">
    			                <br>
                          <label for="exampleInputEmail1" style="color:white;"><b>Tipe Penghuni</b></label>
    			                <select id="tipepenghuni" name="tipe" class="form-control">
    				                	<option value="" selected disabled>Pilih Tipe Penghuni</option>
    				                	<option value="sendiri">Sendiri</option>
    				                	<option value="pasutri">Suami Istri (+ Rp. 200.000)</option>
    				                	<option value="teman">Teman (+ Rp. 200.000)</option>
    				                	<option value="adikkakak">Adik Kakak (+ Rp. 200.000)</option>
    			                </select>
    			                <br>
    	                		<label for="exampleInputEmail1">Lama Sewa</label>
    	                		<input name="lamasewa" type="number" class="form-control" id="lamasewa" aria-describedby="emailHelp" placeholder="Lama Sewa (Bulan)">
    		            	</div>

                    <div class="form-group" id="tempo">
                      <label for="exampleInputEmail1" style="color:white;"><b>Tanggal Tempo</b></label>
                      <input name="tanggaltempo" id="tanggaltempo" type="date" min="" class="form-control" value="{{date('Y-m-d', strtotime('+1 month',strtotime('now')))}}" aria-describedby="emailHelp">
                    </div>
                    <!-- ------------------------------------------------------------- -->
    		          </div>
  	              <div class="form-group">
                      <label for="exampleInputEmail1" style="color:white;"><b>Nominal Transaksi</b></label>
                      <input required id="nominaltransaksi" name="nominaltransaksi" type="number" class="form-control" aria-describedby="emailHelp" placeholder="Nominal Transaksi">
                      <div id="warningnominal">
                      </div>
  	              </div>  	              
  	              <div class="form-group">
                      <label for="exampleInputEmail1" style="color:white;"><b>Sumber Pembayaran</b></label>
  		                <select id="metodetransaksi" name="metode" class="form-control" style="color:gray">
  		                	<option value="" selected disabled>Pilih Metode</option>
  		                	<option value="transfer" >Transfer</option>
  		                	<option value="tunai" >Tunai</option>
  		                </select>
  	              </div>  
  	              <div class="form-group">
                      <label for="exampleInputEmail1" style="color:white;"><b>Keterangan Transaksi</b></label>
  		                <input id="keterangantransaksi" required name="keterangantransaksi" type="text" class="form-control" aria-describedby="emailHelp" placeholder="Keterangan Transaksi">
  	              </div>       
                  </div>
                  <span>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                      <center>
                      <button class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                  </center>
                  </div>
                    <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                    <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                    </div>
                  </span>   
            	</form>
            </div>
          </div>
        </div>


        <div class="modal fade" id="downloaddata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          <div class="modal-dialog" role="document">
            <div class="modal-content">
            <form  id="formdownload" action="{{action('KeuanganController@downloadfile')}}" method="post" enctype="multipart/form-data">
             {{csrf_field()}}
              <div class="modal-header">
                <center><h1 style="color:white;" class="modal-title" id="exampleModalLabel"><b>Financial Download</b></h1></center>  
              </div>
              <div class="modal-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Download Berdasarkan...</label>
                    <select required class="form-control" id="dasar" name="dasar">
                      <option selected value="" disabled >Pilih Opsi</option>
                      <option value="rentang">Rentang Tanggal</option>
                      <option value="bulan">Bulan</option>
                    </select>
                  </div>

                  <div class="myRentang" id="showrentang">
                    <div class="form-group" >
                      <label for="exampleInputEmail1">Dari</label>
                        <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Awal" name="from" style="margin-bottom: 10px;">   
                      <label for="exampleInputEmail1">Ke</label>
                        <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Akhir" name="to">
                    </div>
                  </div>


                  <div class="myBulan" id="showbulan">
                    <div class="form-group">
                      <select name="pilihanbulan" class="form-control" >          
                        <option value="" selected disabled>Pilih Bulan</option>
                        <option value="1">Januari</option>  
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>  
                        <option value="4">April</option>  
                        <option value="5">Mei</option>  
                        <option value="6">Juni</option>  
                        <option value="7">Juli</option>  
                        <option value="8">Agustus</option>  
                        <option value="9">September</option>  
                        <option value="10">Oktober</option>  
                        <option value="11">November</option>  
                        <option value="12">Desember</option>  
                      </select>
                      <select name="pilihantahun" class="form-control" style="margin-bottom: 30px;" >                    
                        <option value="" selected disabled>Pilih Tahun</option>
                        @php $tahunawal = 2019; @endphp
                        @foreach (range(date('Y'), $tahunawal) as $x)
                        <option value="{{$x}}">{{$x}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>  

                  <span >
                    <div class="col-sm-12 col-lg-12 col-md-12">
                      <center>         
                      <button class="btn btn-lg btn-success" style="margin-bottom: 30px; margin-top: 20px;  width: 100%; padding: 20px; background-color: orange;">Download</button>
                  </center>
                  </div>
                    <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                    <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                    </div>
                  </span>   
              </div> 
            </form>
          </div>
        </div>
    	</div>

      <div class="modal fade" id="searchdata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <center><h1 style="color:white;" class="modal-title" id="exampleModalLabel"><b>Financial Search</b></h1></center>  
              </div>
              <div class="modal-body">
              <form action="{{'\caritanggalpenjaga'}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}} 
                  <label>Dari</label>
                  <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Awal" name="from" required style="margin-bottom: 10px;"> 
                  <label>Ke</label>
                  <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Akhir" name="to" required>                    
                      <button class="btn btn-lg btn-success" style="margin-bottom: 30px; margin-top: 20px;  width: 100%; padding: 20px; background-color: orange;">Search</button>
                    <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
              </form>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('footer')

    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>



<script>
    
    $(document).ready(function(){
    var demovalue2 = 'a';
    $("div.myRentang").hide();
    $("div.myBulan").hide();
      $('#dasar').on('change', function(){
        var demovalue = $(this).val();
            if(demovalue2=='a'){
              demovalue2 = demovalue; 
            }
            if(demovalue2!=demovalue){
                  $("#show"+demovalue2).hide();
                  $("#show"+demovalue).show();
                demovalue2 = demovalue;
            }
            if(demovalue2==demovalue){
                  $("#show"+demovalue).show();
                demovalue2 = demovalue;
            }
          }
      );

      $('#dasar').on('submit', function(){
              $('form > input > display:none').attr("disabled",true);
              $('form').submit();
      }
      );      
    });

  // pilihan transaksi untuk memunculkan jenis transaksi pemasukkan belum
  $(document).ready(function(){

    // Menampilkan Layout Sesuai Ukuran Layar
    if ($(window).width() < 960) {
      $('#menubesar').hide();
      $('#menukecil').show();
    }else {
      $('#menukecil').hide();
      $('#menubesar').show();
    }

    var temp2 = 'a';
    var par2 = 'a';
    $("div.myDP").hide();
    $("div.myBulanan").hide();
    $("div.myLunas").hide();
    $("div.myTambahan").hide();
    $("div.myLama").hide();
    $("div.myjenisLunas").hide();
    $("div.mylangsungLunas").hide();
    $("div.divpilih").hide();
    $("#tempo").hide();
    $("#masuk").hide();       

    // Menampilkan Pilihan Transaksi (Pemasukan atau Pengeluaran)
    $('#pilihan').on('change', function(){
    	$("div.myTambahan").hide();
      $("#tempo").hide();
	    $("div.myDP").hide();
	    $("div.myBulanan").hide();
	    $("div.myLunas").hide();
	    $("div.myLama").hide();
	    $("div.mylangsungLunas").hide();
	    $("div.myjenisLunas").hide();
        var par = $(this).val();
            if(par2=='a'){
              par2 = par; 
            }
            if(par2!=par){
                  $("#pilih"+par2).hide();
        		$("#jenis"+par2).prop('required',false); 
                  $("#pilih"+par).show();
        		$("#jenis"+par).prop('required',true); 
                par2 = par;
            }
            if(par2==par){
                  $("#pilih"+par).show();
        		$("#pilih"+par).prop('required',true); 
                par2 = par;
            }
    });
    // Menampilkan Pilihan Jenis Pemasukkan
    $('#jenispemasukkan').on('change', function(){     
      $("#masuk").hide();     
      $("#tempo").hide();
      $("div.myTambahan").hide();
      $("div.myDP").hide();
      $("div.myBulanan").hide();
      $("div.myLunas").hide();
      $("div.myLama").hide();
      $("#tempo").hide();
      $("div.mylangsungLunas").hide();
        var temp = $(this).val(); 
          if(temp2=='a'){
                temp2 = temp; 
          }
          if(temp2!=temp){
                $("#show"+temp2).hide();
                $("#showselect"+temp2).prop('required',false);
                $("#show"+temp).show();
                $("#showselect"+temp).prop('required',true);
                temp2 = temp;
          }
          if(temp2==temp){
                $("#show"+temp).show();
                $("#showselect"+temp).prop('required',true);
                temp2 = temp;
          }
          if($(this).val()=='dp'||$(this).val()=='bulanan'){		            	
                $("#lama").show();
                $("#lama").prop('required',true);  
                $("#tempo").show(); 
          }
          if($(this).val()=='bulanan'){         	
                $("#lama").show();   
                $("#lama").prop('required',true);
                $("#tempo").show();
        }
    });
    // Menampilkan Jenis Pelunasan (Pelunasan dengan DP, atau Langsung lunas tanpa DP)
    $('#jenislunas').on('change', function(){  
      $("#masuk").hide();     
      $("#tempo").hide();
      $("div.myTambahan").hide();
      $("div.myDP").hide();
      $("div.myBulanan").hide();
      $("div.myLunas").hide();
      $("div.mylangsungLunas").hide();
        var par = $(this).val();
        if(par=='lunas'){
          $("#lama").hide();   
          $("#lama").prop('required',false);  
          $("#tempo").show();
        }
        if(par=='lunaslangsung'){
          $("#lama").show();
          $("#lama").prop('required',true);  
          $("#masuk").show();       
          $("#masuk").prop('required',true); 
          $("#tempo").show();
        }
            if(par2=='a'){
              par2 = par; 
            }
            if(par2!=par){
                  $("#show"+par2).hide();
                  $("#showselect"+par2).prop('required',false);
                  $("#show"+par).show();
                  $("#showselect"+par).prop('required',true);
                par2 = par;
            }
            if(par2==par){
                  $("#showselect"+par).show();
                  $("#showselect"+par).prop('required',true);
                par2 = par;
            }
    });

    

    // Menampilkan Form Validasi Sebelum Submit
    $('.submit_form').on('submit',function(e){
      // FIELD TANGGAL TRANSAKSI
      var tanggaltransaksi = document.getElementById('tanggaltransaksi').value;
      // FIELD PILIHAN TRANSAKSI
      var pilihan = document.getElementById('pilihan').value;
      // FIELD JENIS TRANSAKSI
      var pngl = document.getElementById('jenispengeluaran');
      var pmsk = document.getElementById('jenispemasukkan');
      // FIELD DETAIL TRANSAKSI
      var ldp = document.getElementById('listdp');
      var pts = document.getElementById('showselecttambahan');
      var lts = document.getElementById('showselecttambahanhari');
      var jlns = document.getElementById('jenislunas');
      var llns = document.getElementById('showselectlangsunglunas');
      var sslns = document.getElementById('showselectlunas');
      var sbl = document.getElementById('showselectbulanan');
      var tpp = document.getElementById('tipepenghuni');

      // PENGURAIAN
      var pengeluaran = pngl.options[pngl.selectedIndex].text;
      var pemasukkan = pmsk.options[pmsk.selectedIndex].text;
      var valuepemasukkan = pmsk.options[pmsk.selectedIndex].value;
      var penghuni_tambahdp = ldp.options[ldp.selectedIndex].text;
      var penghuni_tambahsewa = pts.options[pts.selectedIndex].text;
      var lama_tambahsewatambahan = lts.options[lts.selectedIndex].text;
      var jenis_lunas = jlns.options[jlns.selectedIndex].value;
      var penghuni_langsunglunas = llns.options[llns.selectedIndex].text;
      var penghuni_tambahlunas = sslns.options[sslns.selectedIndex].text;
      var penghuni_tambahbulanan = sbl.options[sbl.selectedIndex].text;
      var tipepenghuni = tpp.options[tpp.selectedIndex].text;
      var tanggaltempo = document.getElementById('tanggaltempo').value;
      var lamasewa = document.getElementById('lamasewa').value;
      
      var detailtransaksi = '-';
      // DETAIL TRANSAKSI 
      if(valuepemasukkan=='dp'){
        detailtransaksi ="Penghuni : "+penghuni_tambahdp+" \n Tipe : "+tipepenghuni+" \n Lama Sewa : "+lamasewa+" Bulan \n Tanggal Tempo Berikutnya : "+tanggaltempo;
      }else if(valuepemasukkan=='jenislunas'){
        if(jenis_lunas=='lunas'){
          detailtransaksi ="Penghuni : "+penghuni_tambahlunas+"\n Tanggal Tempo : "+tanggaltempo;
        }else if(jenis_lunas=='lunaslangsung'){
          detailtransaksi ="Detail : "
        +"\n ------------------------------------------- \n "+penghuni_langsunglunas+
        +"\n ------------------------------------------- \n Tipe : "+tipepenghuni+" \n Lama Sewa : "+lamasewa+" Bulan \n Tanggal Tempo Berikutnya : "+tanggaltempo;
        }
      }else if(valuepemasukkan=='bulanan'){
        detailtransaksi ="Detail : "
        +"\n ------------------------------------------- \n "+penghuni_tambahbulanan
        +"\n ------------------------------------------- \n Tipe : "+tipepenghuni+" \n Lama Sewa : "+lamasewa+" Bulan \n Tanggal Tempo Berikutnya : "+tanggaltempo;
      }else if(valuepemasukkan=='tambahan'){
        detailtransaksi = "Detail : "
        +"\n ------------------------------------------- \n "+penghuni_tambahsewa
        +"\n ------------------------------------------- \n  - Selama "+lama_tambahsewatambahan+" Hari";
      }
      // FIELD UMUM
      var nominaltransaksi = document.getElementById('nominaltransaksi').value;
      var metodetransaksi = document.getElementById('metodetransaksi').value;
      var keterangantransaksi = document.getElementById('keterangantransaksi').value;

      if(pilihan=='pengeluaran'){
        var pass = prompt("Anda akan submit data "
        +"\n -------------------------------------------"
        +"\n Pengeluaran : "+pengeluaran
        +"\n -------------------------------------------"
        +"\n Tanggal Transaksi : "+tanggaltransaksi
        +"\n -------------------------------------------"
        +"\n Sebesar : "+nominaltransaksi
        +"\n Metode : "+metodetransaksi
        +"\n Keterangan : "+keterangantransaksi
        +"\n -------------------------------------------"
        +"\n Silahkan Ketik \"SAYA YAKIN\" : ", "");
      }else if(pilihan=='pemasukkan'){
        var pass = prompt("Anda akan submit data "
        +"\n -------------------------------------------"
        +"\n Pemasukkan : "+pemasukkan
        +"\n -------------------------------------------"
        +"\n Tanggal Transaksi : "+tanggaltransaksi
        +"\n -------------------------------------------"
        +"\n "+detailtransaksi
        +"\n Sebesar : "+nominaltransaksi
        +"\n Metode : "+metodetransaksi
        +"\n Keterangan : "+keterangantransaksi
        +"\n -------------------------------------------"
        +"\n Silahkan Ketik \"SAYA YAKIN\" : ", "");
      }
      if (pass == 'SAYA YAKIN') {
        return true;
      } else {
        return false;
      }
    });

  });
  
  var start = 10;
  // Rincian Keuangan Fetch
  //  $('.scrollfield').scroll(function() {
     
  //     if($(this)[0].scrollHeight - $(this).scrollTop() == $(this).height() ||$(this)[0].scrollHeight - $(this).scrollTop() < $(this).height()+50) {
  //       fetchRincian(start);
  //       start += 10;
  //     }
  //    });

   // Disable Tanggal yang Telah Lampau (Jatuh Tempo) MASIH BERMASALAH
   $(function(){
        var tanggalnow = new Date();
        var bulan = tanggalnow.getMonth() + 1;
        var hari = tanggalnow.getDate() - 7;
        var tahun = tanggalnow.getFullYear();
        if(hari == 0){
          bulan -= 1;
          if(bulan==0){
            tahun-=1;
          }
        }
        if(bulan < 10)
            bulan = '0' + bulan.toString();
        if(hari < 10)
            hari = '0' + hari.toString();
        
        var tanggalmaks = tahun + '-' + bulan + '-' + hari;
        $("input[name='tanggaltempo']").attr('min', tanggalmaks);
    });

   // Disable Tanggal yang Telah Lampau (Tanggal Transaksi)
   $(function(){
        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate() - 7;
        var year = dtToday.getFullYear();
        if(day == 0){
          month -= 1;
          if(month==0){
            year-=1;
          }
        }
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        
        var maxDate = year + '-' + month + '-' + day;
        $('#tanggaltransaksi').attr('min', maxDate);
    });

</script>

<script type="text/javascript">

    $(document).ready(function(){
    
    //
    $('#lamasewa').on('input', function() {
      var lamasewa = this.value;
      var jenispenghuni = $('#tipepenghuni').val();
      if($('#jenispemasukkan').val()=='bulanan'){
        var idkeuangan = $('#showselectbulanan').val();
        fetchNominalBulanan(idkeuangan,lamasewa,jenispenghuni);
      }else if($('#jenispemasukkan').val()=='jenislunas'&&$('#jenislunas').val()=='lunaslangsung'){
        var idpenghuni = $('#showselectlangsunglunas').val();
        fetchNominalLangsungLunas(idpenghuni,lamasewa,jenispenghuni);
      }
    });

    $('#showselectlunas').on('change', function() {
      if($('#jenispemasukkan').val()=='jenislunas'&&$('#jenislunas').val()=='lunas'){
          var idkeuangan = this.value;
          fetchNominalPelunasan(idkeuangan);
        }
    });
    
    $('#showselectbulanan').on('change', function() {
          var idkeuanganbulanan = this.value;
          fetchWarningTempo(idkeuanganbulanan);
    });
    //Mengambil data keuangan kamar
    $('#nopilihkamar').show();
    $('#pilihankamar').hide();
    $('#tahunkamar').change(function(){
      $('#pilihankamar').show();
    });
    $('#pilihankamar').change(function(){
      
      $('#nopilihkamar').hide();

      var idkamarpilihan = this.value;
      var tahun = $('#tahunkamar').val();
      var idkosan = {!! json_encode(Auth::user()->idkost) !!};

      if(idkamarpilihan != '' && tahun !=''){

          fetchRecords(idkamarpilihan,tahun,idkosan);
          $('#result').show();
          var tr_strer = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                    "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                      "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<b>Mengambil Data ....</b><br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";

                $("#result").empty();
                $("#result").append(tr_strer);
        

      }else{
        var tr_strer = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                    "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                      "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<b>Silahkan masukkan username dan tag kamu, silahkan bertanya ke penjaga kos jika belum mengetahuinya :)</b><br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";

                $("#result").empty();
                $("#result").append(tr_strer);
      }

    });
  });

  //Mengambil Record Berdasarkan ID Kamar dan Tahun Data Keuangan
  function fetchRecords(idkamarpilihan,tahun,idkosan){
       $.ajax({
         url: 'getFinKamar',
         type: 'get',
         dataType: 'json',
         data: { 
            idkamarpilihan: idkamarpilihan,
            tahun: tahun,
            idkosan: idkosan
          },
         success: function(response){
           var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

           if(response['jumlahdata']!='0'){
            var len = 0;
            var fin = 0;
            if(response['datafinkamar'] != null){
              len = response['datafinkamar'].length;
            }

            if(len > 0){
              
                $("#result").empty();
              var totaltransaksi = 0;
              for(var i=0; i<len; i++){
                var transaksi = '';
                var idkost = response['authid'];
                var jatuhtempo = response['datafinkamar'][i].jatuhtempo;
                var jenistransaksi = response['datafinkamar'][i].jenistransaksi;
                var namapenghuni = response['datafinkamar'][i].namapenghuni;
                var namakamarkost = response['datafinkamar'][i].namakamarkost;
                var nominaltransaksi = parseInt(response['datafinkamar'][i].nominaltransaksi);
                
                var date_transaksi = new Date(jatuhtempo);
                totaltransaksi += nominaltransaksi;
                if(jenistransaksi=='bulanan'){
                  var transaksi = 'Pembayaran Bulanan';
                }else if(jenistransaksi=='lunas'){
                  var transaksi = 'Pembayaran Pelunasan DP';
                }else if(jenistransaksi=='lunaslangsung'){
                  var transaksi = 'Pembayaran Langsung Lunas';
                }else if(jenistransaksi=='dp'){
                  var transaksi = 'Pembayaran DP';
                }else{
                  var transaksi = 'Lainnya';
                }
                
                var tr_strsu = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\" text-align: center; position: relative;\">"+
                    "<div style=\"background-color:white;text-align:center;padding:10px 30px 10px 30px;\">"+
                      "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<div class=\"alert bg-warning\" role=\"alert\" style=\"padding: 20px;\">"+
                          "<b>"+transaksi+"<br> Bulan "+months[date_transaksi.getMonth()]+"<br>"+namapenghuni+"<br> Kamar "+namakamarkost+"</b><br>"+
                          "<b>"+jatuhtempo+"</b><br>Rp."+nominaltransaksi.toLocaleString()+" <br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";
              
                $("#result").append(tr_strsu);
              }
              
                var tr_strsutotal = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\" text-align: center; position: relative;\">"+
                    "<div style=\"background-color:white;text-align:center;padding:10px 30px 10px 30px;\">"+
                       "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<div class=\"alert bg-success\" role=\"alert\" style=\"padding: 20px;\">"+
                          "<center><em style=\"margin-bottom: 20px;\" class=\"fa fa-lg fa-money\">&nbsp;</em><br>" +
                          "<b>Total Rp. "+totaltransaksi.toLocaleString()+"</b><br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";
                
                $("#result").append(tr_strsutotal);
            }
               
              
           }else{
             var tr_strnodata = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                    "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                      "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<b>Data Keuangan di tahun yang dipilih Tidak Ditemukan</b><br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";
                
                $("#result").empty();
                $("#result").append(tr_strnodata);
           }
         },
         error: function(response){
            if (response['datafinkamar'] === undefined || response['datafinkamar'].length == 0 || response['jumlahdata'] == '0' ) {
              var tr_strerror = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                    "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                      "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<b>Data Keuangan di tahun yang dipilih Tidak Ditemukan</b><br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";
                
                $("#result").empty();
                $("#result").append(tr_strerror);
            }
         }
       });
  }

  function fetchNominalBulanan(idkeuangan,lamasewa,jenispenghuni){
       $.ajax({
         url: 'getNominalBulanan',
         type: 'get',
         dataType: 'json',
         data: { 
          idkeuangan: idkeuangan,
          lamasewa: lamasewa,
          jenispenghuni: jenispenghuni
          },
         success: function(response){
           if(response['jumlahdata']!='0'){
                $("input#nominaltransaksi").val(response['total']);
            }else{
              $("input#nominaltransaksi").val('Silahkan periksa input penghuni, lamasewa, dan jenis penghuni');
            } 
         },
         error: function(response){
          $("input#nominaltransaksi").val('Silahkan periksa input penghuni, lamasewa, dan jenis penghuni');
         }
        });
  }

  function fetchNominalPelunasan(idkeuangan){
       $.ajax({
         url: 'getNominalPelunasan',
         type: 'get',
         dataType: 'json',
         data: { 
          idkeuangan: idkeuangan
          },
         success: function(response){
           if(response['jumlahdata']!='0'){
                $("input#nominaltransaksi").val(response['total']);
            }else{
              $("input#nominaltransaksi").val('Silahkan periksa input penghuni, lamasewa, dan jenis penghuni');
            } 
         },
         error: function(response){
          $("input#nominaltransaksi").val('Silahkan periksa input penghuni, lamasewa, dan jenis penghuni');
         }
        });
  }
  function fetchNominalLangsungLunas(idpenghuni,lamasewa,jenispenghuni){
       $.ajax({
         url: 'getNominalLangsungLunas',
         type: 'get',
         dataType: 'json',
         data: { 
          idpenghuni: idpenghuni,
          lamasewa: lamasewa,
          jenispenghuni: jenispenghuni
          },
         success: function(response){
           if(response['jumlahdata']!='0'){
                $("input#nominaltransaksi").val(response['total']);
            }else{
              $("input#nominaltransaksi").val('Silahkan periksa input penghuni, lamasewa, dan jenis penghuni');
            } 
         },
         error: function(response){
          $("input#nominaltransaksi").val('Silahkan periksa input penghuni, lamasewa, dan jenis penghuni');
         }
        });
  }
  function fetchWarningTempo(idkeuanganbulanan){
       $.ajax({
         url: 'getWarning',
         type: 'get',
         dataType: 'json',
         data: { 
          idkeuanganbulanan: idkeuanganbulanan
          },
         success: function(response){
           if(response['jumlahdata']!='0'){
             
            $("input#tanggaltempo").val(response['tanggaltempo']);
             if(response['hariterlewat']=='3'){
              var warning = 
                "<div class=\"alert bg-danger\" role=\"alert\" style=\"padding: 20px;\"><center><em style=\"margin-bottom: 20px;\" class=\"fa fa-lg fa-warning\">&nbsp;</em><br>"+ 
                "<b>Penghuni sudah masuk masa denda 10 persen, abaikan jika tidak perlu didenda.</b>"+
                "<a href=\"#\" data-dismiss=\"alert\" class=\"pull-right\"><em class=\"fa fa-lg fa-close\"></em></a></center> "+
                "</div> ";

                
                $("#warningnominal").empty();
                $("#warningnominal").append(warning);
             }else if(response['hariterlewat']=='7'){
              var warning = 
                "<div class=\"alert bg-danger\" role=\"alert\" style=\"padding: 20px;\"><center><em style=\"margin-bottom: 20px;\" class=\"fa fa-lg fa-warning\">&nbsp;</em><br>"+ 
                "<b>Penghuni sudah mencapai batas maksimum pembayaran 8 hari dan kamar berhak dikosongkan, abaikan jika tidak perlu didenda.</b>"+
                "<a href=\"#\" data-dismiss=\"alert\" class=\"pull-right\"><em class=\"fa fa-lg fa-close\"></em></a></center> "+
                "</div> ";

                
                $("#warningnominal").empty();
                $("#warningnominal").append(warning);
             }else{
                $("#warningnominal").empty();
             }
            }else{
                $("#warningnominal").empty();
            } 
         },
         error: function(response){
                $("#warningnominal").empty();
         }
        });
  }
</script>


<script type="text/javascript">
  function lihat_jatuhtempo(){
    document.getElementById("pan_welcome").style.display = "none";
    $("#pan_jatuhtempo").fadeIn(1000);
    document.getElementById("but_showall").style.display = "block";
    document.getElementById("but_rinciankeuangan").style.display = "none";
    document.getElementById("but_panel_finkamar").style.display = "none";
  }
  
  function lihat_finkamar(){
    document.getElementById("pan_welcome").style.display = "none";
    $("#pan_panel_finkamar").fadeIn(1000);
    document.getElementById("but_showall").style.display = "block";
    document.getElementById("but_rinciankeuangan").style.display = "none";
    document.getElementById("but_jatuhtempo").style.display = "none";
  }
  
  function lihat_rinciankeuangan(){
    document.getElementById("pan_welcome").style.display = "none";
    $("#pan_rinciankeuangan").fadeIn(1000);
    document.getElementById("but_showall").style.display = "block";
    document.getElementById("but_jatuhtempo").style.display = "none";
    document.getElementById("but_panel_finkamar").style.display = "none";
  }
  
  function lihat_semua(){
    document.getElementById("pan_welcome").style.display = "block";
    document.getElementById("pan_jatuhtempo").style.display = "none";
    document.getElementById("pan_rinciankeuangan").style.display = "none";
    document.getElementById("pan_panel_finkamar").style.display = "none";
    document.getElementById("but_showall").style.display = "none";
    document.getElementById("but_jatuhtempo").style.display = "block";
    document.getElementById("but_rinciankeuangan").style.display = "block";
    document.getElementById("but_panel_finkamar").style.display = "block";
  }




  function fetchRincian(start){
       $.ajax({
         url: 'getRincian',
         type: 'get',
         dataType: 'json',
         data: { 
          start: start
          },
         success: function(response){
           if(response['jumlahdata']!='0'){
              var hari=0;
              $.each(response['datarincian'], function(key, value) {
              var datafull = "";
              var tanggal = "";
              var datetanggaltrans = new Date(value['tanggaltransaksi']);
              const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
               
              if(datetanggaltrans.getDate()!=hari){
                  hari = datetanggaltrans.getDate();
                  // Bagian Tanggal Transaksi
                  tanggal += "<tr style=\"width: 100%; background-color: #f5f5f5;\">"+
                                "   <td colspan=\"4\"> "+
                                  "   <h4 style=\" color: #b3b3b3;\">"+
                                    "   <center>";
                  var tanggalhari = ""; 
                  if(datetanggaltrans.getDate()<10){
                    tanggalhari+="0"+datetanggaltrans.getDate();
                  }else{
                    tanggalhari+=datetanggaltrans.getDate();
                  }
                  tanggal+= "  <p>"+ tanggalhari+" "+monthNames[datetanggaltrans.getMonth()] +" "+ datetanggaltrans.getFullYear()+"</p>";
                  tanggal+= "</center>"+"</h4>"+" </td>"+" </tr>";
              }
              // Bagian Gambar Ikon Transaksi
              var gambarikon = "<tr height=\"30\" style=\"padding-bottom: 10px;\">"+
                                    " <!-- Ikon Transaksi -->"+
                                    "   <th rowspan=\"2\" style=\"vertical-align: middle; \"> "+
                                      "     <center>";
              if(value['pilihantransaksi']=='pemasukkan'){
                if(value['jenistransaksi']=='dp'){ 
                      gambarikon+="<img src=\"/uploads/ikondp.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='lunas'){
                      gambarikon+="<img src=\"/uploads/ikonpelunasan.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='lunaslangsung'){
                      gambarikon+="<img src=\"/uploads/lunaslangsung.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='bulanan'){
                      gambarikon+="<img src=\"/uploads/ikonbulanan.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='tambahan'){
                      gambarikon+="<img src=\"/uploads/extendhari.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='lain'||value['jenistransaksi']=='lainnya'){
                      gambarikon+="<img src=\"/uploads/incomelain.png\" width=\"30%\">"
                }
              }
              if(value['pilihantransaksi']=='pengeluaran'){
                if(value['jenistransaksi']=='listrik'){
                      gambarikon+="<img src=\"/uploads/ikonlistrik.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='air'){
                      gambarikon+="<img src=\"/uploads/ikonair.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='gaji'){
                      gambarikon+="<img src=\"/uploads/ikongaji.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='pemeliharaan'){
                      gambarikon+="<img src=\"/uploads/ikonpemeliharaan.png\" width=\"30%\">"
                }else if(value['jenistransaksi']=='lain'||value['jenistransaksi']=='lainnya'){
                      gambarikon+="<img src=\"/uploads/outcomelain.png\" width=\"30%\">"
                }
              }
              gambarikon+="</center></th>";

              //  <!-- Jenis Transaksi -->
              var jenistransaksi = 
                  "  <td style=\"vertical-align: bottom;\"><b>"+value['jenistransaksi'].toUpperCase()+
              "  </b></td>";
              // <!--  -->
              
              // <!-- Nominal Transaksi -->
              var totalnomi = Number(value['nominaltransaksi'])+Number(value['nominaltambahan']);
              var nomitrans = 
              "  <td class=\"textnominal\" style=\"vertical-align: bottom; padding-right: 10px\" align=\"right\">"+
                "        <b style=\" color: ";
                if(value['pilihantransaksi']=='pemasukkan'){nomitrans+=" green ";}
                if(value['pilihantransaksi']=='pengeluaran'){nomitrans+=" #F06560 ";}

                nomitrans+= " ; margin-bottom: 10px;\">"+totalnomi+"</b></td></tr>";
              // <!--  -->

              // <!-- Metode Transaksi -->
              var deskripsi =
                "<tr height=\"30\" style=\"padding-bottom: 10px;color: #b3b3b3;\">"+
                  "    <td class=\"textrincian\" align=\"left\" style=\"vertical-align: top; background-color:white;\">";
                
                    if(value['jatuhtempo']){
                      $.each(response['datanotpaid'], function(keynot, valuenot) {
                        if (valuenot['idpenghuni']==value['idpenghuni']){
                        deskripsi+= "<small>";
                          var datetrans = new Date(value['tanggaltransaksi']);
                          var datetempo = new Date(value['jatuhtempo']);
                          deskripsi+= monthNames[datetrans.getMonth()] +" "+ datetrans.getFullYear();
                          deskripsi+= " - "+monthNames[datetempo.getMonth()] +" "+ datetempo.getFullYear();
                        deskripsi+= "</small> <br>";
                        }
                      });
                    }
                  
                    if(value['namakamar']){
                      deskripsi+= "<small>"+value['namakamar']+"</small>";
                    }
                  if(value['jenistransaksi']=='lain'||value['jenistransaksi']=='lainnya'||value['jenistransaksi']=='gaji'||value['jenistransaksi']=='pemeliharaan'){
                    deskripsi+= value['deskripsi'];
                  }

                var metode = " </td><td class=\"ikonmetode\" style=\"vertical-align: top; padding-right: 10px;\" align=\"right\"><b>";
                  if(value['metode'] =='tunai'){
                    metode+="<img src='/uploads/ikontunai.png'>";
                  }else if(value['metode']=='transfer'){
                    metode+="<img src='/uploads/ikontransfer.png'>";
                  }
                  metode+="</b></td></tr>";
                  // <!--  -->



              if(tanggal!=""){
                datafull=tanggal+gambarikon+jenistransaksi+nomitrans+deskripsi+metode;
              }else{
                datafull=gambarikon+jenistransaksi+nomitrans+deskripsi+metode;
              }
              var datarinci = 
                $(datafull).hide().fadeIn(3000);
                $("#datauang").append(datarinci);
              });
                 
            



            }
         },
         error: function(response){
          $("input#nominaltransaksi").val('Silahkan periksa input penghuni, lamasewa, dan jenis penghuni');
         }
        });
  }
  
</script>

@endsection