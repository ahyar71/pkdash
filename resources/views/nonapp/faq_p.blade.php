@extends('layout.nonapp')

@section('metadesc','Daftar Pertanyaan yang sering ditanyakan tentang Kost atau Frequently Asked Question (FAQ) Kos-kosan')

@section('judul','Frequently Asked Question (FAQ) - Pertanyaan yang sering diajukan seputar kost')


@section('isibodi')
  
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #ff9f43; text-align: center; padding: 80px 20px 20px 20px;position: relative;">
          <div class="bnr-mami">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:inline; color:white;">
                <i class="fas fa-question" style="font-size:4em"></i><br><br>
                <b>Frequently Asked Question (FAQ)</b><br>
                <small>Pertanyaan yang sering diajukan seputar kost</small>
              </p>
            </center>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #ffdec0; text-align: center; padding: 20px;position: relative;">
          <center>
              {{-- Div FAQ --}}
              <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 ">
                <div class="faq_panel faq_panel_top" style="background-color: #50e250;">
                  <p class="faq_panel_q">Berapakah kamar kosong yang tersedia saat ini?</p>
                  <div class="divider" style="background-color:white;"></div>
                  <p class="faq_panel_a">Kamu dapat melihat ketersediaan kamar di halaman 
                  <a href="/" style="font-family: 'Montserrat', sans-serif;color:white"><b>home</b></a></p>
                </div>
              </div>
              {{-- Div FAQ --}}

              <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 ">    
                <div class="faq_panel" style="background-color: #50e250;">
                <p class="faq_panel_q">Apa boleh bawa peliharaan?</p>
                  <div class="divider" style="background-color:white;"></div>
                  <p class="faq_panel_a">Boleh saja kak tapi hanya kucing, burung, atau ikan ya, yang penting tidak najis dan dijaga kebersihannya ya!</p>
                </div>
              </div>
              {{-- Div FAQ --}}
              
              <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 ">    
                <div class="faq_panel" style="background-color: #4bd64b;">
                <p class="faq_panel_q">Apa boleh mencuci pakaian di Kost?</p>
                  <div class="divider" style="background-color:white;"></div>
                  <p class="faq_panel_a">Boleh dong! tapi ingat pakai air secukupnya ya, demi menjaga planet kita bersama.
                </div>
              </div>
              {{-- Div FAQ --}}
              
              <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 ">
                <div class="faq_panel" style="background-color: #3ebe3e;">
                <p class="faq_panel_q">Apa dekat dengan tempat makan, ATM, atau minimarket?</p>
                  <div class="divider" style="background-color:white;"></div>
                  <p class="faq_panel_a">Relatif dekat kak, kurleb paling jauh 200m dari kost :) bisa lihat di map aja, 
                    <a href="/#MapLocation" style="font-family: 'Montserrat', sans-serif;color:#fccca0"><b>klik disini</b></a></p>
                </div>
              </div>
              {{-- Div FAQ --}}
              
              <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 ">    
                <div class="faq_panel" style="background-color: #38a838;">
                <p class="faq_panel_q">Boleh sekamar dengan pasangan yang belum menikah?</p>
                  <div class="divider" style="background-color:white;"></div>
                  <p class="faq_panel_a" style="font-size:1.5em;">BIG NO! Jika kakak Pasutri bisa menunjukkan bukti nikah saat mulai Ngekos yah! :)</p>
                </div>
              </div>
              {{-- Div FAQ --}}
              
              <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 ">    
                <div class="faq_panel" style="background-color: #329b32;">
                <p class="faq_panel_q">Apakah ada parkir mobil? Apa free wifi? Apa kos sudah AC?</p>
                  <div class="divider" style="background-color:white;"></div>
                  <p class="faq_panel_a" style="font-size:1.35em;" >Ketersediaan parkir mobil silahkan tanya penjaga kos 
                    (<a href="http://wa.me/6287880103866" style="font-family: 'Montserrat', sans-serif;color:white"><b>klik disini</b></a>). Kost sudah dilengkapi FREE wifi dan AC yah kak :) lengkapnya bisa 
                    (<a href="/profil_page" style="font-family: 'Montserrat', sans-serif;color:white"><b>klik disini</b></a>)
                  </p>
                </div>
              </div>

              {{-- Div FAQ --}}
              
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 ">    
                <div class="faq_panel faq_panel_bottom" style="background-color: #297a29; text-align: center; padding: 40px;position: relative;width:100%;margin-top:20px;margin-bottom:20px">
                <p class="faq_panel_q">Pertanyaanmu masih belum terjawab?</p>
                  <div class="divider" style="background-color:white;"></div>
                  <p style="font-family: 'Montserrat', sans-serif;font-size:1.5em;color:white;">
                    <a href="http://wa.me/6287880103866" style="font-family: 'Montserrat', sans-serif;color:white"><b>Tanya yuk! Klik disini</b></a>
                  </p>
                </div>
              </div>
          </center>
        </div>
      </div>
    </div> 


    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #a8a8a8; text-align: center; padding: 10px 10px 20px 20px;position: relative;">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:inline;font-size:1.2em;margin-top:5px;color:white;" >
              Terima kasih <br>atas kepercayaan penghuni kost selama ini!<br>
               </p>
            </center>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #707070; text-align: center; padding: 10px 10px 20px 20px;position: relative;">
            <center>
              <p style="color:#ff9f43;font-size:1.5em"><b>Yuk Survey ke Kost ! </b></p>
              <a href="https://wa.me/6287880103866" target="_top"><button style="background-color:#ff9f43;padding:10px 70px 10px 70px;color:white;border-radius:20px;"><b>Buka</b></button></a>
            </center>
        </div>
      </div>
    </div>
        
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 bnr-mami" style="background-color: #525252; text-align: center; padding: 20px;">
            <center>
              <p style="font-family: 'Bulgatti';display:inline; color:white;"><b> P r e m i e r e  K o s t .com</b></p>
            </center>
        </div>
      </div>
    </div>

  

    <!--HEADER END-->
@endsection

@section('footer')

<script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript">
    // When the user scrolls the page, execute myFunction
  window.onscroll = function() {myFunction()};
  $( window ).on( "load", myFunction );
  // Get the header
  var header = document.getElementById("navigasistick");

  // Get the offset position of the navbar
  var sticky = header.offsetTop;

  // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("navbar-custom-stick");
    } else {
      header.classList.remove("navbar-custom-stick");
    }
  }

  var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

  var slideIndex = 1;
showDivsPelita(slideIndex);

function plusDivsPelita(n) {
  showDivsPelita(slideIndex += n);
}

function showDivsPelita(n) {
  var i;
  var x = document.getElementsByClassName("mySlidesPelita");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>

  
@endsection