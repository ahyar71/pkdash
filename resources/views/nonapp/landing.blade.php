@extends('layout.nonapp')

@section('metadesc','Cari kost Karyawan AC Murah di Jakarta? Kost Setia Premiere Bekasi jawabannya! Kost Premium dengan AC, FREE WIFI, Springbed, dan masih banyak lagi.')

@section('judul','Premiere Kost - Kost Murah, Lengkap, Nyaman')


@section('isibodi')
    <div class="landingbanner">
          <div class="info-namakost">
            <h2 data-aos="zoom-in"><b>
              <span style="font-family: alphae;color:#b96933">PREMIERE KOST</span> <br><br>
              <span style="background-color:#FFEADC;font-family: 'justone', justone; color:#ff9100;">Simple, Strategis, Ekonomis</span>
          </b></h2>
          </div>          
    </div> 
    
    <div class="container-fluid" >
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " data-aos="zoom-in-up" style="background-color: white; text-align: center; padding: 20px;position: relative;">
          <p style="font-family: 'Montserrat', sans-serif;color:#b96933;"><b> <h2 style="color:#b96933">Our Hospitalities</h2></b></p>
        </div>
        <div class="col-md-3 col-lg-2 col-sm-6 col-xs-6"  data-aos="zoom-in-up" style="background-color: white; text-align: center; padding: 40px;position: relative;">
        <div style="padding:20px;"><img src="{{asset('/uploads/wifi.svg')}}" width="100%;"></div>
          <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;padding-top:20px;color:#bdbbbb;"><b> Free Internet</b></p>
        </div>  
        <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6 "  data-aos="zoom-in-up" style="background-color: white; text-align: center; padding: 40px;position: relative;">
          <div style="padding:20px;"><img src="{{asset('/uploads/ac.svg')}}" width="100%;"></div>
          <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;padding-top:20px;color:#bdbbbb;"><b> Air Conditioner</b></p>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6  "  data-aos="zoom-in-up" style="background-color: white; text-align: center; padding: 40px;position: relative;">
          <div style="padding:20px;"><img src="{{asset('/uploads/kmdalam.svg')}}" width="100%;"></div>
          <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;padding-top:20px;color:#bdbbbb;"><b> KM Dalam</b></p>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6 "  data-aos="zoom-in-up" style="background-color: white; text-align: center; padding: 40px;position: relative;">
          <div style="padding:20px;"><img src="{{asset('/uploads/springbed.svg')}}" width="100%;"></div>
          <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;padding-top:20px;color:#bdbbbb;"><b> Springbed</b></p>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6 "  data-aos="zoom-in-up" style="background-color: white; text-align: center; padding: 40px;position: relative;">
          <div style="padding:20px;"><img src="{{asset('/uploads/lemari.svg')}}" width="100%;"></div>
          <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;padding-top:20px;color:#bdbbbb;"><b> Lemari</b></p>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6 "  data-aos="zoom-in-up" style="background-color: white; text-align: center; padding: 40px;position: relative;">
          <div style="padding:20px;"><img src="{{asset('/uploads/meja.svg')}}" width="100%;"></div>
          <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;padding-top:20px;color:#bdbbbb;"><b> Meja Kursi</b></p>
        </div>
      </div>
    </div> 

    <div class="container-fluid" >
      <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 " style="padding: 80px;">
        <div class="panelkamarkosong" id="panelkkosong"  data-aos="zoom-in" style="background-color: #CCCCFF;border-radius:20px;">
          <div class="toppanel">
            <p class="judul-panelkamarkosong"> Kost Setia Premiere</p>
            <p class="isi-paneltotalkamarkosong"  data-aos="zoom-in">  {{$kosongsetia}} </p>
            <p class="isi-panelkamarkosong">  Kamar Tersedia </p>
          </div>
        </div>
      </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 " style="padding: 80px;">
        <div class="panelkamarkosong" data-aos="zoom-in" style="background-color: #FFE8B2;border-radius:20px;">
          <p class="judul-panelkamarkosong"> Kost Pelita Premiere</p>
          <p class="isi-paneltotalkamarkosong" data-aos="zoom-in">  {{$kosongpelita}} </p>
          <p class="isi-panelkamarkosong">  Kamar Tersedia </p>
        </div>
        </div>
      </div>
    </div> 

    
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " data-aos="zoom-in" style="height:200px;" >
          <center><img src="/uploads/waveline.svg"></center>
        </div>
      </div>
    </div>
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 " data-aos="zoom-in" style="padding: 80px;background-image:url('/uploads/setiates.svg');background-repeat:no-repeat;height:500px;">
          <p style="font-family: 'Montserrat';color:#b96933;"><b> <h2 data-aos="zoom-in" style="color:#b96933">Kost Setia<br> Premiere</h2></b></p>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <a href="tel:087880103866" target="_top"><button class="btn btn-lg" style="width:100%;margin-top:20px;background-color:white;font-family:'Montserrat';font-size:100%;border:3px solid #a7a7ff;border-radius:20px;color:#a7a7ff;">Contact</button></a>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="background-color:white;font-family:'Montserrat';padding: 20px 40px; ">
          <div class="mapouter" data-aos="zoom-in" style="margin-top:40px;">
            <div class="gmap_canvas">
              <iframe width="100%" height="303" id="gmap_canvas" src="https://maps.google.com/maps?q=kost%20setia%20premiere&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
              </iframe>
            </div>
            <style>.mapouter{position:relative;text-align:right;height:303px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:303px;width:100%;;}</style>
          </div>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <a href="https://www.google.com/maps?saddr=My+Location&daddr=Kost+Setia+Premiere,+Jalan+Setia+1,+Jatiwaringin,+Bekasi+City,+West+Java"><button class="btn btn-lg" style="width:100%;margin-top:20px;background-color:white;font-family:'Montserrat';font-size:100%;border:3px solid #a7a7ff;border-radius:20px;color:#a7a7ff;">From Me</button></a>
          </div>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <a href="https://g.page/kostjakartatimur"><button class="btn btn-lg" style="width:100%;margin-top:20px;background-color:white;font-family:'Montserrat';font-size:100%;border:3px solid #a7a7ff;border-radius:20px;color:#a7a7ff;">Review</button></a>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding: 20px 20px;">
            <a href="tel:087880103866" ><button class="btn btn-lg" style="width:100%;background-color:#a7a7ff;font-family:'Montserrat';font-size:100%;color:white;border-radius:20px;">More Detail</button></a>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid" style="margin-bottom: 100px;">
      <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 " data-aos="zoom-in" style="padding: 80px;background-image:url('/uploads/pelita.svg');background-repeat:no-repeat;height:500px;">
          <p style="font-family: 'Montserrat';color:#b96933;"><b> <h2 data-aos="zoom-in" style="color:#b96933">Kost Pelita<br> Premiere</h2></b></p>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <a href="tel:082114500157" target="_top"><button class="btn btn-lg" style="width:100%;margin-top:20px;background-color:white;font-family:'Montserrat';font-size:100%;border:3px solid #a7a7ff;border-radius:20px;color:#a7a7ff;">Contact</button></a>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="background-color:white;font-family:'Montserrat';padding: 20px 40px; ">
          <div class="mapouter" data-aos="zoom-in">
            <div class="gmap_canvas">
              <iframe width="100%" height="303" id="gmap_canvas" src="https://maps.google.com/maps?q=kost%20pelita%20premiere%20makassar&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
              <style>.mapouter{position:relative;text-align:right;height:303px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:303px;width:100%;}</style>
          </div>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <a href="https://www.google.com/maps?saddr=My+Location&daddr=Kost+Pelita+Premiere,+Jalan+Pelita+Raya+6,+Bua+Kana,+Makassar+City,+South+Sulawesi"><button class="btn btn-lg" style="width:100%;margin-top:20px;background-color:white;font-family:'Montserrat';font-size:100%;border:3px solid #a7a7ff;border-radius:20px;color:#a7a7ff;">From Me</button></a>
          </div>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <a href="https://g.page/kosmakassar"><button class="btn btn-lg" style="width:100%;margin-top:20px;background-color:white;font-family:'Montserrat';font-size:100%;border:3px solid #a7a7ff;border-radius:20px;color:#a7a7ff;">Review</button></a>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding: 20px 20px;">
            <a href="tel:082114500157"><button class="btn btn-lg" style="width:100%;background-color:#a7a7ff;font-family:'Montserrat';font-size:100%;color:white;border-radius:20px;">More Detail</button></a>
          </div>
        </div>
      </div>
    </div>


    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 bnr-bottom with-shadow" data-aos="zoom-in-down" style="background-color: #b35701;">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:block; color:white;"><b> e-Tagihanmu</b></p>
              <a href="/et_page" ><button style="background-color:#b35701 ;">Buka</button></a>
            </center>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 bnr-bottom" data-aos="zoom-in-down" style="background-color: #d46700;">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:block; color:white;"><b> Lokasi</b></p>
              <a href="#" ><button type="button" data-toggle="modal" data-target="#ModalMap"  aria-expanded="false" style="background-color:#d46700 ;">Buka</button></a>
            </center>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 bnr-bottom " data-aos="zoom-in-down" style="background-color: #ff7b00;">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:block; color:white;"><b> Soc Media</b></p>
              <a href="#" ><button type="button" data-toggle="modal" data-target="#ModalSoc"  aria-expanded="false" style="background-color:#ff7b00 ;">Buka</button></a>
            </center>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 bnr-bottom " data-aos="zoom-in-down" style="background-color: #ff9028;">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:block; color:white;"><b> Dev Profile</b></p>
              <a href="#" ><button type="button" data-toggle="modal" data-target="#ModalDev"  aria-expanded="false" style="background-color:#ff9028 ;">Buka</button></a>
            </center>
        </div>
      </div>
    </div>


    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" data-aos="zoom-in-up" style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
          <div class="bnr-mami">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:block; color:#ff9f43;margin-bottom:20px;"><b> Mau tanya-tanya atau reservasi?</b></p>
              <a href="https://wa.me/6282114500157" target="_top"><img src="https://cdn3.iconfinder.com/data/icons/social-network-30/512/social-01-512.png" style="width:3em;display:inline;margin:20px;"></a>
              <a href="tel:082114500157" target="_top"><img src="https://image.flaticon.com/icons/png/512/15/15874.png" style="width:3em;display:inline;margin:20px;"></a>
              <a href="sms:082114500157" target="_top"><img src="https://image.flaticon.com/icons/svg/1112/1112547.svg" style="width:3em;display:inline;margin:20px;  "></a>
            </center>
          </div>
        </div>


      </div>
    </div>
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 bnr-mami" style="background-color: #a5a5a5; text-align: center; padding: 20px;position: relative;">
            
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 bnr-mami" style="background-color: #807e7e; text-align: center; padding: 20px;position: relative;">
            
        </div>
      </div>
    </div>  

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 bnr-mami" style="background-color: #525252; text-align: center; padding: 20px;position: relative;">
            <center>
              <p style="font-family: 'Bulgatti';display:inline; color:white;"><b> P r e m i e r e  K o s t .com</b></p>
            </center>
        </div>
      </div>
    </div>

    <div class="col-sm-12 col-lg-12">
      <div class="modal fade" id="ModalMap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: 2px dashed white">
              <center><h1 style="color:white;" ><b>Lokasi Kost</b></h1></center>
            </div>
            <div class="modal-body">
              <div class="mapouter">
                <div class="gmap_canvas">
                  <iframe width="100%" height="303" id="gmap_canvas" src="https://maps.google.com/maps?q=kost%20setia%20premiere&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                  </iframe>
                </div>
                <style>.mapouter{position:relative;text-align:right;height:303px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:303px;width:100%;;}</style>
              </div>
              <a class="dropdown-item" href="#" style=" text-align: center; background-color: #ffd828;box-shadow:0px 15px 25px #ffd828;">
                <p style="font-size: 1.2em;color:white;">Google Maps Kost Setia Premiere</p>
              </a>
              <a class="dropdown-item" href="https://www.google.com/maps?saddr=My+Location&daddr=Kost+Setia+Premiere,+Jalan+Setia+1,+Jatiwaringin,+Bekasi+City,+West+Java" style=" text-align: center; margin-bottom: 30px;  background-color: #f8ce14;box-shadow:0px 15px 25px #f8ce14;border-bottom-left-radius:40px;border-bottom-right-radius:40px;">
                <p style="font-size: 1.2em;color:white;">-Arahkan Saya ke Kost-</p>
              </a>

              <div class="mapouter">
                <div class="gmap_canvas">
                  <iframe width="100%" height="303" id="gmap_canvas" src="https://maps.google.com/maps?q=kost%20pelita%20premiere%20makassar&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </div>
                  <style>.mapouter{position:relative;text-align:right;height:303px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:303px;width:100%;}</style>
              </div>
              <a class="dropdown-item" href="#" style=" text-align: center; background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Google Maps Kost Pelita Premiere</p>
              </a>
              <a class="dropdown-item" href="https://www.google.com/maps?saddr=My+Location&daddr=Kost+Pelita+Premiere,+Jalan+Pelita+Raya+6,+Bua+Kana,+Makassar+City,+South+Sulawesi" style=" text-align: center; margin-bottom: 30px;  background-color: #e99211;box-shadow:0px 15px 25px #e99211;border-bottom-left-radius:40px;border-bottom-right-radius:40px;">
                <p style="font-size: 1.2em;color:white;">-Arahkan Saya ke Kost-</p>
              </a>

              <div class="divider"></div>
              <button type="button" class="dropdown-item" style="border: 2px dashed white; text-align: center; margin-bottom: 10px;   background-color: #1ED760;" data-dismiss="modal" aria-label="Close">
                <label style="font-size: 25px;">Kembali</label>
              </button>
              </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="ModalSoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: 2px dashed white">
              <center><h1 style="color:white;" ><b>Tetap Terhubung Yuk!</b></h1></center>
            </div>
            <div class="modal-body">

              <a class="dropdown-item" href="#" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Instagram</p>
              </a>
              <blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BioVRHGFoCj/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/BioVRHGFoCj/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BioVRHGFoCj/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Premiere Kost Group (@premieregroup.kost)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2018-05-11T08:19:06+00:00">May 11, 2018 at 1:19am PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
              
              <a class="dropdown-item" href="https://wa.me/6282114500157" target="_top" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Whatsapp</p>
              </a>
              
              <div class="divider"></div>
              <button type="button" class="dropdown-item" style="border: 2px dashed white; text-align: center; margin-bottom: 10px;   background-color: #1ED760;" data-dismiss="modal" aria-label="Close">
                <label style="font-size: 25px;">Kembali</label>
              </button>
              </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="ModalDev" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: 2px dashed white">
              <center><h1 style="color:white;" ><b>Web Dev Profile</b></h1></center>
            </div>
            <div class="modal-body" style="text-align:center;">
              <img src="https://t3.ftcdn.net/jpg/00/64/67/52/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg" style="border-radius:100%; width:40%;">
              
              <h2><b>Ahyar</b></h2>
              <div style="background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;">
              <p style="">
                <br><br>
                Hello there!<br>
                Thank you for viewing my dev profile.<br><br>
                
                I present this website to my parents who raise me 'til now,
                which also the owner of Premiere Kost Group. Paralel with my thesis, 
                i develop this system as a whole from the use case, design, coding.
                If you interested to discuss about something technical, 
                or even a <b>Job Opportunity</b> I will really appreciate it! :)<br><br>

                Feel free to reach me through my social media below.<br>
                Thank you! <br><br>

                
                <small style="width:100%;">I hope you could find enough information in the website.</small><br>
                If you not, please contact the owner by click <a href="https:/wa.me/6282114500157" style="color:#ff9f43;">here</a>
                <br><br><br><br>
              </p>
              </div>
                
              <center><a href="https://www.linkedin.com/in/muhammad-ahyar"><img src="https://images.vexels.com/media/users/3/137382/isolated/preview/c59b2807ea44f0d70f41ca73c61d281d-linkedin-icon-logo-by-vexels.png" style="width:20%;display:block;margin-top:20px;margin-bottom:20px;"></a></center>
              
                
              
              <div class="divider"></div>
              <button type="button" class="dropdown-item" style="border: 2px dashed white; text-align: center; margin-bottom: 10px;   background-color: #1ED760;" data-dismiss="modal" aria-label="Close">
                <label style="font-size: 25px;">Kembali</label>
              </button>
              </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="ModalMami" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: 2px dashed white">
              <center><h1 style="color:white;" ><b>PremiereKost X Mamikost</b></h1></center>
            </div>
            <div class="modal-body">
              <a href="https://mamikos.com/room/kost-bekasi-kost-campur-murah-kost-hijau-type-a-pondok-gede-bekasi#/" target="_blank"><img src="https://static.mamikos.com/uploads/cache/data/style/2019-10-07/WhloY6EF-540x720.jpg" style="width:100%;"></a>
              <a class="dropdown-item" href="https://mamikos.com/room/kost-bekasi-kost-campur-murah-kost-hijau-type-a-pondok-gede-bekasi#/" target="_blank" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Kost Setia Premiere Jakarta</p>
              </a>
              <a href="https://mamikos.com/room/kost-makassar-kost-campur-eksklusif-kost-pelita-premiere-rappocini-makassar#/" target="_blank"><img src="https://static.mamikos.com/uploads/cache/data/style/2018-08-18/EUVGZYRB-540x720.jpg" style="width:100%;"></a>
              <a class="dropdown-item" href="https://mamikos.com/room/kost-makassar-kost-campur-eksklusif-kost-pelita-premiere-rappocini-makassar#/" target="_blank" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Kost Pelita Premiere Makassar</p>
              </a>
            </div>
          </div>
        </div>
      </div>

    </div>

    <!--HEADER END-->
@endsection

@section('footer')

<script
  src="https://code.jquery.com/jquery-3.5.1.js"></script>

{{-- <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script> --}}
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/aos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script> --}}
    
  <script type="text/javascript">
    AOS.init();
  </script>

<script type="text/javascript">


    // When the user scrolls the page, execute myFunction
  window.onscroll = function() {myFunction()};
  
  $( window ).on( "load", myFunction );
  // Get the header
  var header = document.getElementById("navigasistick");

  // Get the offset position of the navbar
  var sticky = header.offsetTop;

  // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("navbar-custom-stick");
    } else {
      header.classList.remove("navbar-custom-stick");
    }
  }

  if(window.location.href.indexOf('#MapLocation') != -1) {
    $('#ModalMap').modal('show');
  }
  if(window.location.href.indexOf('#SocialMedia') != -1) {
    $('#ModalSoc').modal('show');
  }
  if(window.location.href.indexOf('#DevProfil') != -1) {
    $('#ModalDev').modal('show');
  }

  
  var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

  var slideIndex = 1;
showDivsPelita(slideIndex);

function plusDivsPelita(n) {
  showDivsPelita(slideIndex += n);
}

function showDivsPelita(n) {
  var i;
  var x = document.getElementsByClassName("mySlidesPelita");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>

  
@endsection