@extends('layout.nonapp')

@section('metadesc','Lihat jadwal tagihan bulanan kosan mu disini!')

@section('judul','e-Tagih - Liat Tagihanmu')


@section('isibodi')

<div class="modal-dialog" role="document">
    <div class="modal-content" style="top: 100px">
        <div class="modal-header">
            <center><h1 style="color:white;" ><b>Form Tambah Penghuni</b></h1></center>
        </div>
        <form action="{{action('penghunicontroller@newPenghuni')}}" class="ubahan" method="post" enctype="multipart/form-data">
        <div class="modal-body">
            {{csrf_field()}}
            <!--1. Kolom Input Pilihan Kost-->
            <div class="form-group">
                <label for="exampleInputEmail1">Nama Kost</label>                    
                <select id="idkost" name="idkost" required="" class="form-control" style="margin-top: 10px; background-color: #3e89d8; color: white;">
                    <option value="" selected="" disabled="">Pilih Kost</option>                 
                    <option value="1">Kost Setia Premiere (Bekasi)</option>                     
                    <option value="2">Kost Pelita Premiere (Makassar)</option>    
                </select>               
            </div>
            <div class="form-group" id="kolominput">
                
            </div>
        </div>  
        <!--Tombol Submit dan Batal-->
        <span >
            <div class="col-sm-12 col-lg-12 col-md-12">
            <center>
            <button class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
        </center>
        </div>
        </span>   
        </form>
        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
        <a href="/"><button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;">Close</button></a>
        </div>
    </div>
</div>

@endsection

@section('footer')

<script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>

{{-- Untuk menampilkan kamar kosong berdasarkan pilihan kost --}}
<script type="text/javascript">
$(document).ready(function(){
    
    $("#navigasistick").css("background-color", "orange");
    $('#idkost').change(function(){
        var idkosan = this.value;
        var tr_strer = 
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Nama Penghuni</label>"+                    
            "<input type=\"text\" name=\"namapenghuni\" maxlength=\"70\" class=\"form-control\" required >"+                      
        "</div>"+
        // Membuat fungsi AJAX mengambil kamar kosong berdasarkan pilihan idkost 
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Kamar Hunian</label>"+  
            "<input type=\"text\" name=\"status\" value=\"terisi\" hidden>"+
            "<select id=\"opsikamar\" name=\"idkamar\" class=\"form-control\" style=\"margin-top: 10px; background-color: #3e89d8; color: white;\">"+
                "<option id=\"pilloading\" value=\"\" selected=\"\" disabled=\"\">Loading .....</option>"+
            "</select>"+
        "</div>"+
        // ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">NIK (KTP/SIM)</label>"+
            "<input type=\"number\" name=\"nik\" minlength=\"8\" maxlength=\"30\" required class=\"form-control\">"+                     
        "</div>"+
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Foto KTP</label>"+
            "<input class=\"form-control\" style=\"background-color: #1ED760; border: 2px dashed white; color:white; font-weight: bold; width: 100%;\" type=\"file\" name=\"filefoto\" id=\"filefoto\">"+
        "</div>"+
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Nomor Telepon</label>"+
            "<input type=\"number\" name=\"nopenghuni\" required class=\"form-control\" maxlength=\"15\">"+
            "<label>Contoh: 62812xxxxxxx</label>"+
        "</div>"+
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Pekerjaan</label>"+                         
            "<input type=\"text\" name=\"pekerjaan\" required class=\"form-control\" maxlength=\"70\">"+
            "</div>"+
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Email</label>"+
            "<input type=\"email\" name=\"email\" required class=\"form-control\" maxlength=\"100\">"+
        "</div>"+
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Kendaraan</label>"+
            "<select id=\"jeniskendaraan\" name=\"jeniskendaraan\" required class=\"form-control\">"+
                    "<option value=\"mobil\" selected>Mobil</option>"+
                    "<option value=\"motor\" selected>Motor</option>"+
                    "<option value=\"none\" selected>Tidak Ada</option>"+
            "</select>"+
        "</div>"+
        "<div class=\"form-group plat\">"+
            "<label for=\"exampleInputEmail1\">Plat Kendaraan</label>"+
            "<input type=\"text\" name=\"platkendaraan\" id=\"plat\" class=\"form-control\" maxlength=\"20\">"+
        "</div>"+
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Tanggal Masuk</label>"+
            "<input type=\"date\" name=\"tanggalmasuk\" required class=\"form-control\" maxlength=\"15\" value=\"{{date("Y-m-d",strtotime("now"))}}\">"+
        "</div>"+
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Catatan</label>"+
            "<textarea name=\"deskripsi\" required class=\"form-control\" size=\"3\" maxlength=\"300\"></textarea>"+
        "</div>"+
        "<div style=\"width:100%;padding:20px;color:white;background-color:darkblue;text-align:center;\"><h2 style=\"color:white;margin-bottom:20px;\">Premiere Friends</h2><p style=\"color:white;font-size:80%;margin-bottom:20px;\">Silahkan membuat akun PremiereFriends anda <br>untuk menikmati berbagai fitur kost kami</p><div>"+
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Username</label>"+
            "<input type=\"text\" name=\"username\" minlength=\"3\" maxlength=\"30\" required class=\"form-control\">"+                     
        "</div>"+
        "<div class=\"form-group\">"+
            "<label for=\"exampleInputEmail1\">Password</label>"+
            "<input id=\"cekPass\" type=\"password\" name=\"tag\" minlength=\"3\" maxlength=\"30\" required class=\"form-control\">"+                     
            "<span class=\"pull-right\"><input type=\"checkbox\" onClick=\"revealPass()\" style=\"margin-top:10px;padding-bottom:20px;\"> <label style=\"font-size:80%;\">Lihat Password</label></span>"+
        "</div>";

        $("#kolominput").empty();
        $("#kolominput").append(tr_strer);
        fetchKamar(idkosan);
      
    });
  });

  //Mengambil Record Berdasarkan ID Kamar
 function fetchKamar(idkost){
       $.ajax({
         url: 'getKamarKostbyKost',
         type: 'get',
         dataType: 'json',
         data: { 
            idkost: idkost
          },
         success: function(data_kamar){
            if(data_kamar['jumlahdata']!='0'){
                var len = 0;
                var fin = 0;
                if(data_kamar['data'] != null){
                    len = data_kamar['data'].length;
                }

                if(len > 0){
                    
                    $("#opsikamar").empty();
                    var opsiheader = "<option id=\"pilkamar\" value=\"\" selected=\"\" disabled=\"\">Pilih Kamar</option>";
                    $("#opsikamar").append(opsiheader);
                    for(var i=0; i<len; i++){
                        var idkamar = data_kamar['data'][i].id;
                        var namakamarkost = data_kamar['data'][i].namakamarkost;
                        var stringopsikamar = '';
                        
                        var tr_strsu = "<option value=\""+idkamar+"\">"+namakamarkost+"</option>";
                    
                        $("#opsikamar").append(tr_strsu);
                    }
                }
            }else{
                    var tr_strnodata = 
                    "<option id=\"kamarnotfound\" value=\"\" disabled selected=\"\">Pilihan Kamar Tidak Ada</option>";
                    
                    $("#opsikamar").append(tr_strnodata);
            }
         },
         error: function(data_kamar){
                var tr_strnodata = 
                "<option id=\"kamarnotfound\" value=\"\" disabled selected=\"\">Pilihan Kamar Tidak Ada</option>";
                
                $("#opsikamar").empty();
                $("#opsikamar").append(tr_strnodata);
            }
         })
       }

function revealPass() {
  var x = document.getElementById("cekPass");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

 // When the user scrolls the page, execute myFunction
 window.onscroll = function() {myFunction()};
  
  $( window ).on( "load", myFunction );
  // Get the header
  var header = document.getElementById("navigasistick");

  // Get the offset position of the navbar
  var sticky = header.offsetTop;

  // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("navbar-custom-stick");
    } else {
      header.classList.remove("navbar-custom-stick");
    }
  }
</script>
  
@endsection