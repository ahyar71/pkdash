@extends('layout.nonapp')

@section('judul','e-Tagih - Liat Tagihanmu')


@section('isibodi')
  
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #ff9f43; text-align: center; padding: 80px 20px 20px 20px;position: relative;">
          <div class="bnr-mami">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:inline; color:white;">
                <b>Hai, Penghuni Premiere Kost</b><br>
                <small>Yuk cek tagihan bulananmu.</small>
              </p>
            </center>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
          <center>
            <div class="input-group" style="width:70%;">
              <input type="text" class="form-control" id="username" name="username" placeholder="Username" style="width:40%;">
              <h1>#</h1>
              <input type="text" class="form-control" id="tag" name="tag" placeholder="Tag"style="width:20%;">
            </div>
            <button type="button" id="cek" class="form-control" style="width:30%; margin-top:20px; background-color:orange;color:white;">Cek</button>
            
          </center>
        </div>
      </div>
    </div> 

    <div class="container-fluid" id="result">
      {{-- <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
          <div style="background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;">
            <p style="font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em">
                <b>Hai, kak</b><br>
                <small id="namaPenghuni"></small><br><br>

                Kamu masuk tanggal <b id="tanggalMasuk"></b><br>
                Kamu sudah 'Ngekos' Selama <b id="lamaNgekos">5</b><br>
                Loh disini. Makasih banyak ya!.<br><br>

                Next Pembayaran Tanggal <br>
                <b style="font-size:2em;" id="jatuhTempo">18 Juni 2020</b><br>
                Jangan Lupa Bayar yah!<br><br><br>

                <b>ttd</b><br>
                Kost Setia Premiere<br><br><br>
              <center>
                <a href="/et_page"><button type="button" class="form-control" style="width:30%; margin-top:20px; background-color:orange;color:white;">Refresh</button></a>
                <a href="/"><button type="submit" class="form-control" style="width:30%; margin-top:20px; background-color:orange;color:white;">Back</button></a>
              </center> --}}
                {{-- Jika tidak ada data ditemukan --}}
                {{-- <i class="fa fa-ban" style="font-size:2em;color:#ff9f43;"></i><br>
                <b style="font-size:1em;">Waduh, Tidak ada data yang cocok. <br>
                  Coba cek lagi username dan tagnya ya kak!</b><br> --}}
            {{-- </p>
          </div>
        </div>
      </div> --}}
    </div> 

    

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 bnr-mami" style="background-color: #525252; text-align: center; padding: 20px;">
            <center>
              <p style="font-family: 'Bulgatti';display:inline; color:white;"><b> P r e m i e r e  K o s t .com</b></p>
            </center>
        </div>
      </div>
    </div>

    <!--HEADER END-->
@endsection

@section('footer')

<script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript">
    // When the user scrolls the page, execute myFunction
  window.onscroll = function() {myFunction()};

  // Get the header
  var header = document.getElementById("navigasistick");

  // Get the offset position of the navbar
  var sticky = header.offsetTop;

  // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("navbar-custom-stick");
    } else {
      header.classList.remove("navbar-custom-stick");
    }
  }


  // Script dibawah ini untuk fetch data username dan tag untuk mengambil data tagihan penghuni
  
$(document).ready(function(){
  
      $('#result').hide();
    // Search by userid
    $('#cek').click(function(){
      
      var username = $('#username').val();
      var tag = $('#tag').val();
    
      if(tag != '' && username != ''){
        
        fetchRecords(username, tag);
          $('#result').show();
          var tr_strer = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                    "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                      "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<b>Mengambil Data ....</b><br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";

                $("#result").empty();
                $("#result").append(tr_strer);

      }else{
        var tr_strer = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                    "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                      "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<b>Silahkan masukkan username dan tag kamu, silahkan bertanya ke penjaga kos jika belum mengetahuinya :)</b><br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";

                $("#result").empty();
                $("#result").append(tr_strer);
      }

    });
  });
  function getAge(dateString) {
        var now = new Date();
        var today = new Date(now.getYear(),now.getMonth(),now.getDate());

        var yearNow = now.getYear();
        var monthNow = now.getMonth();
        var dateNow = now.getDate();

        var dob = new Date(dateString.substring(6,10),
                          dateString.substring(0,2)-1,                   
                          dateString.substring(3,5)                  
                          );

        var yearDob = dob.getYear();
        var monthDob = dob.getMonth();
        var dateDob = dob.getDate();
        var age = {};
        var ageString = "";
        var yearString = "";
        var monthString = "";
        var dayString = "";
        var grateful = "";


        yearAge = yearNow - yearDob;

        if (monthNow >= monthDob)
          var monthAge = monthNow - monthDob;
        else {
          yearAge--;
          var monthAge = 12 + monthNow -monthDob;
        }

        if (dateNow >= dateDob)
          var dateAge = dateNow - dateDob;
        else {
          monthAge--;
          var dateAge = 31 + dateNow - dateDob;

          if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
          }
        }

        age = {
            years: yearAge,
            months: monthAge,
            days: dateAge
            };

        if ( age.years > 1 ) yearString = " Tahun";
        else yearString = " Tahun";
        if ( age.months> 1 ) monthString = " Bulan";
        else monthString = " Bulan";
        if ( age.days > 1 ) dayString = " Hari";
        else dayString = " Hari";


        if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
          ageString = age.years + yearString + ", " + age.months + monthString + ", dan " + age.days + dayString + grateful;
        else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
          ageString = age.days + dayString + grateful ;
        else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
          ageString = age.years + yearString + grateful;
        else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
          ageString = age.years + yearString + " dan " + age.months + monthString + grateful;
        else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
          ageString = age.months + monthString + " dan " + age.days + dayString + grateful;
        else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
          ageString = age.years + yearString + " dan " + age.days + dayString + grateful;
        else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
          ageString = age.months + monthString + grateful;
        else ageString = "-";

        return ageString;
    }


  function fetchRecords(username,tag){
       $.ajax({
         url: 'getPenghuni',
         type: 'get',
         dataType: 'json',
         data: { 
            username: username, 
            tag: tag
          },
         success: function(response){
           if(response['jumlahdata']!='0'){
            var len = 0;
            var fin = 0;
            if(response['datapenghuni'] != null){
              len = response['datapenghuni'].length;
            }

            //Untuk menampilkan tanggal masuk dan nama
            if(len > 0){
              for(var i=0; i<len; i++){
                var id = response['datapenghuni'][i].id;
                var usernames = response['datapenghuni'][i].username;
                var namas = response['datapenghuni'][i].namapenghuni;
                var tags = response['datapenghuni'][i].tag;
                var tags2 = response['datapenghuni'][i].tags;
                var awalmasuk = response['datapenghuni'][i].tanggalmasuk;
                response['datapenghuni'][i].tanggalmasuk = Date.parse(response['datapenghuni'][i].tanggalmasuk);
                var tanggalmasuks = response['datapenghuni'][i].tanggalmasuk;
                var d = new Date(tanggalmasuks);
                var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]; 
              }

              
            if(response['datakeuangan']==null){
              var tr_strfa = 
              "<div class=\"row\">"+
                "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                  "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                    "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                        "<b>Hai, kak</b><br>"+
                        "<small id=\"namaPenghuni\">"+namas+"</small><br><br>"+
                        "Kamu masuk tanggal <b id=\"tanggalMasuk\">"+d.getDate()+" "+months[d.getMonth()]+" "+d.getFullYear()+"</b><br>"+
                        "Makasih yah sudah ngekos disini. :)!<br><br>"+
                        "<b>ttd</b><br>"+
                        "Kost Setia Premiere<br><br><br>"+
                      "<center>"+
                        "<a href=\"/et_page\"><button type=\"button\" class=\"form-control\" style=\"width:30%; margin-top:20px; background-color:orange;color:white;\">Refresh</button></a>"+
                        "<a href=\"/\"><button type=\"submit\" class=\"form-control\" style=\"width:30%; margin-top:20px; background-color:orange;color:white;\">Back</button></a>"+
                      "</center>"+
                    "</p>"+
                  "</div>"+
                "</div>"+
              "</div>";
            
              $("#result").empty();
              $("#result").append(tr_strfa);
            }

              //Untuk menampilkan jatuh tempo
                var jatuhtempo = response['datakeuangan']['jatuhtempo'];
                response['datakeuangan']['jatuhtempo'] = Date.parse(response['datakeuangan']['jatuhtempo']);
                var jatuhtempo = response['datakeuangan']['jatuhtempo'];
                var date_jt = new Date(jatuhtempo);
                var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
                var day = d.getDate();
                var month = d.getMonth()+1;
                if(day<10){
                    day = "0"+d.getDate();
                }
                if((month+1)<10){
                    month = "0"+month;
                }
              
              var tr_strsu = 
              "<div class=\"row\">"+
                "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                  "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                    "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                        "<b>Hai, kak</b><br>"+
                        "<small id=\"namaPenghuni\">"+namas+"</small><br><br>"+
                        "Kamu masuk tanggal <b id=\"tanggalMasuk\">"+d.getDate()+" "+months[d.getMonth()]+" "+d.getFullYear()+"</b><br>"+
                        "dan sudah 'Ngekos' Selama <br><br>"+
                        "<span style=\"padding:10px;background-color:#33B7FF;border-radius:20px;color:white;display:block;\"><b id=\"lamaNgekos\">"+getAge(month+"/"+day+"/"+d.getFullYear())+"</b></span><br><br>"+
                        "Jatuh Tempo Selanjutnya <br><b style=\"font-size:2em;\" id=\"jatuhTempo\">"+date_jt.getDate()+" "+months[date_jt.getMonth()]+" "+date_jt.getFullYear()+"</b><br>"+
                        "<br><br><br><b>ttd</b><br>"+
                        "Premiere Kost<br><br><br>"+
                      "<center>"+
                        "<a href=\"/et_page\"><button type=\"button\" class=\"form-control\" style=\"width:30%; margin-top:20px; background-color:orange;color:white;\">Refresh</button></a>"+
                        "<a href=\"/\"><button type=\"submit\" class=\"form-control\" style=\"width:30%; margin-top:20px; background-color:orange;color:white;\">Back</button></a>"+
                      "</center>"+
                    "</p>"+
                  "</div>"+
                "</div>"+
              "</div>";
            
              $("#result").empty();
              $("#result").append(tr_strsu);
            }
               
              
           }else{
             var tr_strnodata = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                    "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                      "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<b>Data Penghuni Tidak Ditemukan, Silahkan tanyakan username dan tag mu ke penjaga kos yah! </b><br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";
                
                $("#result").empty();
                $("#result").append(tr_strnodata);
           }
         },
         error: function(response){
            if (response['datapenghuni'] === undefined || response['datapenghuni'].length == 0 || response['jumlahdata'] == '0' ) {
              var tr_strerror = 
                "<div class=\"row\">"+
                  "<div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\" style=\"background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;\">"+
                    "<div style=\"background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;\">"+
                      "<p style=\"font-family: 'Montserrat', sans-serif;display:inline; color:#ff9f43;font-size:1.5em\">"+
                          "<b>Maaf Data Penghuni Tidak Ditemukan, silahkan cek username dan tag anda  "+response['jumlahdata']+" "+response['datapenghuni']+"</b><br>"+
                      "</p>"+
                    "</div>"+
                  "</div>"+
                "</div>";
                
                $("#result").empty();
                $("#result").append(tr_strerror);
            }
         }
       });
  }
</script>

  
@endsection