@extends('layout.nonapp')

@section('metadesc','Yuk lihat profil kos-kosan Premiere Group Kost supaya tahu lebih detailnya!')

@section('judul','Profil Premiere Kost - Yuk Kenalan!')


@section('isibodi')
  
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #ff9f43; text-align: center; padding: 80px 20px 20px 20px;position: relative;">
          <div class="bnr-mami">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:inline; color:white;">
                <b>Profil Premiere Kost</b><br>
                <small>Yuk Kenalan!</small>
              </p>
            </center>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
          <center>
            <img src="https://t3.ftcdn.net/jpg/00/64/67/52/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg" style="border-radius:100%; width:40px;">
          </center>
        </div>
      </div>
    </div> 

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
          <center>
            <p class="profil_head">Sejak 2010</p><br>
            <div class="profil_body">
              <p class="profil_body">
                Premiere Kost Group telah melayani selama {{date('Y',strtotime('now'))-2010}} Tahun.
                Berbagai pengalaman kami peroleh dalam melayani para penghuni dengan baik dan nyaman.
                Fasilitas dan bangunan pun terus direnovasi agar menghadirkan suasana baru yang segar.
                Kami memberikan fasilitas yang lengkap dan kost yang bersih, aman, dan nyaman 
                namun tetap memegang prinsip untuk mempertahankan 
                kost murah di Jakarta dan juga kost murah di Makassar .
                </p>
            </div>
          </center>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 " style="background-color: #ff9f43; text-align: center; padding: 20px;position: relative;">
          <center>
              <p style="font-family: 'Montserrat', sans-serif;display:inline;font-size:1.4em; color:white;" ><b>38 Kamar</b>
              </p><br>
          </center>
        </div>
        <div class="col-md-4 col-lg-4 ccol-sm-12 col-xs-12 " style="background-color: #ff9f43; text-align: center; padding: 20px;position: relative;">
          <center>
              <p style="font-family: 'Montserrat', sans-serif;display:inline;font-size:1.4em;color:white;" ><b>>500 Penghuni</b>
              </p><br>
          </center>
        </div>
        <div class="col-md-4 col-lg-4 ccol-sm-12 col-xs-12 " style="background-color: #ff9f43; text-align: center; padding: 20px;position: relative;">
          <center>
              <p style="font-family: 'Montserrat', sans-serif;display:inline;font-size:1.4em;color:white;" ><b>2 Kota Besar</b>
              </p><br>
          </center>
        </div>
      </div>
    </div> 


    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 " style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
          <center>
            <h1 style="padding:20px;color:#929292;">Kost Setia Premiere</h1>
            
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <div class="w3-content w3-display-container">
                <img class="mySlides" src="https://static.mamikos.com/uploads/cache/data/style/2019-10-07/WhloY6EF-540x720.jpg" style="width:100%">
                <img class="mySlides" src="https://static.mamikos.com/uploads/cache/data/style/2019-10-07/nThIpWTL-540x720.jpg" style="width:100%">
                <img class="mySlides" src="https://static.mamikos.com/uploads/cache/data/style/2019-10-07/nnmrtunw-540x720.jpg" style="width:100%">
                <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
              </div>
            </div>

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #f8f8f8; text-align: center;position: relative;">
              <h2 style="color:#929292;">Rp. 1.300.000</h2><small>per Bulan</small>
            </div>

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #f8f8f8; text-align: center; padding: 5px;position: relative;">
              <h4 style="padding:5px;color:#929292;">Kost Murah di Jakarta dengan fasilitas lengkap</h4>
            </div>
            
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <div class="mapouter">
                  <div class="gmap_canvas">
                    <iframe width="100%" height="303" id="gmap_canvas" src="https://maps.google.com/maps?q=kost%20setia%20premiere&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                    </iframe>
                  </div>
                  <style>.mapouter{position:relative;text-align:right;height:303px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:303px;width:100%;margin-top:20px;padding:50px;}</style>
              </div>
              <a class="dropdown-item" href="#" style=" text-align: center; margin-bottom: 10px;  background-color: #ffd828;box-shadow:0px 15px 25px #ffd828;">
                <p style="font-size: 1.2em;color:white;">Lokasi Kost Setia Premiere</p>
              </a>
              <a class="dropdown-item" href="https://www.google.com/maps?saddr=My+Location&daddr=Kost+Setia+Premiere,+Jalan+Setia+1,+Jatiwaringin,+Bekasi+City,+West+Java" style=" text-align: center; background-color: #f8ce14;box-shadow:0px 15px 25px #f8ce14;">
                <p style="font-size: 1.2em;color:white;">-Arahkan Saya ke Kost-</p>
              </a>
              <a class="dropdown-item" href="https://wa.me/6287880103866" style=" text-align: center; margin-bottom: 30px;  background-color: #00a015;box-shadow:0px 15px 25px #00a015;border-bottom-left-radius:40px;border-bottom-right-radius:40px;">
                <p style="font-size: 1.2em;color:white;">-Whatsapp Pengelola Kost-</p>
              </a>
            </div>

            {{-- Baris Pertama - Laptop --}}
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 " style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-wifi" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;"><b> Wifi Ngegas</b></p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-bed" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;"><b> Springbed, AC, dll</b></p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-utensils" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;"> <b>Dapur Komplit</b> </p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-road" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;"> <b>Akses Tol Jakarta</b> </p>
            </div>
            {{-- Baris Kedua - Laptop --}}
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-car" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;"> <b>Parkir Mobil Motor</b> </p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-toilet" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;"> <b>KM Dalam</b> </p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-shower" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;"> <b>Air Bersih Lancar</b> </p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #f8f8f8; text-align: center; padding: 20px;position: relative;">
              <i class="fa fa-home" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;"> <b>Warung Murah</b> </p>
            </div>
            
          </center>
          
        </div>

        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 " style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
          <center>
            <h1 style="padding:20px;color:white;">Kost Pelita Premiere</h1>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
              
              <div class="w3-content w3-display-container">
                <img class="mySlidesPelita" src="https://static.mamikos.com/uploads/cache/data/style/2018-08-18/vFJXeGFT-540x720.jpg" style="width:100%">
                <img class="mySlidesPelita" src="https://static.mamikos.com/uploads/cache/data/style/2018-08-18/QS4uDgI6-540x720.jpg" style="width:100%">
                <img class="mySlidesPelita" src="https://static.mamikos.com/uploads/cache/data/style/2018-08-18/ZFo1J2va-540x720.jpg" style="width:100%">
                <img class="mySlidesPelita" src="https://static.mamikos.com/uploads/cache/data/style/2018-08-18/VmaKOW1y-540x720.jpg" style="width:100%">
                <button class="w3-button w3-black w3-display-left" onclick="plusDivsPelita(-1)">&#10094;</button>
                <button class="w3-button w3-black w3-display-right" onclick="plusDivsPelita(1)">&#10095;</button>
              </div>
            </div>
            
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #c9c9c9; text-align: center;position: relative;">
              <h2 style="color:#3a3a3a;">Rp. 1.400.000</h2><small>per Bulan</small>
            </div>

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #c9c9c9; text-align: center; padding: 5px;position: relative;">
              <h4 style="padding:5px;color:#3a3a3a;">Kost Paling Nyaman di Pusat Kota Makassar</h4>
            </div>

            
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
            <div class="mapouter">
              <div class="gmap_canvas">
                <iframe width="100%" height="303" id="gmap_canvas" src="https://maps.google.com/maps?q=kost%20pelita%20premiere%20makassar&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
              </div>
                <style>.mapouter{position:relative;text-align:right;height:303px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:303px;width:100%;}</style>
            </div>
            <a class="dropdown-item" href="#" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
              <p style="font-size: 1.2em;color:white;">Lokasi Kost Pelita Premiere</p>
            </a>
            <a class="dropdown-item" href="https://www.google.com/maps?saddr=My+Location&daddr=Kost+Pelita+Premiere,+Jalan+Pelita+Raya+6,+Bua+Kana,+Makassar+City,+South+Sulawesi" style=" text-align: center; background-color: #f8ce14;box-shadow:0px 15px 25px #f8ce14;">
              <p style="font-size: 1.2em;color:white;">-Arahkan Saya ke Kost-</p>
            </a>
            <a class="dropdown-item" href="https://wa.me/6282114500157" style=" text-align: center; margin-bottom: 30px;  background-color: #00a015;box-shadow:0px 15px 25px #00a015;border-bottom-left-radius:40px;border-bottom-right-radius:40px;">
              <p style="font-size: 1.2em;color:white;">-Whatsapp Pengelola Kost-</p>
            </a>
          </div>
            
            {{-- Baris Pertama - Laptop --}}
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 " style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-wifi" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;color: #3a3a3a;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;color: #3a3a3a;"><b> Wifi Ngegas</b></p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-bed" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;color: #3a3a3a;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;color: #3a3a3a;"><b> Springbed, AC, dll</b></p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-utensils" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;color: #3a3a3a;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;color: #3a3a3a;"> <b>Dapur Komplit</b> </p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-road" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;color: #3a3a3a;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;color: #3a3a3a;"> <b>Akses Tengah Kota</b> </p>
            </div>
            {{-- Baris Kedua - Laptop --}}
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-car" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;color: #3a3a3a;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;color: #3a3a3a;"> <b>Parkir Mobil Motor</b> </p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-toilet" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;color: #3a3a3a;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;color: #3a3a3a;"> <b>KM Dalam</b> </p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
              <i class="fas fa-shower" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;color: #3a3a3a;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;color: #3a3a3a;"> <b>Air Bersih Lancar</b> </p>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6" style="background-color: #c9c9c9; text-align: center; padding: 20px;position: relative;">
              <i class="fa fa-home" style="padding-top:20px;padding-bottom :20px;font-size: 2.5em;color: #3a3a3a;"></i>
              <p style="font-family: 'Montserrat', sans-serif;padding-bottom :20px;color: #3a3a3a;"> <b>Warung Murah</b> </p>
            </div>

          </center>
        </div>
      </div>
    </div> 

    
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #a8a8a8; text-align: center; padding: 10px 10px 20px 20px;position: relative;">
            <center>
              <p style="font-family: 'Montserrat', sans-serif;display:inline;font-size:1.2em;margin-top:5px;color:white;" >
              Terima kasih <br>atas kepercayaan penghuni kost selama ini!<br>
               </p>
            </center>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color: #707070; text-align: center; padding: 10px 10px 20px 20px;position: relative;">
            <center>
              <p style="color:#ff9f43;font-size:1.5em"><b>Yuk Survey ke Kost ! </b></p>
              <a href="https://wa.me/6282114500157" target="_top"><button style="background-color:#ff9f43;padding:10px 70px 10px 70px;color:white;border-radius:20px;"><b>Buka</b></button></a>
            </center>
        </div>
      </div>
    </div>
        
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 bnr-mami" style="background-color: #525252; text-align: center; padding: 20px;">
            <center>
              <p style="font-family: 'Bulgatti';display:inline; color:white;"><b> P r e m i e r e  K o s t .com</b></p>
            </center>
        </div>
      </div>
    </div>

    

    <div class="col-sm-12 col-lg-12">
      <div class="modal fade" id="ModalMap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: 2px dashed white">
              <center><h1 style="color:white;" ><b>Lokasi Kost</b></h1></center>
            </div>
            <div class="modal-body">
              <div class="mapouter">
                <div class="gmap_canvas">
                  <iframe width="100%" height="303" id="gmap_canvas" src="https://maps.google.com/maps?q=kost%20setia%20premiere&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                  </iframe>
                </div>
                <style>.mapouter{position:relative;text-align:right;height:303px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:303px;width:100%;;}</style>
              </div>
              <a class="dropdown-item" href="#" style=" text-align: center; margin-bottom: 10px;  background-color: #ffd828;box-shadow:0px 15px 25px #ffd828;">
                <p style="font-size: 1.2em;color:white;">Google Maps Kost Setia Premiere</p>
              </a>

              <div class="mapouter">
                <div class="gmap_canvas">
                  <iframe width="100%" height="303" id="gmap_canvas" src="https://maps.google.com/maps?q=kost%20pelita%20premiere%20makassar&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </div>
                  <style>.mapouter{position:relative;text-align:right;height:303px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:303px;width:100%;}</style>
              </div>
              <a class="dropdown-item" href="#" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Google Maps Kost Pelita Premiere</p>
              </a>
              <div class="divider"></div>
              <button type="button" class="dropdown-item" style="border: 2px dashed white; text-align: center; margin-bottom: 10px;   background-color: #1ED760;" data-dismiss="modal" aria-label="Close">
                <label style="font-size: 25px;">Kembali</label>
              </button>
              </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="ModalSoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: 2px dashed white">
              <center><h1 style="color:white;" ><b>Tetap Terhubung Yuk!</b></h1></center>
            </div>
            <div class="modal-body">

              <a class="dropdown-item" href="#" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Instagram</p>
              </a>
              <blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BioVRHGFoCj/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/BioVRHGFoCj/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BioVRHGFoCj/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Premiere Kost Group (@premieregroup.kost)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2018-05-11T08:19:06+00:00">May 11, 2018 at 1:19am PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
              
              <a class="dropdown-item" href="https://wa.me/6282114500157" target="_top" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Whatsapp</p>
              </a>
              
              <div class="divider"></div>
              <button type="button" class="dropdown-item" style="border: 2px dashed white; text-align: center; margin-bottom: 10px;   background-color: #1ED760;" data-dismiss="modal" aria-label="Close">
                <label style="font-size: 25px;">Kembali</label>
              </button>
              </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="ModalDev" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: 2px dashed white">
              <center><h1 style="color:white;" ><b>Web Dev Profile</b></h1></center>
            </div>
            <div class="modal-body" style="text-align:center;">
              <img src="https://t3.ftcdn.net/jpg/00/64/67/52/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg" style="border-radius:100%; width:40%;">
              
              <h2><b>Ahyar</b></h2>
              <div style="background-color:white;border-radius:20px;margin-top:30px;text-align:center;padding:50px;">
              <p style="">
                <br><br>
                Hello there!<br>
                Thank you for viewing my dev profile.<br><br>
                
                I present this website to my parents who raise me 'til now,
                which also the owner of Premiere Kost Group. Paralel with my thesis, 
                i develop this system as a whole from the use case, design, coding.
                If you interested to discuss about something technical, 
                or even a <b>Job Opportunity</b> I will really appreciate it! :)<br><br>

                Feel free to reach me through my social media below.<br>
                Thank you! <br><br>

                
                <small style="width:100%;">I hope you could find enough information in the website.</small><br>
                If you not, please contact the owner by click <a href="https:/wa.me/6282114500157" style="color:#ff9f43;">here</a>
                <br><br><br><br>
              </p>
              </div>
                
              <center><a href="https://www.linkedin.com/in/muhammad-ahyar"><img src="https://images.vexels.com/media/users/3/137382/isolated/preview/c59b2807ea44f0d70f41ca73c61d281d-linkedin-icon-logo-by-vexels.png" style="width:20%;display:block;margin-top:20px;margin-bottom:20px;"></a></center>
              
                
              
              <div class="divider"></div>
              <button type="button" class="dropdown-item" style="border: 2px dashed white; text-align: center; margin-bottom: 10px;   background-color: #1ED760;" data-dismiss="modal" aria-label="Close">
                <label style="font-size: 25px;">Kembali</label>
              </button>
              </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="ModalMami" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: 2px dashed white">
              <center><h1 style="color:white;" ><b>Web Dev Profile</b></h1></center>
            </div>
            <div class="modal-body">
              <a href="https://mamikos.com/room/kost-bekasi-kost-campur-murah-kost-hijau-type-a-pondok-gede-bekasi#/" target="_blank"><img src="https://static.mamikos.com/uploads/cache/data/style/2019-10-07/WhloY6EF-540x720.jpg" style="width:100%;"></a>
              <a class="dropdown-item" href="https://mamikos.com/room/kost-bekasi-kost-campur-murah-kost-hijau-type-a-pondok-gede-bekasi#/" target="_blank" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Kost Setia Premiere Jakarta</p>
              </a>
              <a href="https://mamikos.com/room/kost-makassar-kost-campur-eksklusif-kost-pelita-premiere-rappocini-makassar#/" target="_blank"><img src="https://static.mamikos.com/uploads/cache/data/style/2018-08-18/EUVGZYRB-540x720.jpg" style="width:100%;"></a>
              <a class="dropdown-item" href="https://mamikos.com/room/kost-makassar-kost-campur-eksklusif-kost-pelita-premiere-rappocini-makassar#/" target="_blank" style=" text-align: center; margin-bottom: 10px;   background-color: #ffa928;box-shadow:0px 15px 25px #ffa928;">
                <p style="font-size: 1.2em;color:white;">Kost Pelita Premiere Makassar</p>
              </a>
            </div>
          </div>
        </div>
      </div>

    </div>

    <!--HEADER END-->
@endsection

@section('footer')

<script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript">
    // When the user scrolls the page, execute myFunction
  window.onscroll = function() {myFunction()};
  $( window ).on( "load", myFunction );
  // Get the header
  var header = document.getElementById("navigasistick");

  // Get the offset position of the navbar
  var sticky = header.offsetTop;

  // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("navbar-custom-stick");
    } else {
      header.classList.remove("navbar-custom-stick");
    }
  }

  var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

  var slideIndex = 1;
showDivsPelita(slideIndex);

function plusDivsPelita(n) {
  showDivsPelita(slideIndex += n);
}

function showDivsPelita(n) {
  var i;
  var x = document.getElementsByClassName("mySlidesPelita");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>

  
@endsection