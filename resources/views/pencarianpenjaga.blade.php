@extends('layout.app')


@section('judul', 'Dashboard Overview Kos')


@section('isibodi')
        <div class="row">


      <div id="menukecil" class="stickykeuangan" style="display: inline-block;">
          <div class="col-xs-3 col-sm-3 col-md-3" style="display: inline-block;">
                  <button class="btn-lg"  onclick="window.location.href=('/keuangan')" style="background-color: white;color:orange; border-radius:50px; width: 100%; border: 2px dashed white;">
                              <i class="fa fa-home"></i>
                  </button> 
          </div>
          <div class="col-xs-9 col-sm-9 col-md-9" style="display: inline-block;">            
                  <button class="btn-lg" data-toggle="modal" data-target="#searchdata" style="background-color: white;color:orange; border-radius:50px; width: 100%; padding: 20px; border: 2px dashed orange;">
                  <i class="fa fa-search"></i>
                  </button> 
          </div>
      </div>

                <div class="col-xs-12 col-sm-12 col-md-12">  
                  <!-- Bagian Rincian Keuangan -->
                      <div class="col-xs-12 col-sm-12 col-md-12">    
                          <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: orange;color:white;text-align: center;margin-top: 10px;">
                                <label>Rincian Keuangan</label> 
                            </div>            
                            <div class="panel-body">
                            <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 10px;"> 
                              <div style="background-color: orange; padding: 10px 10px 10px 10px; margin-top: 20px; border-radius: 10px; color:white;">
                                <select class="form-control" id="search" >
                                <option value="">Semua</option>
                                @php $count=0; $harian=0; @endphp                  
                                  @foreach($data_pencarian as $rowcari)    
                                    @if(date("d",strtotime($rowcari->tanggaltransaksi))!=$harian)        
                                    @php $harian = date("d",strtotime($rowcari->tanggaltransaksi)); @endphp                        
                                    <option value="{{$rowcari->tanggaltransaksi}}">{{date("d F Y",strtotime($rowcari->tanggaltransaksi))}}</option>
                                    @endif
                                  @endforeach   
                                </select>
                              </div>

                              <div class="scrollfield" >                                
                                <table style="margin-top: 10px; " width="100%"> 
                                  <tbody id="datauang">
                                    @php $count=0; $hari=0; @endphp
                                    @if(!empty($data_pencarian))                                    
                                      @foreach($data_pencarian as $rowcari)                         
                                          @if($rowcari->statustransaksi=='sudah') 
                                            @php $count++ @endphp     

                                        @if(date("d",strtotime($rowcari->tanggaltransaksi))!=$hari)
                                          @php $hari = date("d",strtotime($rowcari->tanggaltransaksi)); @endphp
                                            <tr style=" border:2px solid orange; width: 100%; background-color: orange;">
                                              <td colspan="3">
                                                <h4 style=" color: white;">
                                                  <center>
                                                    {{date("d F Y",strtotime($rowcari->tanggaltransaksi))}}<br>
                                                    <p hidden>{{$rowcari->tanggaltransaksi}} </b></p>
                                                  </center>
                                                </h4>
                                              </td>
                                            </tr>
                                          @endif
                                            <tr style=" border:2px solid orange; 
                                              @if((strtotime('now')>=strtotime($rowcari->tanggaltransaksi))&&(strtotime('-4 days')<=strtotime($rowcari->tanggaltransaksi)))
                                              border-bottom: 0px;
                                              @endif
                                              ">
                                              <td rowspan="
                                              @if((strtotime('now')>=strtotime($rowcari->tanggaltransaksi))&&(strtotime('-4 days')<=strtotime($rowcari->tanggaltransaksi)))
                                              2
                                              @else
                                              1
                                              @endif
                                              " width="40%" style="vertical-align: middle; background-color: 
                                              @if($rowcari->pilihantransaksi=='pemasukkan' )
                                              green
                                              @elseif($rowcari->pilihantransaksi=='pengeluaran')
                                              red
                                              @endif
                                              ;">
                                              <center>
                                                <h3 style="padding:10px;">
                                                  <p style=" color: white;"><b>
                                                    <span style="font-size: 2.4vw;"> Rp. </span><span style="font-size: 3vw;"> {{number_format($rowcari->nominaltransaksi)}} </span></b>
                                                  </p>
                                                </h3>
                                              </center>
                                              </td>
                                              <td>
                                                <h5 style="padding-left: 10px;">
                                                  {{$rowcari->tanggaltransaksi}} <br>
                                                  {{$rowcari->deskripsi}} ({{$rowcari->metode}})<br>
                                                </h5>
                                              </td>   
                                              @if((strtotime("now")>=strtotime($rowcari->tanggaltransaksi))&&(strtotime("-4 days")<=strtotime($rowcari->tanggaltransaksi)))
                                              <tr class="cek" style="border-right:2px solid orange;"><td>
                                                <center>
                                                @if($rowcari->pilihantransaksi=='pengeluaran')
                                                    @php $arrayku = ['id'=>$rowcari->id, 'idpenghuni'=>'0','idkamar'=>'0','pilihan'=>$rowcari->pilihantransaksi,'jenis'=>$rowcari->jenistransaksi]; @endphp
                                                    <form class="delete_form" action="{{action('KeuanganController@destroy',$arrayku)}}" method="get">
                                                    <button type="submit" style="width: 60%; margin-top: 10px;margin-bottom: 10px;" class="btn btn-danger" onclick="" style="float: left; margin-left:4px;" >                      
                                                      Hapus <i class="fa fa-trash"></i>
                                                    </button>
                                                    </form>
                                                @elseif($rowcari->pilihantransaksi=='pemasukkan')
                                                    @if($rowcari->pilihantransaksi=='pemasukkan'&&$rowcari->jenistransaksi=='lainnya')
                                                        @php $arrayku = ['id'=>$rowcari->id,'idpenghuni'=>'0','idkamar'=>'0','pilihan'=>$rowcari->pilihantransaksi,'jenis'=>$rowcari->jenistransaksi]; @endphp
                                                    @else
                                                      @php $arrayku = ['id'=>$rowcari->id,'idpenghuni'=>$rowcari->idpenghuni,'idkamar'=>$rowcari->idkamar,'pilihan'=>$rowcari->pilihantransaksi,'jenis'=>$rowcari->jenistransaksi]; @endphp
                                                    @endif
                                                    <form class="delete_form" action="{{action('KeuanganController@destroy',$arrayku)}}" method="get">
                                                    <button type="submit" style="width: 60%; margin-top: 10px;margin-bottom: 10px;" class="btn btn-danger" onclick="" style="float: left; margin-left:4px;" >                      
                                                      Hapus<i class="fa fa-trash"></i>
                                                    </button>
                                                    </form>                                          
                                                @endif
                                              </center>
                                              </td> </tr>      
                                              @endif                    
                                            </tr>
                                        @endif
                                      @endforeach
                                    </tbody>
                                  </table>
                                @endif


                                @if($data_pencarian==null)
                                <p>Data Tidak Ditemukan</p>
                                @endif

                              </div> <!-- scrollfield -->

                                  <hr style="border: 2px dotted orange">
                                  <center><b style="color:orange">Hasil pencarian tanggal {{$tanggalawal}} hingga {{$tanggalakhir}} ( {{count($data_pencarian)}}  Transaksi Ditemukan)</b></center>
                                  <hr style="border: 2px dotted orange; margin-bottom: 100px;">
                            </div>
                            </div> <!-- panelbody -->
                            </div"> <!-- paneldefault -->
                      </div>      
                      
                  <!-- Bagian Data Kamar -->
                </div>

                 
        </div>


@endsection

@section('tambahan')

<div class="modal fade" id="searchdata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <center><h1 style="color:white;" class="modal-title" id="exampleModalLabel"><b>Financial Search</b></h1></center>  
              </div>
              <div class="modal-body">
              <form action="{{'\caritanggalpenjaga'}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}} 
                  <label>Dari</label>
                  <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Awal" name="from" required style="margin-bottom: 10px;"> 
                  <label>Ke</label>
                  <input type="date" class="form-control" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" placeholder="Silahkan Masukkan Tanggal Akhir" name="to" required>                    
                      <button class="btn btn-lg btn-success" style="margin-bottom: 30px; margin-top: 20px;  width: 100%; padding: 20px; background-color: orange;">Search</button>
                    <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
              </form>
              </div>
          </div>
        </div>
      </div>

@endsection

@section('footer')

    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/respond.min.js') }}"></script>
    <script>



      $(document).ready(function() {
      $('.delete_form').on('submit',function(){
        var pass = prompt("Silahkan Ketik \"SAYA YAKIN HAPUS\" :", "");
        if (pass == 'SAYA YAKIN HAPUS') {
          return true;
        } else {
          return false;
        }
      });
    });

      $(document).ready(function(){
      $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#datauang tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
        document.getElementById("#datauang").style.marginTop = "0px";
      });
    });
    </script>

@endsection