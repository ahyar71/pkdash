@extends('layout.app')



@section('judul','Data Aset')


@section('isibodi')

<div class="container" style="padding-bottom: 40px;" >
  <div class="row">
      <div class="col-md-3 col-sm-12 col-lg-3">
            <div class="panel panel-default">
              <div class="panel-heading" style="background-color: #1ED760; border: 2px solid white; color:white; ">
                Total Aset Saat ini
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal" style="float: right; border-radius: 50px;">
                    <i class="fa fa-plus"></i>
                </button>
              </div>
              <div class="panel-body" style="text-align:center;">
                <h4 style="color:white;"><b></b></h4>
                <h1>{{count($data_aset)}}</h1>
              </div>
            </div>

        <div class="panel panel-teal">
          <div class="panel-heading">
            <h3><b>Daftar Aset</b></h3>
          </div>
          <div class="panel-body">
            <select id="idkamarkost" name="idkamar" required="" class="form-control" style="margin-top: 10px; background-color: #3e89d8; color: white;">
              <option id="pilkamar" value="" selected="" disabled="">Pilih Aset</option>
                @if(!empty($data_aset)) 
                    @foreach ($data_aset as $rowaset)
                        <option class="form-control" value="{{$rowaset->id}}"> Kamar {{$rowaset->getNamaKamar()}} - {{strtoupper($rowaset->nama)}} </option>
                    @endforeach
                @else
                   <option id="kamarnotfound" value="" disabled selected="">Pilihan Kamar Tidak Ada</option>
                @endif
                   </select>
          </div>
        </div>
      </div>
        
      <div class="col-md-9 col-sm-12 col-lg-9">
        <div id="default" class="default">
          <div class="panel panel-ateal">
            <div class="panel-heading">
              <h3 id="judulkamar"><b>Aset</b></h3>
              <hr>
            </div>
            <div class="panel-body">
              <div class="detail">
                <h3>Keterangan Detail</h3>
                <h3><b> . . . .</b></h3>
                <h3 class="kosong"><b>BELUM TERDATA</b></h3>
              </div>
            </div>
          </div>
        </div>
        @if(!empty($data_aset))
        @foreach ($data_aset as $rowaset)
        <div id="show{{$rowaset->id}}" class="myKamar">
          <div class="panel panel-ateal">
            <div class="panel-heading">
              <h3 id="judulkamar"><b>{{strtoupper($rowaset->nama)}} Kamar {{$rowaset->getNamaKamar()}}</b></h3>
              <hr>
            </div>
            <div class="panel-body">
              <div class="detail">

                <h3>Waktu Pembelian</h3>
                @if(!empty($rowaset->waktupembelian))
                  <h3><b>{{date('d F Y' ,strtotime($rowaset->waktupembelian))}}</b></h3>
                @elseif(empty($rowaset->waktupembelian))
                  <h3><b> - </b></h3>
                @endif

                <h3>Waktu Service Aset Terakhir</h3>
                @if(!empty($rowaset->waktuservis))
                  <h3><b>{{date('d F Y' ,strtotime($rowaset->waktuservis))}}</b></h3>
                @elseif(empty($rowaset->waktuservis))
                  <h3><b> - </b></h3>
                @endif

                <h3>Jangka Waktu Service Aset (Bulan)</h3>
                @if(!empty($rowaset->jangkaservice))
                  <h3><b>{{$rowaset->jangkaservice}}</b></h3>
                @elseif(empty($rowaset->jangkaservice))
                  <h3><b> - </b></h3>
                @endif

              </div>
                    <div class="input-container">
                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalEdit{{$rowaset->id}}" style="float: right; border-radius: 50px; width:80%;">
                                Edit Aset
                            </button>
                            @php $arraydata = ['id'=>$rowaset->id];@endphp
                          <form class="delete_form" action="{{action('asetcontroller@destroy',$arraydata)}}" method="get" style="width: 20%;" >
                            {{csrf_field()}}
                            <center>
                              <button type="submit" class="btn btn-secondary" style="background-color:grey; opacity:0.5;float: right; border-radius: 50px; width:100%;">
                                Hapus
                              </button>
                            </center>
                          </form>
                        </div>

            </div>
          </div>
        </div>
        @endforeach
        @endif
      </div>

      </div>
    </div>
      

@endsection
@section('tambahan')
        <!-- Modal -->
        <div class="col-sm-12 col-md-12">
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
          </button>
          <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Tambah Aset</b></h1></center>
                  </div>
              <div class="modal-body">
                <form action="/aset/create" id="formcari" method="POST">
                  {{csrf_field()}}
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Aset</label>
                      <select required class="form-control" name="nama">
                        <option selected value="" disabled >Pilih Aset</option>
                        <option value="ac">AC</option>
                        <option value="lemari">Lemari</option>
                        <option value="tv">TV</option>
                        <option value="springbed">Spring Bed</option>
                        <option value="meja">Meja</option>
                        <option value="kursi">Kursi</option>
                        <option value="bantal">Bantal</option>
                        <option value="sprei">sprei</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Pilih Kamar</label>
                      <select required class="form-control" name="idkamar">
                        <option selected value="" disabled >Pilih Kamar</option>
                        @foreach($data_kamar as $rowkamar)
                          <option value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Waktu Beli</label>
                      <span class="pull-right"><input type="checkbox" id="cekbeli" name="cekbeli"> <label>Abaikan</label></span>
                      <input name="waktupembelian" type="date" class="form-control"  aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Waktu Service Terakhir</label>
                      <span class="pull-right"><input type="checkbox" id="cekservis" name="cekservis"> <label>Tidak Diservis</label></span>
                      <input name="waktuservis" type="date" class="form-control"  aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jangka Waktu Servis</label>
                      <input name="jangkaservice" type="number" class="form-control"  aria-describedby="emailHelp">
                    </div>
                    
                      <span >
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                          <button class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                          </center>
                        </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                          <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>   
            </form>
            </div>
          </div>
        </div>
    </div>
  </div>

        @foreach($data_aset as $rowaset)
        <!-- Modal -->

        <div class="col-sm-12 col-md-12">
        <div class="modal fade" id="modalEdit{{$rowaset->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
          </button>

          <div class="modal-dialog" role="document">
            <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Edit Aset</b></h1></center>
                  </div>
              <div class="modal-body">
                <form action="/aset/edit" method="get">
                  {{csrf_field()}}

                    <div class="form-group">
                    <label for="exampleInputEmail1">Jenis Aset</label>
                    <input name="nama" required type="text" class="form-control" required  aria-describedby="emailHelp" value="{{$rowaset->nama}}">
                      <input name="id" hidden type="text" value="{{$rowaset->id}}">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Pilih Kamar</label>
                    <select required class="form-control" name="idkamar">
                      <option selected value="" disabled >Pilih Kamar</option>
                      @foreach($data_kamar as $rowkamar)
                          @if($rowkamar->id==$rowaset->idkamar)
                            <option  class="form-control" selected value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                          @else
                            <option  class="form-control" value="{{$rowkamar->id}}">{{$rowkamar->namakamarkost}}</option>
                          @endif
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">
                      <label for="exampleInputEmail1">Waktu Beli</label>
                      <span class="pull-right"><input type="checkbox" id="cekbeli" name="cekbeli"> <label>Abaikan</label></span>
                      <input name="waktupembelian" required type="date" class="form-control"  aria-describedby="emailHelp" value="{{$rowaset->waktupembelian}}">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Waktu Service Terakhir</label>
                      <span class="pull-right"><input type="checkbox" id="cekservis" name="cekservis"> <label>Tidak Diservis</label></span>
                      <input name="waktuservis" required type="date" class="form-control"  aria-describedby="emailHelp" value="{{$rowaset->waktuservis}}">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jangka Waktu Servis</label>
                      <input name="jangkaservice" required  type="number" class="form-control"  aria-describedby="emailHelp" value="{{$rowaset->jangkaservice}}">
                    </div>
                    
                      <span>
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                          <button type="submit" class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>   

                  </form>
              </div> 
            </div>
          </div>
        </div>
        </div>
        @endforeach
@endsection

@section('footer')


    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>



  <script>


    //fungsi detail daftar kamar
    $(document).ready(function(){
    var demovalue2 = 'a'; //b
    $("div.myKamar").hide();
      $('#idkamarkost').on('change', function(){
        var demovalue = $(this).val();
            if(demovalue2=='a'){
              demovalue2 = demovalue; 
            }
            if(demovalue2!=demovalue){
                  $("#default").hide();
                  $("#default2").hide();
                  $("#show"+demovalue2).hide();
                  $("#buka"+demovalue2).hide();
                  $("#show"+demovalue).show();
                  $("#buka"+demovalue).show();
                demovalue2 = demovalue;
            }
            if(demovalue2==demovalue){
                  $("#default").hide();
                  $("#default2").hide();
                  $("#show"+demovalue).show();
                  $("#buka"+demovalue).show();
                demovalue2 = demovalue;
            }
          }
      );
    });

    $(document).ready(function() {
      $('.delete_form').on('submit',function(){
        if(confirm("Yakin Aset Tersebut mau di hapus? Data Tidak dapat dikembalikan"))
        {
          return true;
        }
        else{
          return false;
        }
      });
    });
    

      //show it when the checkbox is clicked
      $('input[name="cekservis"]').on('click', function () {
          if ($(this).prop('checked')) {
              $('input[name="jangkaservice"]').fadeOut();
              $('input[name="waktuservis"]').fadeOut();
              $("input[name='waktuservis']").prop('required',false);
              $("input[name='jangkaservice']").prop('required',false);
          } else {
              $('input[name="jangkaservice"]').fadeIn();
              $('input[name="waktuservis"]').fadeIn();
              $("input[name='waktuservis']").prop('required',true);
              $("input[name='jangkaservice']").prop('required',true);
          }
      });


      //show it when the checkbox is clicked
      $('input[name="cekbeli"]').on('click', function () {
          if ($(this).prop('checked')) {
              $('input[name="waktupembelian"]').fadeOut();
              $("input[name='waktupembelian']").prop('required',false);
          } else {
              $('input[name="waktupembelian"]').fadeIn();
              $("input[name='waktupembelian']").prop('required',true);
          }
      });

  </script>

  
@endsection