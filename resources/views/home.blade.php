
 

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link rel="icon" href="{{ URL::asset('/uploads/icon.png')}}" type="image/x-icon">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

</head>
<body onload="@yield('eventdibody')" style="padding-bottom: 0px;">


<!--HEADER START-->
    <nav class="navbar navbar-def navbar-fixed-top" id="navigasistick" role="navigation">
        <div class="container-fluid" >
            <div class="navbar-header">
                <a class="navbar-brand" href="#"><span style="color: white;"><b>Premiere</b></span>Kost</a>
                <div class="pull-right">
                    <div class="dropdown" id="menuHP">
                      <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="modal" data-target="#ModalMenu"  aria-expanded="false" style="border-radius: 50px;"><i class="fa fa-bars"></i>
                      </button>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </nav>

<div class="container">
<div class="row">
    <div class="col-sm-12 col-lg-12 main">
      <div class="modal fade" id="ModalMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="border-bottom: 2px dashed white">
                    <center><h1 style="color:white;" ><b>Hi {{$data_user->nama}} !</b></h1></center>
                  </div>
              <div class="modal-body">
                        <a class="dropdown-item" href="/login" style=" text-align: center; margin-bottom: 10px;  background-color: #128039;"><label style="font-size: 25px;">Dashboard</label></a>
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#ModalAdu" style="background-color: orange; color:white; text-align: center;"><label style="font-size: 25px;">Pengaduan</label></a><div class="divider"></div>
                        <a class="dropdown-item" href="/logout" style="border: 2px dashed white; text-align: center; margin-bottom: 10px;   background-color: #1ED760;"><label>Logout</label></a>
              </div>
            </div>
          </div>
        </div>
      <div class="modal fade" id="ModalAdu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Trouble</b></h1></center>
                  </div>
              <div class="modal-body">
                      <!--Tombol Submit dan Batal-->
                      <span >
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                            <textarea class="komplain form-control" id="komplain" style="margin-bottom: 20px;"></textarea>
                            <button class="btn btn-lg btn-success" onclick="teswa()"  style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>  
            </div>
          </div>
        </div>
      </div>

<!--HEADER END-->


   <div class="row">
    
    <!-- Banner Add -->
      <div class="col-md-12">
        <div class="banner-add">
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalTambahKost" style="float: right; border-radius: 50px; color:grey; width:100%;">
                               <b>Tambah Kost</b> <i class="fa fa-plus icon"></i>
                            </button>

        </div>
      </div>
    <!--  -->


      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        @include('flash-message')

        @yield('content')
      </div>
    <!-- Daftar Kost -->
    @php  $count=0; @endphp
    @foreach($datakost as $rowkos)
      @php $count++;@endphp
      <div class="listkost col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <a href="/dashboard/kost/{{$rowkos->id}}"><img src="{{ URL::asset('/uploads/'.$rowkos->gambar)}}">
        <p><b>{{$rowkos->namakost}}</b></p></a>
      </div>
    @endforeach
    <!--  -->

    <!-- Banner Add -->
    @if($count==0)
      <div class="col-md-12">
        <div class="banner-add">
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalEdit" style="float: right; border-radius: 50px; width:100%;color:grey;border-radius: 10px; height: 200px;" disabled>
                               <b>Belum ada data kost tersedia</b>
                            </button>

        </div>
      </div>
    @endif
    <!--  -->

  </div>


    </div>
  </div>
</div>


        <!-- Modal -->
        <div class="col-sm-12 col-md-12">
        <div class="modal fade" id="modalTambahKost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
          </button>
          <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Tambah Data Kost</b></h1></center>
                  </div>
              <div class="modal-body">
                <form action="/new/kost" id="formcari" method="POST">
                  {{csrf_field()}}
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Kost</label>
                <input name="namakost" type="text" required class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder=".  .  .">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Username Kost</label>
                <input name="usernamekost" type="text" required class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder=".  .  .">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Foto Kost</label>  
                <span class="pull-right"><input type="checkbox" id="fotokost" name="fotokost"> <label>Upload Nanti</label></span>
                <input class="form-control" style="background-color: #1ED760; border: 2px dashed white; color:white; font-weight: bold; width: 100%;" type="file" name="filefoto" id="filefoto">
                <input type="text" name="fileexist" hidden value="none.jpg" id="fileexist" style="margin-top: 10px;">
              </div>

              <div class="form-group" style="background-color: #128039;border-radius:20px;padding: 10px;margin-top: 20px;">
                <center>
                  <h3 style="color:white;margin:0px;"><b>Silahkan Buat Akun Penjaga Kost</b></h3>
                </center>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Penjaga Kost</label>
                <input name="namapenjaga" type="text" class="form-control" required  id="exampleInputEmail1" aria-describedby="emailHelp" placeholder=".  .  .">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Username Penjaga Kost</label>
                <input name="usernamepenjaga" type="username" class="form-control" required  id="exampleInputEmail1" aria-describedby="emailHelp" placeholder=".  .  .">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Password Penjaga Kost</label>
                <input name="passpenjaga" type="password" class="form-control" required  id="exampleInputEmail1" aria-describedby="emailHelp" placeholder=".  .  .">
              </div>
              
                      <span >
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                          <button class="btn btn-lg btn-success" style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>   
            </form>
            </div>
          </div>
        </div>
    </div>
  </div>


    <div class="col-md-12 col-lg-12 col-sm-12 footbanner">
      <p>Copyright Premiere Code 2020</p>
    </div>


    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/easypiechart-data.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>

    <script>
    function teswa(){
      var eco = encodeURI(document.getElementById("komplain").value);
      window.open('https://api.whatsapp.com/send?phone=6281213073571&text='+eco);
    }

    //show it when the checkbox is clicked
      $('input[name="fotokost"]').on('click', function () {
          if ($(this).prop('checked')) {
              $('input[name="filefoto"]').fadeOut();
          } else {
              $('input[name="filefoto"]').fadeIn();
          }
      });

    </script>

    
</body>
</html>