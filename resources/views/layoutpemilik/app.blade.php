<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('judul')</title>
    <link rel="icon" href="{{ URL::asset('/uploads/iconweb.png')}}" type="image/x-icon">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

</head>
<body onload="@yield('eventdibody')" style="padding:0px;">

@section('variabelphp')
@show

<!--HEADER START-->
    <nav class="navbar navbar-custom navbar-fixed-top" id="navigasistick" role="navigation">
        <div class="container-fluid" >
            <div class="navbar-header">
                <a class="navbar-brand" href="#"><span><b>Premiere</b></span>Kost</a>
                <div class="pull-right">
                    <div class="dropdown" id="menuHP">
                      <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="modal" data-target="#ModalMenu"  aria-expanded="false" style="border-radius: 50px;"><i class="fa fa-bars"></i>
                      </button>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </nav>

     


  
    @section('topbanner')
    @show    

<div class="container-fluid">
<div class="row">
    <div class="col-sm-12 col-lg-12 main">
      <div class="modal fade" id="ModalMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="border-bottom: 2px dashed white">
                    <center><h1 style="color:white;" ><b>Hi {{$data_user->nama}} !</b></h1></center>
                  </div>
              <div class="modal-body">
                        <a class="dropdown-item" href="/login" style=" text-align: center; margin-bottom: 10px;  background-color: #128039;"><label style="font-size: 25px;">Dashboard</label></a>
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#ModalAdu" style="background-color: orange; color:white; text-align: center;"><label style="font-size: 25px;">Pengaduan</label></a><div class="divider"></div>
                        <a class="dropdown-item" href="/logout" style="border: 2px dashed white; text-align: center; margin-bottom: 10px;   background-color: #1ED760;"><label>Logout</label></a>
              </div>
            </div>
          </div>
        </div>
      <div class="modal fade" id="ModalAdu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <button type="button" class="close" style="font-size: 60px;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h1 style="color:white;" ><b>Form Trouble</b></h1></center>
                  </div>
              <div class="modal-body">
                      <!--Tombol Submit dan Batal-->
                      <span >
                        <div class="col-sm-12 col-lg-12 col-md-12">
                          <center>
                            <textarea class="komplain form-control" id="komplain" style="margin-bottom: 20px;"></textarea>
                            <button class="btn btn-lg btn-success" onclick="teswa()"  style="margin-bottom: 30px; width: 100%; padding: 20px; background-color: orange;">Submit</button>
                      </center>
                      </div>
                        <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 30px;">
                        <button class="btn btn-lg btn-light" style="width: 100%; background-color: #1ED760; border: 1px dashed white; color:white; padding: 10px;" data-dismiss="modal">Close</button>
                        </div>
                      </span>  
            </div>
          </div>
        </div>
      </div>

<!--HEADER END-->

        @include('flash-message')

        @yield('content')


    @section('isibodi')
    @show


    @section('tambahan')
    @show

    @section('footer')
    @show



    </div>
  </div>
</div>

    <div class="col-md-12 col-lg-12 col-sm-12 footbanner" style="text-align: center;">
      <p>Copyright Premiere Code 2020</p>
    </div>

    <script>
    function teswa(){
      var eco = encodeURI(document.getElementById("komplain").value);
      window.open('https://api.whatsapp.com/send?phone=6281213073571&text='+eco);
    }
    </script>

<script type="text/javascript">
  // When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the header
var header = document.getElementById("navigasistick");

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("navbar-custom-stick");
  } else {
    header.classList.remove("navbar-custom-stick");
  }
}
</script>
    
</body>
</html>