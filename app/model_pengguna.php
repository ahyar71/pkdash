<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class model_pengguna extends Model
{	
	protected $table='pengguna';
	
    protected $fillable = ['id','idkost', 'nama','username','password','isAdmin'];

}
