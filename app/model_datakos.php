<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class model_datakos extends Model
{
    protected $table = 'data_kost';

    protected $fillable = ['id','idpemilik','idpenjaga','kost','namakost','gambar'];


}
