<?php

namespace App\Mail;

use App\penghuni;
use App\keuangan;
use App\kamar;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class NotifEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $datapenghuni;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dataemail)
    {
        $this->datapenghuni = $dataemail;
        $this->datapenghuni['jatuhtempo'] = strtotime($dataemail['jatuhtempo']);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->subject('Peringatan Pembayaran Kos')
                ->markdown('emails.content')
                ->with([
                        'nama' => $this->datapenghuni['namapenghuni'],
                        'namakamar' => $this->datapenghuni['namakamar'],
                        'nominal' => $this->datapenghuni['nominalrincian'],
                        'jatuhtempo' => $this->datapenghuni['jatuhtempo']
                        ]);
    }
}
