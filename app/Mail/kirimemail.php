<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class kirimemail extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $password;
    public $email;
    public $wa;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nama,$pass,$email,$wa)
    {
        $this->name = $nama;
        $this->password = $pass;
        $this->email = $email;
        $this->wa = $wa;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Request User Baru')->view('emails.confirm');
    }
}
