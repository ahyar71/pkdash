<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class keuangan extends Model
{
    
    protected $table = 'keuangan';
    
    protected $fillable = ['idpenghuni','idkost','idkamar','jenistransaksi','nominaltransaksi','nominaltambahan','tanggaltransaksi','statustransaksi','pilihantransaksi','jatuhtempo','metode','deskripsi','penginput','penginput','created_at','updated_at'];


	public function getNamaPenghuni()
		{	
		    return penghuni::where('id',$this->idpenghuni)->value('namapenghuni');
		}
	public function getKamarKost()
		{	
		    return kamar::where('id',$this->idkamar)->value('namakamarkost');
		}
	public function getHargaKamar()
		{	
		    return kamar::where('id',$this->idkamar)->value('harga');
		}

}
