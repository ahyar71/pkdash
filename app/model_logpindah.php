<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class model_logpindah extends Model
{
    protected $table = 'log_pindahkamar';

    protected $fillable = ['idkamarlama','idkamarbaru','keterangan','tanggalpindah'];

}
