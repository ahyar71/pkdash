<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kamar extends Model
{
	protected $table = 'kamar';

	protected $fillable = ['namakamarkost', 'idkost', 'harga', 'status', 'namapenghuni', 'kasur', 'kmdalam', 'mejakursi', 'lemari', 'spreibantal', 'ac', 'created_at', 'updated_at'];

	public function getNamaPenghuni()
	{
		$datapenghuni = penghuni::where('idkamar', $this->id)->where('tanggalkeluar', null)->value('namapenghuni');
		return $datapenghuni;
		// return penghuni::where('idkamar',$this->id)->first()->namapenghuni;
	}

	public function getFasilitas()
	{
		return null;
		// return penghuni::where('idkamar',$this->id)->first()->namapenghuni;
	}

	public function getFotoPenghuni()
	{
		$datapenghuni = penghuni::where('idkamar', $this->id)->where('tanggalkeluar', null)->value('fotonik');
		return $datapenghuni;
	}
	public function getIdPenghuni()
	{
		$datapenghuni = penghuni::where('idkamar', $this->id)->where('tanggalkeluar', null)->value('id');
		return $datapenghuni;
	}
	public function getIdKamarBulan()
	{
		$datapenghuni = $this->getIdPenghuni();
		$idkamar = keuangan::where('idpenghuni', $datapenghuni)->where('jenistransaksi', 'bulanan')->where('statustransaksi', 'sudah')->value('idkamar');
		return $idkamar;
	}
	public function getIdKamarLunas()
	{
		$datapenghuni = $this->getIdPenghuni();
		$idkamar = keuangan::where('idpenghuni', $datapenghuni)->where('jenistransaksi', 'lunaslangsung')->where('statustransaksi', 'sudah')->value('idkamar');
		return $idkamar;
	}
	public function getIdKamarDP()
	{
		$datapenghuni = $this->getIdPenghuni();
		$idkamar = keuangan::where('idpenghuni', $datapenghuni)->where('jenistransaksi', 'dp')->where('statustransaksi', 'sudah')->value('idkamar');
		return $idkamar;
	}
	public function getStatusPenghuni()
	{
		$datapenghuni = penghuni::where('idkamar', $this->id)->where('tanggalkeluar', null)->value('tanggalkeluar');
		return $datapenghuni;
	}
	public function getStatusBayar()
	{
		$datakeuangan = keuangan::where('idkamar', $this->id)->value('statustransaksi');
		return $datakeuangan;
	}
	public function getJatuhTempo()
	{
		$datakeuangan = keuangan::orderBy('id', 'desc')->where('idpenghuni', $this->getIdPenghuni())->where('statustransaksi', 'belumbayar')->whereIn('jenistransaksi', ['bulanan', 'lainnya'])->value('jatuhtempo');
		if ($datakeuangan == null) {
			$datakeuangan = keuangan::orderBy('id', 'asc')->where('idkamar', $this->getIdKamarLunas())->where('idpenghuni', $this->getIdPenghuni())->where('statustransaksi', 'belumbayar')->where('jenistransaksi', 'lunaslangsung')->value('jatuhtempo');
		}
		if ($datakeuangan == null) {
			$datakeuangan = keuangan::orderBy('id', 'asc')->where('idkamar', $this->getIdKamarDP())->where('idpenghuni', $this->getIdPenghuni())->where('statustransaksi', 'belumbayar')->where('jenistransaksi', 'lunas')->value('jatuhtempo');
		}
		return $datakeuangan;
	}
	public function getNominal()
	{
		$datakeuangan = keuangan::orderBy('id', 'desc')->where('idpenghuni', $this->getIdPenghuni())->where('statustransaksi', 'belumbayar')->whereIn('jenistransaksi', ['bulanan', 'lainnya'])->value('nominaltransaksi');
		if ($datakeuangan == null) {
			$datakeuangan = keuangan::orderBy('id', 'asc')->where('idkamar', $this->getIdKamarLunas())->where('idpenghuni', $this->getIdPenghuni())->where('statustransaksi', 'belumbayar')->where('jenistransaksi', 'lunaslangsung')->value('nominaltransaksi');
		}
		if ($datakeuangan == null) {
			$datakeuangan = keuangan::orderBy('id', 'asc')->where('idkamar', $this->getIdKamarDP())->where('idpenghuni', $this->getIdPenghuni())->where('statustransaksi', 'belumbayar')->where('jenistransaksi', 'lunas')->value('nominaltransaksi');
		}
		setlocale(LC_MONETARY, 'en_US');
		$datakeuangan = number_format($datakeuangan);
		return $datakeuangan;
	}
}
