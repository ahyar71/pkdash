<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penghuni extends Model
{

	protected $table = 'penghuni';

	protected $fillable = ['username', 'tag', 'namapenghuni', 'idkost', 'idkamar', 'nik', 'email', 'nopenghuni', 'pekerjaan', 'jeniskendaraan', 'platkendaraan', 'tanggalmasuk', 'deskripsi', 'fotonik', 'created_at', 'updated_at'];

	public function getKamarNama()
	{
		$datanamakamar = kamar::where('id', $this->idkamar)->value('namakamarkost');
		return $datanamakamar;
	}

	public function getHargaKamar()
	{
		$datahargakamar = kamar::where('id', $this->idkamar)->value('harga');

		return $datahargakamar;
	}

	public static function selectData($username, $tag)
	{
		$value = DB::table('penghuni')->where('username', $username)->where('tag', $tag)->orderBy('id', 'asc')->get();
		return $value;
	}


	public function getStatusKamar()
	{
		$namakamar = kamar::where('id', $this->idkamar)->where('status', 'kosong')->value('id');
		if (!empty($namakamar)) {
			return 'terisi';
		} else {
			return 'kosong';
		}
	}
	public function getIdKamarKeuangan() //baru
	{
		$dataidkamar = keuangan::where('idkamar', $this->idkamar)->value('idkamar');
		return $dataidkamar;
	}
	public function getIdPenghuniKeuangan() //baru
	{
		$dataidpenghuni = keuangan::where('idpenghuni', $this->id)->value('id');
		return $dataidpenghuni;
	}

	public function getStatusTransaksi() //baru
	{
		$dataidpenghuni = keuangan::where('idpenghuni', $this->id)->value('statustransaksi');
		return $dataidpenghuni;
	}
}
