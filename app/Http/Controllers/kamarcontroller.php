<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class kamarcontroller extends Controller
{
    public function index()
    {   

        $datakos = \App\model_datakos::select('id')->where('idpenjaga',Auth::user()->id)->get();
        foreach($datakos as $rowkost) 
        {   
            $data_aset = \App\model_dataaset::where('idkost',$rowkost->id)->get();
            $data_kamar = \App\kamar::where('idkost',$rowkost->id)->orderBy('status')->get();
            $data_user = \App\pengguna::find(Auth::user()->id);
            $judul = 'Data Kamar';
            return view('kamar.index',['data_kamar'=>$data_kamar,
                'judul'=>$judul,'data_user'=>$data_user,'data_aset'=>$data_aset]);  
        }
    }

    public function create(Request $request)
    {   

        $datakos = \App\model_datakos::select('id')->where('idpenjaga',Auth::user()->id)->get();
        foreach($datakos as $rowkost) 
        {
            if($request['harga']<=0){
                return redirect()->action('kamarcontroller@index')->with('error','Harga Kamar tidak bisa dibawah atau sama dengan 0');          
            }
            $request['status'] = 'kosong';
            $request['idkost'] = $rowkost->id;
            \App\kamar::create($request->all());
        }
        return redirect()->action('kamarcontroller@index')->with('success','Data Kamar ' .$request['namakamarkost'].' Berhasil Ditambah');
    }


    public function edit(Request $request)
    {   
        $kamar = \App\kamar::find($request['idkamar']);
        $status = \App\kamar::where('id',$request['idkamar'])->value('status');
        $harga = \App\kamar::where('id',$request['idkamar'])->value('harga');
        if($request['harga']<=0){
                return redirect()->action('kamarcontroller@index')->with('error','Harga Kamar tidak bisa dibawah atau sama dengan 0');          
            }
        $kamar->namakamarkost = $request['namakamarkost'];
        $kamar->harga = $request['harga'];
        $kamar->save();
        return redirect()->action('kamarcontroller@index')->with('success','Data Kamar '. $request['namakamarkost'].' Telah Berhasil Diubah');
    }
//menghapus kamar belum benar, tambahkan status pada kolom database kamar untuk supaya tidak terhapus benar-benar, dan hanya menunjukkan kamar yang aktif saja, karena supaya dapat melihat detail transaksi nantinya.
//melihat transaksi pastikan bisa dilihat semua baik kamarnya telah dihapus atau belum.
    
    public function destroy($idkamar)
    {
        $statuskamar = \App\kamar::where('id',$idkamar)->value('status');
        $namakamar = \App\kamar::where('id',$idkamar)->value('namakamarkost');
        if($statuskamar=='terisi'){
            return back()->with('error','Kamar Masih Terisi');
        }elseif($statuskamar=='kosong'){
            $kamar = \App\kamar::find($idkamar);
            $kamar->delete();
            return redirect('/kamar')->with('warning','Kamar '.$namakamar.' telah dihapus');
        }        
    }

    public function sudahservis($idservis)
    {
            $aset = \App\model_dataaset::find($idservis);
            $aset->waktuservis = date('y-m-d',strtotime('now'));
            $aset->save();
            return redirect('/kamar')->with('warning','Waktu Pemeliharaan Telah Diperbaharui');
              
    }
}
