<?php

namespace App\Http\Controllers;

use App\keuangan;
use App\kamar;
use App\penghuni;
use App\Mail\NotifEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use PDF;
use Charts;
use DateTime;

class KeuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datakos = \App\model_datakos::select('id')->where('idpenjaga', Auth::user()->id)->get();
        foreach ($datakos as $rowkost) {
            $data_aset = \App\model_dataaset::where('idkost', $rowkost->id)->get();
            $data_penghuni = \App\penghuni::all()->where('tanggalkeluar', null)->where('idkost', $rowkost->id);
            $data_kamar = \App\kamar::where('idkost', $rowkost->id)->orderBy('status')->get();
            $data_keuangan = \App\keuangan::all()->where('idkost', $rowkost->id);
            $data_keuanganlimit = \App\keuangan::limit(100)->where('statustransaksi', 'sudah')->where('idkost', $rowkost->id)->orderBy('updated_at', 'desc')->get();
            $data_keuangan_notpaid = \App\keuangan::where('statustransaksi', 'belumbayar')->where('idkost', $rowkost->id)->orderBy('updated_at', 'desc')->get();
            $data_user = \App\pengguna::find(Auth::user()->id);
            $databelumbayar = DB::table('keuangan')
                ->join('penghuni', 'keuangan.idpenghuni', '=', 'penghuni.id')
                ->join('kamar', 'keuangan.idkamar', '=', 'kamar.id')
                ->select('keuangan.*', 'penghuni.namapenghuni', 'kamar.namakamarkost')->orderBy('jatuhtempo', 'desc')
                ->where('keuangan.idkost', $rowkost->id)
                ->get();
            $hasilpencarian = '';
            $judul = 'Data Keuangan';
        }
        return view('keuangan.index', ['data_penghuni' => $data_penghuni, 'data_kamar' => $data_kamar, 'data_keuangan' => $data_keuangan, 'data_user' => $data_user, 'judul' => $judul, 'data_pencarian' => $hasilpencarian, 'data_keuanganlimit' => $data_keuanganlimit, 'data_keuangan_notpaid' => $data_keuangan_notpaid, 'databelumbayar' => $databelumbayar, 'data_aset' => $data_aset]);
    }

    public function http_request($url)
    {
        // persiapkan curl
        $ch = curl_init();

        // set url 
        curl_setopt($ch, CURLOPT_URL, $url);

        // set user agent    
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        // return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string 
        $output = curl_exec($ch);

        // tutup curl 
        curl_close($ch);

        // mengembalikan hasil curl
        return $output;
    }

    public function firebase_push($penginput, $pilihan, $jenistransaksi, $nominal)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n    \"notification\": {\r\n        \"title\":\"" . "Ada data " . $pilihan . " baru!" . ",\"\r\n        \"body\": \"Data transaksi " . $jenistransaksi . " Sejumlah Rp. " . number_format($nominal) . "\"\r\n    },\r\n    \"priority\": \"high\",\r\n    \"data\": {\r\n    },\r\n    \"to\": \"/topics/admin\"\r\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: key=AAAA6a70eCQ:APA91bHoXlX5T5ECv-2oatBnnclNmrTmvko78hqa6lyVP26lpRwA4gLJsleqV6sJPfa2N5tbR5cvYWEveVPOPc0Lvc4DG-fvkJRiWEQESGVQTUMtt_WWtpumJ9jnjLTVey7bJbDKcXAx"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        return true;
    }

    public function jatuhtempo_pushdaily()
    {
        $tanggalfrom = date('Y-m-d', strtotime('-7 day'));
        $tanggalto = date('Y-m-d', strtotime('3 day'));
        $jatuhtempoq = keuangan::where('statustransaksi', 'belumbayar')->where('idkost', '1')->whereBetween('jatuhtempo', [$tanggalfrom, $tanggalto,])->orderBy('jatuhtempo', 'desc')->get();


        foreach ($jatuhtempoq as $rowjatuhtempo) {
            $tanggalawal = date('Y-m-d', strtotime('now'));
            $datesawal = new DateTime($tanggalawal);
            $tanggalakhir = date('Y-m-d', strtotime($rowjatuhtempo->jatuhtempo));
            $datesakhir = new DateTime($tanggalakhir);
            $kamar = kamar::select('namakamarkost')->where('id', $rowjatuhtempo->idkamar)->get();
            $penghuni = penghuni::select('namapenghuni')->where('id', $rowjatuhtempo->idpenghuni)->get();
            foreach ($kamar as $rowkamar) {
                $rowjatuhtempo->namakamar = $rowkamar->namakamarkost;
            }
            foreach ($penghuni as $rowpenghuni) {
                $rowjatuhtempo->namapenghuni = $rowpenghuni->namapenghuni;
            }

            $a = date_diff($datesawal, $datesakhir)->format("%r%a");
            if ((strtotime("now") >= strtotime($rowjatuhtempo->jatuhtempo)) && (strtotime("-1 day") <= strtotime($rowjatuhtempo->jatuhtempo))) {
                $rowjatuhtempo->durasitempo = 0;
            } else {
                $rowjatuhtempo->durasitempo = $a;
            }

            if ($rowjatuhtempo->durasitempo >= 1) {
                $rowjatuhtempo->keterangantempo = $rowjatuhtempo->durasitempo . ' Hari lagi';
            } elseif ($rowjatuhtempo->durasitempo == 0) {
                $rowjatuhtempo->keterangantempo = $rowjatuhtempo->durasitempo . ' Hari lagi';
            } elseif ($rowjatuhtempo->durasitempo >= -1) {
                $rowjatuhtempo->keterangantempo = 'Lewat ' . $rowjatuhtempo->durasitempo . ' Hari';
            } elseif ($rowjatuhtempo->durasitempo >= -3) {
                $rowjatuhtempo->keterangantempo = 'Lewat ' . $rowjatuhtempo->durasitempo . ' Hari. DENDA';
            } elseif ($rowjatuhtempo->durasitempo >= -8) {
                $rowjatuhtempo->keterangantempo = 'Lewat ' . $rowjatuhtempo->durasitempo . ' Hari. DIKELUARKAN';
            }

            if ($rowjatuhtempo->durasitempo <= 1) {
                $this->firebase_pushdaily($rowjatuhtempo->namapenghuni, $rowjatuhtempo->namakamar, $rowjatuhtempo->keterangantempo);
            }
        }
    }

    public function firebase_pushdaily($namapenghuni, $namakamar, $keterangan)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n    \"notification\": {\r\n        \"title\":\"" . "REMINDER: Pemberitahuan Jatuh Tempo" . "\"\r\n        \"body\": \"Kamar " . $namakamar . " ( " . $namapenghuni . " ) => ".$keterangan."\"\r\n    },\r\n    \"priority\": \"high\",\r\n    \"data\": {\r\n    },\r\n    \"to\": \"/topics/admin\"\r\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: key=AAAA6a70eCQ:APA91bHoXlX5T5ECv-2oatBnnclNmrTmvko78hqa6lyVP26lpRwA4gLJsleqV6sJPfa2N5tbR5cvYWEveVPOPc0Lvc4DG-fvkJRiWEQESGVQTUMtt_WWtpumJ9jnjLTVey7bJbDKcXAx"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        return true;
    }

    public function downloadfile(Request $request)
    {

        $datakos = \App\model_datakos::select('id')->where('idpenjaga', Auth::user()->id)->get();
        foreach ($datakos as $rowkost) {

            if (empty($request)) {
                return redirect('/pengurus');
            }

            $from = '';
            $to = '';
            $tanggalawal = $request['from'];
            $tanggalakhir = $request['to'];
            $pilihanwaktu = $request['pilihanwaktu'];
            $pilihanbulan = $request['pilihanbulan'];
            $pilihantahun = $request['pilihantahun'];
            $tahun = $request['tahun'];
            $dasar = $request['dasar'];

            if ($dasar == 'rentang') {
                $from = date("Y-m-d", strtotime($tanggalawal));
                $to = date("Y-m-d", strtotime($tanggalakhir));
                $hasilpencarian = \App\keuangan::whereBetween('tanggaltransaksi', [$from, $to])->where('statustransaksi', 'sudah')->orderBy('updated_at', 'asc')->where('idkost', $rowkost->id)->get();
            } elseif ($dasar == 'bulan') {
                $hasilpencarian = \App\keuangan::whereYear('tanggaltransaksi', '=', $pilihantahun)->whereMonth('tanggaltransaksi', '=', $pilihanbulan)->where('statustransaksi', 'sudah')->where('idkost', $rowkost->id)->orderBy('updated_at', 'asc')->get();
            } else {
                return back()->with('error', 'Mohon Refresh Halaman dan Isikan Data Dengan Benar');
            }


            $data = ['data_pencarian' => $hasilpencarian, 'tanggalawal' => $from, 'tanggalakhir' => $to, 'pilihanbulan' => $pilihanbulan, 'pilihantahun' => $pilihantahun, 'dasar' => $dasar];
            $pdf = PDF::loadView('laporan', $data);
        }
        return $pdf->download('laporankeuangan' . time() . '.pdf');
    }

    public function caritanggal(Request $request)
    {

        $datakos = \App\model_datakos::select('id')->where('idpenjaga', Auth::user()->id)->get();
        foreach ($datakos as $rowkost) {
            $data_penghuni = \App\penghuni::all()->where('idkost', $rowkost->id);
            $data_kamar = \App\kamar::where('idkost', $rowkost->id)->orderBy('status')->get();
            $data_keuangan = \App\keuangan::all()->where('idkost', $rowkost->id);
            $data_user = \App\pengguna::find(Auth::user()->id);

            $from = date("Y-m-d", strtotime($request['from']));
            $to = date("Y-m-d", strtotime($request['to']));

            $hasilpencarian = \App\keuangan::whereBetween('tanggaltransaksi', [$from, $to])->where('statustransaksi', 'sudah')->where('idkost', $rowkost->id)->orderBy('updated_at', 'desc')->get();

            if (empty($hasilpencarian)) {
                $hasilpencarian = "tidakada";
            }
        }

        return view('pencarianpenjaga', ['data_penghuni' => $data_penghuni, 'data_kamar' => $data_kamar, 'data_keuangan' => $data_keuangan, 'data_user' => $data_user, 'data_pencarian' => $hasilpencarian, 'tanggalawal' => $from, 'tanggalakhir' => $to]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $datakos = \App\model_datakos::select('id')->where('idpenjaga', Auth::user()->id)->get();
        $penginput = '';
        $pilihantransaksi = '';
        $jenistransaksi = '';
        foreach ($datakos as $rowkost) {

            $idpenghuni = $request['listdp'];
            $idkamar = \App\penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar 
            $namapenghuni = \App\penghuni::where('id', $idpenghuni)->value('namapenghuni');
            $namakamar = \App\kamar::where('id', $idkamar)->value('namakamarkost');
            $tambahanbiaya = 0;
            if ($request['tipe'] == 'sendiri') {
                $tambahanbiaya = 0;
            } else if ($request['tipe'] == 'pasutri') {
                $tambahanbiaya = 200000;
            } else if ($request['tipe'] == 'adikkakak') {
                $tambahanbiaya = 200000;
            } else if ($request['tipe'] == 'teman') {
                $tambahanbiaya = 200000;
            }

            if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'dp') {
                if ($request['lamasewa'] <= 0) {
                    return back()->with('error', 'Maaf, Lama Sewa Minimal 1 Bulan');
                }
                $idpenghuni = $request['listdp'];
                $idkamar = \App\penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar 
                $namapenghuni = \App\penghuni::where('id', $idpenghuni)->value('namapenghuni');
                $namakamar = \App\kamar::where('id', $idkamar)->value('namakamarkost');
                $jenistransaksi = $request['jenistransaksi'];
                $pilihantransaksi = $request['pilihantransaksi'];
                $nominal = $request['nominaltransaksi'];
                $metode = $request['metode'];
                $keterangan = "Pembayaran DP " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                $tanggal = $request['tanggaltransaksi'];
                $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                $status = 'sudah';
                $penginput = \App\pengguna::where('id', Auth::user()->id)->value('nama');
                $jatuhtempo = date('Y-m-d', strtotime($request['tanggaltempo']));


                if ($nominal < 200000 || $nominal > 800000) {
                    return back()->with('error', 'Nominal DP Minimal Rp. 200.000 dan Maksimal Rp. 800.000');
                }

                $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                //buat data dp
                $data = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $nominal, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'metode' => $metode, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                \App\keuangan::create($data);

                //buat data pelunasan            
                $keterangan = "Pembayaran Lunas " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                $idpenghuni = $request['listdp'];
                $lamasewa = $request['lamasewa'];
                $status = 'belumbayar';
                $jenistransaksi = 'lunas';
                $idkamar = \App\penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar   
                $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
                $dilunasi = ($hargakamar * $lamasewa + $tambahanbiaya) - $nominal;
                if ($nominal == 200000) {
                    $jatuhtempo = date('Y-m-d', strtotime("+8 days"));
                } else if ($nominal > 200000 && $nominal <= 800000) {
                    $jatuhtempo = date('Y-m-d', strtotime("+14 days"));
                }
                $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $dilunasi, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                \App\keuangan::create($data2);

                return redirect('/keuangan')->with('success', 'Data Pemasukkan Pembayaran DP Berhasil dimasukkan!');
            } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenislunas'] == 'lunas') {
                $nominal = $request['nominaltransaksi'];
                $idtransaksi = $request['listlunas'];
                $metode = $request['metode'];
                $datakeuangan = \App\keuangan::find($idtransaksi);
                $idpenghuni = \App\keuangan::where('id', $idtransaksi)->value('idpenghuni');
                $idkamar = \App\keuangan::where('id', $idtransaksi)->value('idkamar');
                $nominal2 = \App\keuangan::where('id', $idtransaksi)->value('nominaltransaksi');
                $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
                $namapenghuni = \App\penghuni::where('id', $idpenghuni)->value('namapenghuni');
                $namakamar = \App\kamar::where('id', $idkamar)->value('namakamarkost');
                $idtransaksidp = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'dp')->where('statustransaksi', 'sudah')->value('id');
                $nominaltambah = \App\keuangan::where('id', $idtransaksidp)->value('nominaltambahan');
                $nominal1 = \App\keuangan::where('id', $idtransaksidp)->value('nominaltransaksi');
                $totalnominal = $nominal1 + $nominal2 - $nominaltambah;
                $lamasewa = $totalnominal / $hargakamar;
                $jatuhtempo = date('Y-m-d', strtotime($request['tanggaltempo']));

                if ($nominal != $nominal2) {
                    return back()->with('error', 'Data Tidak Berhasil Dimasukkan, Nominal Pelunasan Harus Rp. ' . number_format($nominal2));
                }

                if ((strtotime("-60 day") <= strtotime($request['tanggaltempo'])) && (strtotime("+5 days") >= strtotime($request['tanggaltempo']))) {
                    return back()->with('error', 'Mbak Cuma bisa Masukin Tanggal Tempo yang ngga terlalu jauh dari tanggal yang seharusnya');
                }


                $pilihantransaksi = $request['pilihantransaksi'];
                $keterangan = "Pembayaran Lunas " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                $status = 'sudah';

                $datakeuangan->nominaltransaksi = $nominal;
                $datakeuangan->jatuhtempo = $jatuhtempo;
                $datakeuangan->deskripsi = $keterangan;
                $datakeuangan->tanggaltransaksi = $tanggaltransaksi;
                $datakeuangan->statustransaksi = $status;
                $datakeuangan->metode = $metode;
                $datakeuangan->save();

                $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                //buat data pelunasan
                $keterangan = "Pembayaran Bulanan " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                $bayardp = \App\keuangan::where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'dp')->value('nominaltransaksi');
                $bayarlunas = \App\keuangan::where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'lunas')->value('nominaltransaksi');
                $tambahbayar = $bayardp + $bayarlunas;
                $lamalanjut = 1;
                $status = 'belumbayar';
                $jenistransaksi = 'bulanan';
                $belumlunas = $hargakamar * $lamalanjut;
                $jatuhtempo = date($request['tanggaltempo']);
                $penginput = \App\pengguna::where('id', Auth::user()->id)->first()->nama;


                $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $tambahbayar, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                \App\keuangan::create($data2);

                return redirect('/keuangan')->with('success', 'Data Pemasukkan Pelunasan Dengan DP Berhasil dimasukkan!');
            } elseif ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenislunas'] == 'lunaslangsung') {
                if ($request['lamasewa'] <= 0) {
                    return back()->with('error', 'Maaf, Lama Sewa Minimal 1 Bulan');
                }
                $idpenghuni = $request['listlangsunglunas'];
                $idkamar = \App\penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar 
                $namapenghuni = \App\penghuni::where('id', $idpenghuni)->value('namapenghuni');
                $namakamar = \App\kamar::where('id', $idkamar)->value('namakamarkost');
                $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
                $lamasewa = $request['lamasewa'];
                $nominalharusdibayar = ($lamasewa * $hargakamar) + $tambahanbiaya;
                $jenistransaksi = $request['jenislunas'];
                $pilihantransaksi = $request['pilihantransaksi'];
                $nominal = $request['nominaltransaksi'];
                $metode = $request['metode'];
                $keterangan = "Pembayaran Lunas Langsung " . $namapenghuni . " Bulan " . date("F", strtotime("now")) . " Hingga Bulan " . date("F", strtotime("now" . " +" . $lamasewa . " month")) . "  (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                $tanggal = $request['tanggaltransaksi'];
                $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                $status = 'sudah';
                $penginput = \App\pengguna::where('id', Auth::user()->id)->value('nama');
                $jatuhtempo = date('Y-m-d', strtotime($request['tanggaltempo']));

                if ($nominal < $nominalharusdibayar || $nominal > $nominalharusdibayar) {
                    return back()->with('error', 'Nominal Pembayaran Lunas Langsung Harus Rp. ' . number_format($nominalharusdibayar));
                }

                $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                //buat data dp
                $data = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $nominalharusdibayar, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'metode' => $metode, 'penginput' => $penginput);
                \App\keuangan::create($data);

                //buat data pelunasan
                $keterangan = "Pembayaran Bulanan " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                $lamalanjut = $request['lamasewa'];
                $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
                $status = 'belumbayar';
                $jenistransaksi = 'bulanan';
                $belumlunas = $hargakamar * $lamalanjut;
                $penginput = \App\pengguna::where('id', Auth::user()->id)->first()->nama;

                $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $belumlunas, 'pilihantransaksi' => $pilihantransaksi, 'nominaltambahan' => null, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                \App\keuangan::create($data2);
                return redirect('/keuangan')->with('success', 'Data Pemasukkan Pembayaran Langsung Lunas Berhasil dimasukkan!');
            } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'bulanan') {

                if ($request['lamasewa'] <= 0) {
                    return back()->with('error', 'Maaf, Lama Sewa Minimal 1 Bulan');
                }
                $idtransaksi = $request['listbulanan'];
                $datakeuangan = \App\keuangan::find($idtransaksi);
                $idpenghuni = \App\keuangan::where('id', $idtransaksi)->value('idpenghuni');
                $tambahanbiaya += \App\keuangan::where('id', $idtransaksi)->value('nominaltambahan');
                $idkamar = \App\keuangan::where('id', $idtransaksi)->value('idkamar');
                $namapenghuni = \App\penghuni::where('id', $idpenghuni)->value('namapenghuni');
                $namakamar = \App\kamar::where('id', $idkamar)->value('namakamarkost');
                $nominal = $request['nominaltransaksi'];
                $metode = $request['metode'];
                $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
                $lamasewa = $request['lamasewa'];
                $nominaltanpatambahan = ($hargakamar * $lamasewa);
                $jatuhtempoawal = \App\keuangan::where('id', $idtransaksi)->value('jatuhtempo');

                $pilihantransaksi = $request['pilihantransaksi'];
                $keterangan = "Pembayaran Bulanan " . $namapenghuni . " Bulan " . date("F", strtotime($jatuhtempoawal)) . " Hingga Bulan " . date("F", strtotime($jatuhtempoawal . " +" . $lamasewa . " month")) . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                $tanggal = $request['tanggaltransaksi'];
                $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                $status = 'sudah';

                $datakeuangan->nominaltransaksi = $nominaltanpatambahan;
                $datakeuangan->nominaltambahan = $tambahanbiaya;
                $datakeuangan->deskripsi = $keterangan;
                $datakeuangan->tanggaltransaksi = $tanggaltransaksi;
                $datakeuangan->statustransaksi = $status;
                $datakeuangan->metode = $metode;
                $datakeuangan->save();

                $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);

                //buat data pelunasan            
                if ($request['tipe'] == 'sendiri') {
                    $tambahanbiaya = 0;
                } else if ($request['tipe'] == 'pasutri') {
                    $tambahanbiaya = 200000;
                } else if ($request['tipe'] == 'adikkakak') {
                    $tambahanbiaya = 200000;
                } else if ($request['tipe'] == 'teman') {
                    $tambahanbiaya = 200000;
                }
                $keterangan = "Pembayaran Bulanan " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                $lamalanjut = $request['lamasewa'];
                $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
                $status = 'belumbayar';
                $jatuhtempoawal = \App\keuangan::where('id', $idtransaksi)->value('jatuhtempo');
                $jenistransaksi = 'bulanan';
                $belumlunas = $hargakamar;
                $jatuhtempoakhir = date("Y-m-d", strtotime($request['tanggaltempo']));
                $penginput = \App\pengguna::where('id', Auth::user()->id)->first()->nama;

                $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $belumlunas, 'nominaltambahan' => null, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempoakhir, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                \App\keuangan::create($data2);

                return redirect('/keuangan')->with('success', 'Data Pemasukkan ' . $request['jenistransaksi'] . ' Berhasil dimasukkan!');
            } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'tambahan') {

                list($jenis, $idtransaksi) = explode("|", $request['listtambahan']);
                if ($jenis == 'baru') {
                    $idpenghuni = $idtransaksi;
                    $idkamar = \App\penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar 
                    $namapenghuni = \App\penghuni::where('id', $idpenghuni)->value('namapenghuni');
                    $tanggalmasuk = \App\penghuni::where('id', $idpenghuni)->value('tanggalmasuk');
                    $namakamar = \App\kamar::where('id', $idkamar)->value('namakamarkost');
                    $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
                    $lamasewa = $request['lamahari'];

                    $jenistransaksi = 'tambahan';

                    $pilihantransaksi = $request['pilihantransaksi'];
                    $nominal = $request['nominaltransaksi'];
                    $metode = $request['metode'];
                    $keterangan = "Pembayaran  " . $lamasewa . " Hari " . $namapenghuni . " Bulan " . date("F", strtotime("now")) . " Hingga Bulan " . date("F", strtotime("now" . " +" . $lamasewa . " month")) . "  (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $tanggal = $request['tanggaltransaksi'];
                    $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                    $status = 'sudah';
                    $penginput = \App\pengguna::where('id', Auth::user()->id)->value('nama');

                    $jatuhtempo = date("Y-m-d", strtotime($tanggalmasuk . "+" . $lamasewa . "day"));

                    $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                    //buat data harian/mingguan
                    $data = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $nominal, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'metode' => $metode, 'penginput' => $penginput);
                    \App\keuangan::create($data);

                    //buat data bulanan
                    $keterangan = "Pembayaran Selanjutnya " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $lamalanjut = $request['lamahari'];
                    $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
                    $status = 'belumbayar';
                    $jenistransaksi = 'bulanan';
                    $belumlunas = $hargakamar * $lamalanjut;
                    $penginput = \App\pengguna::where('id', Auth::user()->id)->first()->nama;

                    $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $belumlunas, 'pilihantransaksi' => $pilihantransaksi, 'nominaltambahan' => null, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                    \App\keuangan::create($data2);
                } else {
                    $datakeuangan = \App\keuangan::find($idtransaksi);
                    $idpenghuni = \App\keuangan::where('id', $idtransaksi)->value('idpenghuni');
                    $tambahanbiaya += \App\keuangan::where('id', $idtransaksi)->value('nominaltambahan');
                    $idkamar = \App\keuangan::where('id', $idtransaksi)->value('idkamar');
                    $namapenghuni = \App\penghuni::where('id', $idpenghuni)->value('namapenghuni');
                    $namakamar = \App\kamar::where('id', $idkamar)->value('namakamarkost');
                    $nominal = $request['nominaltransaksi'];
                    $metode = $request['metode'];
                    $lamasewa = $request['lamahari'];
                    $jatuhtempoawal = \App\keuangan::where('id', $idtransaksi)->value('jatuhtempo');


                    $pilihantransaksi = $request['pilihantransaksi'];
                    $keterangan = "Pembayaran Lanjutan " . $namapenghuni . " " . $lamasewa . " Hari (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $tanggal = $request['tanggaltransaksi'];
                    $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                    $status = 'sudah';
                    $jenistransaksi = 'tambahan';


                    $datakeuangan->jenistransaksi = $jenistransaksi;
                    $datakeuangan->nominaltransaksi = $nominal;
                    $datakeuangan->nominaltambahan = 0;
                    $datakeuangan->deskripsi = $keterangan;
                    $datakeuangan->tanggaltransaksi = $tanggaltransaksi;
                    $datakeuangan->statustransaksi = $status;
                    $datakeuangan->metode = $metode;
                    $datakeuangan->save();

                    $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                    $lamalanjut = $request['lamahari'];
                    $keterangan = "Pembayaran Lanjutan " . $namapenghuni . " " . $lamasewa . " Hari (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
                    $status = 'belumbayar';
                    $jatuhtempoawal = \App\keuangan::where('id', $idtransaksi)->value('jatuhtempo');
                    $jenistransaksi = 'bulanan';
                    $jatuhtempoakhir = date("Y-m-d", strtotime($jatuhtempoawal . "+" . $lamalanjut . "day"));
                    $penginput = \App\pengguna::where('id', Auth::user()->id)->first()->nama;

                    $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => 0, 'nominaltambahan' => null, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempoakhir, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                    \App\keuangan::create($data2);
                }
                return redirect('/keuangan')->with('success', 'Data Pemasukkan ' . $request['jenistransaksi'] . ' Berhasil dimasukkan!');
            } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'lain') {

                $nominal = $request['nominaltransaksi'];
                $metode = $request['metode'];

                $pilihantransaksi = $request['pilihantransaksi'];
                $tanggal = $request['tanggaltransaksi'];
                $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                $keterangan = "Pembayaran Lainnya || Catatan: " . $request['keterangantransaksi'];
                $status = 'sudah';
                $jenistransaksi = 'lainnya';
                $penginput = \App\pengguna::where('id', Auth::user()->id)->first()->nama;

                $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                $data2 = array('jenistransaksi' => $jenistransaksi, 'idkost' => $rowkost->id, 'nominaltransaksi' => $nominal, 'nominaltambahan' => null, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'metode' => $metode, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => null, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                \App\keuangan::create($data2);

                return redirect('/keuangan')->with('success', 'Data Pemasukkan ' . $request['jenistransaksi'] . ' Berhasil dimasukkan!');
            } else if ($request['pilihantransaksi'] == 'pengeluaran') {

                $nominal = "-" . $request['nominaltransaksi'];
                $idkamar = null;
                $idpenghuni = null;
                $jenistransaksi = $request['jenistransaksi'];
                $pilihantransaksi = $request['pilihantransaksi'];
                $statustransaksi = 'sudah';
                $jatuhtempo = null;
                $metode = $request['metode'];
                $deskripsi = "Pembayaran " . $request['jenistransaksi'] . ' || Catatan: ' . $request['keterangantransaksi'];
                $tanggaltransaksi = date('Y-m-d', strtotime("now"));
                $penginput = \App\pengguna::where('id', Auth::user()->id)->first()->nama;

                $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $nominal, 'pilihantransaksi' => $pilihantransaksi, 'nominaltambahan' => $tambahanbiaya, 'statustransaksi' => $statustransaksi, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'metode' => $metode, 'deskripsi' => $deskripsi, 'penginput' => $penginput);
                \App\keuangan::create($data2);


                $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                return redirect('/keuangan')->with('success', 'Data Pengeluaran ' . $request['jenistransaksi'] . ' Berhasil dimasukkan!');
            }
        }

        $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);

        return redirect()->action('KeuanganController@index');
    }

    public function destroy($idtransaksi, $idpenghuni, $idkamar, $pilihan, $jenis)
    {
        if ($pilihan == 'pengeluaran') {
            $jenistransaksi = \App\keuangan::where('id', $idtransaksi)->value('jenistransaksi');
            $tanggaltransaksi = \App\keuangan::where('id', $idtransaksi)->value('tanggaltransaksi');
            if (strtotime("now") >= strtotime($tanggaltransaksi) && strtotime("-4 day") < strtotime($tanggaltransaksi)) {
                $datatransaksihapus = \App\keuangan::find($idtransaksi);
                $datatransaksihapus->statustransaksi = 'batal';
                $datatransaksihapus->save();
                return redirect('/keuangan')->with('warning', 'Data Pengeluaran ' . $jenistransaksi . ' telah dihapus, MOHON LEBIH TELITI DALAM MENGINPUTKAN DATA!');
            } else {
                return redirect('/keuangan')->with('error', 'Maaf Data Sudah Tidak Bisa Dihapus.');
            }
        } elseif ($pilihan == 'pemasukkan') {
            if ($jenis == 'dp') {
                $idpelunasan = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'lunas')->value('id');
                $statuspelunasan = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'lunas')->value('statustransaksi');
                $idbulanan = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'bulanan')->value('id');
                $tanggaltransaksi = \App\keuangan::where('id', $idtransaksi)->value('tanggaltransaksi');
                if ((strtotime("now") >= strtotime($tanggaltransaksi)) && (strtotime("-4 day") <= strtotime($tanggaltransaksi)) && $statuspelunasan == 'belumbayar' && $idbulanan == null) {
                    $datatransaksipelunasan = \App\keuangan::find($idpelunasan);
                    $datatransaksipelunasan->delete();
                    $datatransaksihapus = \App\keuangan::find($idtransaksi);
                    $datatransaksihapus->delete();
                    return redirect('/keuangan')->with('warning', 'Data pembayaran DP telah dihapus, MOHON LEBIH TELITI DALAM MENGINPUTKAN DATA!');
                } else {
                    return redirect('/keuangan')->with('error', 'Maaf Data DP Sudah Tidak Bisa Dihapus.');
                }
            } elseif ($jenis == 'lunas') {
                $idbulanan = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'bulanan')->value('id');
                $statusbulanan = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'bulanan')->value('statustransaksi');
                $tanggaldp = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'dp')->value('jatuhtempo');
                $tanggaltransaksi = \App\keuangan::where('id', $idtransaksi)->value('tanggaltransaksi');
                if ((strtotime("now") >= strtotime($tanggaltransaksi)) && (strtotime("-4 day") <= strtotime($tanggaltransaksi)) && $statusbulanan == 'belumbayar') {
                    $datatransaksibulanan = \App\keuangan::find($idbulanan);
                    $datatransaksibulanan->delete();
                    $datatransaksihapus = \App\keuangan::find($idtransaksi);
                    $datatransaksihapus->statustransaksi = 'belumbayar';
                    $datatransaksihapus->jatuhtempo = $tanggaldp;
                    $datatransaksihapus->save();
                    return redirect('/keuangan')->with('warning', 'Data pembayaran Pelunasan DP telah dihapus, MOHON LEBIH TELITI DALAM MENGINPUTKAN DATA!');
                } else {
                    return redirect('/keuangan')->with('error', 'Maaf Data Pelunasan DP Sudah Tidak Bisa Dihapus.');
                }
            } elseif ($jenis == 'lunaslangsung') {
                $statuspelunasan = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'lunaslangsung')->value('statustransaksi');
                $idbulanan = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'bulanan')->value('id');
                $statusbulanan = \App\keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'bulanan')->value('statustransaksi');
                $tanggaltransaksi = \App\keuangan::where('id', $idtransaksi)->value('tanggaltransaksi');
                if ((strtotime("now") >= strtotime($tanggaltransaksi)) && (strtotime("-4 day") <= strtotime($tanggaltransaksi)) && $statusbulanan == 'belumbayar') {
                    $datatransaksibulanan = \App\keuangan::find($idbulanan);
                    $datatransaksibulanan->delete();
                    $datatransaksihapus = \App\keuangan::find($idtransaksi);
                    $datatransaksihapus->delete();
                    return redirect('/keuangan')->with('warning', 'Data pembayaran langsung lunas telah dihapus, MOHON LEBIH TELITI DALAM MENGINPUTKAN DATA!');
                } else {
                    return redirect('/keuangan')->with('error', 'Maaf Data Pembayaran Langsung Lunas Sudah Tidak Bisa Dihapus.');
                }
            } elseif ($jenis == 'bulanan') {
                $tanggaltransaksi = \App\keuangan::where('id', $idtransaksi)->value('tanggaltransaksi');
                $tanggaltempo = \App\keuangan::where('id', $idtransaksi)->value('jatuhtempo');
                $tanggaltempomax = \App\keuangan::where('idpenghuni', $idpenghuni)->where('statustransaksi', 'sudah')->orderBy('jatuhtempo', 'desc')->limit(1)->value('jatuhtempo');
                $temp = 0;


                if ((strtotime("now") >= strtotime($tanggaltransaksi)) && (strtotime("-4 day") <= strtotime($tanggaltransaksi)) && strtotime($tanggaltempo) == strtotime($tanggaltempomax)) {

                    $idbulananselanjutnya = \App\keuangan::where('idpenghuni', $idpenghuni)->where('idkamar', $idkamar)->where('jenistransaksi', 'bulanan')->where('statustransaksi', 'belumbayar')->value('id');
                    $datatransaksihapus1 = \App\keuangan::find($idbulananselanjutnya);
                    $datatransaksihapus1->delete();


                    $datatransaksihapus = \App\keuangan::find($idtransaksi);
                    $datatransaksihapus->statustransaksi = 'belumbayar';
                    $datatransaksihapus->save();
                    return redirect('/keuangan')->with('warning', 'Data pembayaran Bulanan telah dihapus, MOHON LEBIH TELITI DALAM MENGINPUTKAN DATA!');
                } else {
                    return redirect('/keuangan')->with('error', 'Maaf Data Pembayaran Bulanan Sudah Tidak Bisa Dihapus.');
                }
            } elseif ($jenis == 'lainnya') {
                $tanggaltransaksi = \App\keuangan::where('id', $idtransaksi)->value('tanggaltransaksi');
                if ((strtotime("now") >= strtotime($tanggaltransaksi)) && (strtotime("-4 day") <= strtotime($tanggaltransaksi))) {
                    $datatransaksihapus1 = \App\keuangan::find($idtransaksi);
                    $datatransaksihapus1->delete();
                    return redirect('/keuangan')->with('warning', 'Data pembayaran telah dihapus, MOHON LEBIH TELITI DALAM MENGINPUTKAN DATA!');
                } else {
                    return redirect('/keuangan')->with('error', 'Maaf Data Pembayaran Bulanan Sudah Tidak Bisa Dihapus.');
                }
            } else {
                return redirect('/keuangan')->with('error', 'Maaf Data Sudah Tidak Bisa Dihapus.');
            }
        }


        return redirect('/keuangan');
    }
    public function getNominalBulanan()
    {
        $idkeuangan = $_GET['idkeuangan'];
        if ($_GET['lamasewa'] != '' || $_GET['lamasewa'] != null) {
            $lamasewa = $_GET['lamasewa'];
        } else {
            $lamasewa = 0;
        }
        $jenispenghuni = $_GET['jenispenghuni'];
        $tambahanjenispenghuni = 0;
        if ($jenispenghuni != 'sendiri') {
            $tambahanjenispenghuni = 200000;
        }
        $idkamar = \App\keuangan::where('id', $idkeuangan)->value('idkamar');
        $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
        $getData['total'] = ($hargakamar + $tambahanjenispenghuni) * $lamasewa;

        $counterkamar = 0;

        foreach ($getData as $rowfinkamar) {
            $counterkamar++;
        }
        $getData['jumlahdata'] = $counterkamar;

        echo json_encode($getData);
        exit;
    }


    public function getNominalLangsungLunas()
    {
        $idpenghuni = $_GET['idpenghuni'];
        if ($_GET['lamasewa'] != '' || $_GET['lamasewa'] != null) {
            $lamasewa = $_GET['lamasewa'];
        } else {
            $lamasewa = 0;
        }
        $jenispenghuni = $_GET['jenispenghuni'];
        $tambahanjenispenghuni = 0;
        if ($jenispenghuni != 'sendiri') {
            $tambahanjenispenghuni = 200000;
        }
        $idkamar = \App\penghuni::where('id', $idpenghuni)->value('idkamar');
        $hargakamar = \App\kamar::where('id', $idkamar)->value('harga');
        $getData['total'] = ($hargakamar + $tambahanjenispenghuni) * $lamasewa;

        $counterkamar = 0;

        foreach ($getData as $rowfinkamar) {
            $counterkamar++;
        }
        $getData['jumlahdata'] = $counterkamar;

        echo json_encode($getData);
        exit;
    }
    public function getNominalPelunasan()
    {
        $idkeuangan = $_GET['idkeuangan'];
        $kekurangan = \App\keuangan::where('id', $idkeuangan)->value('nominaltransaksi');
        $getData['total'] = $kekurangan;

        $counterkamar = 0;

        foreach ($getData as $rowfinkamar) {
            $counterkamar++;
        }
        $getData['jumlahdata'] = $counterkamar;

        echo json_encode($getData);
        exit;
    }
}
