<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class asetcontroller extends Controller
{
    public function index()
    {	

        $datakos = \App\model_datakos::select('id')->where('idpenjaga',Auth::user()->id)->get();
        foreach($datakos as $rowkost) 
        {
            $data_aset = \App\model_dataaset::where('idkost',$rowkost->id)->orderBy('idkamar','asc')->get();
            $data_kamar = \App\kamar::where('idkost',$rowkost->id)->get();
            $data_user = \App\pengguna::find(Auth::user()->id);
        	$judul = 'Data Aset';
        	return view('aset.index',['data_aset'=>$data_aset,
        		'judul'=>$judul,'data_user'=>$data_user,'data_kamar'=>$data_kamar]);	
        }
    }

    public function create(Request $request)
    {	
        $datakos = \App\model_datakos::select('id')->where('idpenjaga',Auth::user()->id)->get();
        foreach($datakos as $rowkost) 
        {   
            $request['idkost'] = $rowkost->id;
            \App\model_dataaset::create($request->all());
        }
    	return redirect()->action('asetcontroller@index')->with('success','Data Aset ' .$request['nama'].' Berhasil Ditambah');
    }


    public function edit(Request $request)
    {   
        $aset = \App\model_dataaset::find($request['id']);
        $aset->id = $request['id'];
        $aset->idkamar = $request['idkamar'];
        $aset->nama = $request['nama'];
        if(!empty($request['jangkaservice'])){
            $aset->jangkaservice = $request['jangkaservice'];
        }
        if(!empty($request['waktuservis'])){
            $aset->waktuservis = date('Y-m-d',strtotime($request['waktuservis']));
        }
        if(!empty($request['waktupembelian'])){
            $aset->waktupembelian = date('Y-m-d',strtotime($request['waktupembelian']));
        }
        $aset->save();
        return redirect()->action('asetcontroller@index')->with('success','Data Aset '. $request['nama'].' Telah Berhasil Diubah');
    }
    
    public function destroy($idaset)
    {       
            $namaaset = \App\model_dataaset::where('id',$idaset)->value('nama');
            $aset = \App\model_dataaset::find($idaset);
            $aset->delete();
            return redirect('/aset')->with('warning','Aset '.$namaaset.' telah dihapus');
    }
}
