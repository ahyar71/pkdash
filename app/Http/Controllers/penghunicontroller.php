<?php

namespace App\Http\Controllers;

use App\keuangan;
use App\kamar;
use App\penghuni;
use App\Mail\NotifEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class penghunicontroller extends Controller
{
    public function index()
    {
        $datakos = \App\model_datakos::select('id')->where('idpenjaga', Auth::user()->id)->get();
        foreach ($datakos as $rowkost) {
            $data_penghuni = \App\penghuni::all()->where('tanggalkeluar', null)->where('idkost', $rowkost->id);
            $data_penghunisemua = \App\penghuni::where('idkost', $rowkost->id)->orderBy('tanggalkeluar', 'asc')->get();
            $data_kamar = \App\kamar::all()->where('idkost', $rowkost->id);
            $data_keuangan = \App\keuangan::all()->where('idkost', $rowkost->id);
            $data_user = \App\pengguna::find(Auth::user()->id);
            
			$tot_kosong = 0;
			foreach ($data_kamar as $rowkamar) {
                if ($rowkamar->status == 'kosong') {
					$tot_kosong++;
				}
			}


            $judul = 'Data Penghuni';
        }
        return view('penghuni.index', ['tot_kosong' => $tot_kosong, 'data_penghuni' => $data_penghuni, 'data_penghunisemua' => $data_penghunisemua, 'data_kamar' => $data_kamar, 'data_keuangan' => $data_keuangan, 'data_user' => $data_user, 'judul' => $judul]);
    }

    public function penghuniindex()
    {
        $datakos = \App\model_datakos::select('id')->where('id', Auth::user()->idkost)->get();
        foreach ($datakos as $rowkost) {
            $data_penghuni = \App\penghuni::all()->where('tanggalkeluar', null)->where('idkost', Auth::user()->idkost)->where('id',Auth::user()->id);
            $data_penghunisemua = \App\penghuni::where('idkost', Auth::user()->idkost)->where('id',Auth::user()->id)->orderBy('tanggalkeluar', 'asc')->get();
            $data_kamar = \App\kamar::all()->where('idkost', Auth::user()->idkost)->where('id',Auth::user()->idkamar);
            $jatuhtempo = \App\keuangan::where('idkost', Auth::user()->idkost)->where('idpenghuni',Auth::user()->id)->where('statustransaksi','belumbayar')->value('jatuhtempo');
            $data_keuangan = \App\keuangan::all()->where('idkost', Auth::user()->idkost)->where('id',Auth::user()->id);
            $data_user = \App\penghuni::find(Auth::user()->id);
            $jumlahdatapenghuni = count($data_penghunisemua);
            //Menghitung sisa hari pembayaran
            $startTimeStamp = strtotime("now");
            $endTimeStamp = strtotime($jatuhtempo);
            $timeDiff = abs($endTimeStamp - $startTimeStamp);
            $numberDays = $timeDiff/86400;
            $numberDays = intval($numberDays);
            $textSisaHari = '';
            if($numberDays < 0){
                $textSisaHari = '';
            }else{
                $textSisaHari = $numberDays.' Hari lagi';
            }
            $judul = 'Data Penghuni';
        }
        return view('penghuni.okupan', ['jumlahdatapenghuni' => $jumlahdatapenghuni ,'data_penghuni' => $data_penghuni, 'data_penghunisemua' => $data_penghunisemua, 'data_kamar' => $data_kamar, 'data_keuangan' => $data_keuangan, 'data_user' => $data_user, 'judul' => $judul, 'sisahari' => $numberDays, 'textsisahari'=>$textSisaHari]);
    }

    public function create(Request $request)
    {

        $datakos = \App\model_datakos::select('id')->where('idpenjaga', Auth::user()->id)->get();
        foreach ($datakos as $rowkost) {
            /*-------------------------BAGIAN MENGISI DATA PENGHUNI PART 1-------------------------*/
            //DEKLARASI DATA FOTO PENGHUNI
            if ($request->hasFile('filefoto')) {
                $file = $request->file('filefoto');
                $extension = $file->getClientOriginalExtension();
                if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'JPG' || $extension == 'JPEG' || $extension == 'PNG') {
                    $filename = substr(sha1(mt_rand()),17,19).'-'.'premiere'.substr(sha1(mt_rand()),12,19).time() . '.' . $extension;
                    $file->move('uploads', $filename);
                    $request['fotonik'] = $filename;
                } else {
                    return back()->with('error', 'Mohon Upload Gambar dengan ekstensi .jpg,.jpeg atau .png');
                }
            }
            else{
                $request['fotonik']='none.jpg';
            }
            if (empty($request['platkendaraan'])) {
                $request['platkendaraan'] = '-';
            }
            $request['tag'] = rand(1111, 9999);
            //MEMBUAT DATA PENGHUNI BARU
            $request['idkost'] = $rowkost->id;
            \App\penghuni::create($request->all());
            //AKHIR DATA PENGHUNI BARU
            $datakamar = \App\kamar::find($request['idkamar']);
            $datakamar->status = 'terisi';
            $datakamar->save();

            $penghuni = \App\penghuni::where('idkamar', $request['idkamar'])->where('tanggalkeluar', null)->value('namapenghuni');
            /*-------------------------BAGIAN REDIRECT JIKA SUDAH-------------------------*/
            //PENGALIHAN HALAMAN
        }
        return redirect()->action('penghunicontroller@index')->with('success', 'Data ' . $penghuni . ' Berhasil Ditambahkan');
        //AKHIR PENGALIHAN HALAMAN
    }

    public function edit(Request $request)
    {

        $datakos = \App\model_datakos::select('id')->where('idpenjaga', Auth::user()->id)->get();
        foreach ($datakos as $rowkost) {
            /*-------------------------BAGIAN EDIT DATA KEUANGAN-------------------------*/
            //DEKLARASI DATA FOTO PENGHUNI
            if ($request->hasFile('filefoto')) {
                $file = $request->file('filefoto');
                $extension = $file->getClientOriginalExtension();
                if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'JPG' || $extension == 'JPEG' || $extension == 'PNG') {
                    $filename = time() . '.' . $extension;
                    $file->move('uploads', $filename);
                    $request['fotonik'] = $filename;
                    $request['fileexist'] = '';
                } else {
                    return back()->with('error', 'Mohon Upload Gambar dengan ekstensi .jpg,.jpeg atau .png');
                }
            } else {
                $request['fotonik'] = $request['fileexist'];
            }
            //AKHIR DATA PENGHUNI BARU
            //MEMINDAHKAN KAMAR PENGHUNI 
            $idkamarlama = \App\penghuni::where('id', $request['idpenghuni'])->value('idkamar');
            $jenistransaksi = \App\keuangan::where('idpenghuni', $request['idpenghuni'])->where('statustransaksi', 'belumbayar')->value('jenistransaksi');
            if ($request['idkamar'] != $idkamarlama) {
                $namakamarlama = \App\kamar::where('id', $idkamarlama)->value('namakamarkost');
                $namakamarbaru = \App\kamar::where('id', $request['idkamar'])->value('namakamarkost');
                $namapenghuni = \App\penghuni::where('id', $request['idpenghuni'])->value('namapenghuni');
                $keterangan = $namapenghuni . ' Pindah dari Kamar ' . $namakamarlama . ' ke Kamar ' . $namakamarbaru;
                $arrlog = ['idkamarlama' => $idkamarlama, 'idkamarbaru' => $request['idkamar'], 'keterangan' => $keterangan, 'tanggalpindah' => date("Y-m-d")];
                \App\model_logpindah::create($arrlog);
                //EDIT DATA PEMBAYARAN BULANAN YANG BARU
                if ($jenistransaksi == 'bulanan') {
                    $idkamarlama = \App\keuangan::where('idpenghuni', $request['idpenghuni'])->where('jenistransaksi', 'bulanan')->where('statustransaksi', 'belumbayar')->value('idkamar');
                    $idtransaksikamarlama = \App\keuangan::where('idpenghuni', $request['idpenghuni'])->where('jenistransaksi', 'bulanan')->where('statustransaksi', 'belumbayar')->value('id');
                    $hargakamarlama = \App\kamar::where('id', $idkamarlama)->value('harga');
                    $nominalbayarlama =  \App\keuangan::where('id', $idtransaksikamarlama)->value('nominaltransaksi');
                    $nominaltambahan =  \App\keuangan::where('id', $idtransaksikamarlama)->value('nominaltambahan');
                    $nominaltotallama = $nominalbayarlama - $nominaltambahan;
                    $lamasewa = $nominaltotallama / $hargakamarlama;
                    $hargakamarbaru = \App\kamar::where('id', $request['idkamar'])->value('harga');
                    $nominalbayarbaru = $hargakamarbaru * $lamasewa;
                    $keuangan = \App\keuangan::find($idtransaksikamarlama);
                    $keuangan->idkamar = $request['idkamar'];
                    $keuangan->nominaltransaksi = $nominalbayarbaru;
                    $keuangan->save();
                }
                //EDIT DATA STATUS KAMAR
                //ganti status kamar lama jadi kosong
                $datakamarlama = \App\kamar::find($idkamarlama);
                $datakamarlama->status = 'kosong';
                $datakamarlama->save();
                //ganti status kamar baru jadi terisi
                $datakamarbaru = \App\kamar::find($request['idkamar']);
                $datakamarbaru->status = 'terisi';
                $datakamarbaru->save();
            }

            //EDIT DATA PENGHUNI
            $id = $request['idpenghuni'];
            $idk = $request['idkamar'];
            $penghuni = penghuni::find($id);
            $penghuni->idkamar = $request['idkamar'];
            $penghuni->namapenghuni = $request['namapenghuni'];
            $penghuni->nik = $request['nik'];
            $penghuni->email = $request['email'];
            $penghuni->nopenghuni = $request['nopenghuni'];
            $penghuni->pekerjaan = $request['pekerjaan'];
            $penghuni->jeniskendaraan = $request['jeniskendaraan'];
            $penghuni->platkendaraan = $request['platkendaraan'];
            $penghuni->deskripsi = $request['deskripsi'];
            $penghuni->fotonik = $request['fotonik'];
            if ($request['ubahtanggalmasuk']) {
                $penghuni->tanggalmasuk = date('Y-m-d', strtotime($request['ubahtanggalmasuk']));
            }
            $penghuni->save();

            //AKHIR DATA PENGHUNI BARU
        }
        /*-------------------------BAGIAN REDIRECT JIKA SUDAH-------------------------*/
        //PENGALIHAN HALAMAN
        if ($request['idkamar'] != $idkamarlama) {
            return redirect('/penghuni')->with('success', 'Penghuni Kamar ' . $namakamarlama . ' Berhasil dipindahkan ke Kamar ' . $namakamarbaru);
        } else {
            return redirect()->action('penghunicontroller@index')->with('success', 'Data Penghuni Berhasil Diperbaharui');
            //AKHIR PENGALIHAN HALAMAN
        }
    }

    public function destroy($idpenghuni, $idkamar)
    {
        $namapenghuni = \App\penghuni::where('id', $idpenghuni)->value('namapenghuni');
        $penghuni = \App\penghuni::find($idpenghuni);
        $penghuni->tanggalkeluar = date("Y-m-d", strtotime("now"));
        $penghuni->save();

        $kamar =  \App\kamar::find($idkamar);
        $kamar->status = 'kosong';
        $kamar->save();

        if (\App\keuangan::where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'bulanan')->where('statustransaksi', 'belumbayar')->value('id')) {
            $idtransaksi = \App\keuangan::where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'bulanan')->where('statustransaksi', 'belumbayar')->value('id');

            $keuangan = \App\keuangan::find($idtransaksi);
            $keuangan->statustransaksi = 'out';
            $keuangan->save();
        }

        return redirect('/penghuni')->with('warning', 'Penghuni kos, telah dikeluarkan dari kos. Data penghuni ' . $namapenghuni . ' telah dihapus');
    }

    
    public function penghuni_signup(Request $request)
    {   
        return view('nonapp.penghuni_signup');
    }

    public function getKamarKostbyKost()
	{
		$idkost = $_GET['idkost'];
		$counterkamar = 0;
        $data_kamar['data'] = \App\kamar::select('id','namakamarkost')->where('idkost', $idkost)->where('status', 'kosong')->get();
		
		foreach ($data_kamar['data'] as $rowfinkamar) {
            $counterkamar++;
		}
		$data_kamar['jumlahdata'] = $counterkamar;

		echo json_encode($data_kamar);
		exit;
    }
    
    public function newPenghuni(Request $request)
    {
            /*-------------------------BAGIAN MENGISI DATA PENGHUNI PART 1-------------------------*/
            //DEKLARASI DATA FOTO PENGHUNI
            if ($request->hasFile('filefoto')) {
                $file = $request->file('filefoto');
                $extension = $file->getClientOriginalExtension();
                if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'JPG' || $extension == 'JPEG' || $extension == 'PNG') {
                    $filename = time() . '.' . $extension;
                    $file->move('uploads', $filename);
                    $request['fotonik'] = $filename;
                } else {
                    return back()->with('error', 'Mohon Upload Gambar dengan ekstensi .jpg,.jpeg atau .png');
                }
            }
            // else{
            //     $request['fotonik']=$request['fileexist'];
            // }
            if (empty($request['platkendaraan'])) {
                $request['platkendaraan'] = '-';
            }
            \App\penghuni::create($request->all());
            //AKHIR DATA PENGHUNI BARU
            $datakamar = \App\kamar::find($request['idkamar']);
            $datakamar->status = 'terisi';
            $datakamar->save();

            $penghuni = \App\penghuni::where('idkamar', $request['idkamar'])->where('tanggalkeluar', null)->value('namapenghuni');

            return redirect('/');
        
    }
}
