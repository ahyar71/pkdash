<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pengguna;
use Illuminate\Support\Facades\Auth;

class login extends Controller
{
    public function index()
    {
        $pengurus = Auth::guard('pengurus');
        $pemilik = Auth::guard('pemilik');
        $penghuni = Auth::guard('penghuni');
        if ($pengurus->check()) {
            return redirect('/pengurus');
        } else if ($pemilik->check()) {
            return redirect('/pemilik');
        } else if ($penghuni->check()) {
            return redirect('/okupan');
        } else {
            return view('login');
        }
    }
    public function landing()
    {

        $data_kamar = \App\kamar::all();

        $pengurus = Auth::guard('pengurus');
        $pemilik = Auth::guard('pemilik');
        if ($pengurus->check()) {
            return redirect('/login');
        } 
        
        if ($pemilik->check()) {
            return redirect('/login');
        } else {
            $tot_kosongsetia = 0;
            $tot_kosongpelita = 0;
            foreach ($data_kamar as $rowkamar) {
                if ($rowkamar->status == 'kosong' && $rowkamar->idkost == 1) {
                    $tot_kosongsetia++;
                } else if ($rowkamar->status == 'kosong' && $rowkamar->idkost == 2) {
                    $tot_kosongpelita++;
                }
            }
            return view('nonapp.landing', [
                'kosongsetia' => $tot_kosongsetia, 'kosongpelita' => $tot_kosongpelita
            ]);
        }
    }

    public function masuk(Request $kiriman)
    {
        $idpenghuni=0;
        $isAdmin = \App\pengguna::where('username', $kiriman->username)->where('password', md5($kiriman->password) . '781')->value('isAdmin');
        $id = \App\pengguna::where('username', $kiriman->username)->where('password', md5($kiriman->password) . '781')->value('id');
        if($id==null&&$id==''&&$isAdmin==null&&$isAdmin==''){
            $idpenghuni = \App\okupan::where('username', $kiriman->username)->where('tag', $kiriman->password)->value('id');
            if ($idpenghuni!=0) {
                Auth::guard('penghuni')->LoginUsingId($idpenghuni);
                return redirect('/loggedin')->with('sapaan', 'Penghuni Kos');
            }
        }
        if (empty($id)) {
            return back()->with('error', 'Username atau Password Salah');
        } else {
            if (!empty($isAdmin == '1')) {
                Auth::guard('pengurus')->LoginUsingId($id);
                return redirect('/loggedin')->with('sapaan', 'Penjaga Kos');
            } else if (!empty($isAdmin == '0')) {
                Auth::guard('pemilik')->LoginUsingId($id);
                return redirect('/loggedin')->with('sapaan', 'Pemilik Kos');
            } else {
                return redirect('/login');
            }
        }

    }

    public function keluar(Request $kiriman)
    {

        $pengurus = Auth::guard('pengurus');
        $pemilik = Auth::guard('pemilik');
        $penghuni = Auth::guard('penghuni');

        $kiriman->session()->flush();

        $kiriman->session()->regenerate();

        if ($pengurus->check()) {
            $pengurus->logout();
        } else if ($pemilik->check()) {
            $pemilik->logout();
        } else if ($penghuni->check()) {
            $penghuni->logout();
        }
        return redirect('/masuk')->with('info', 'Anda telah berhasil Logout!');
    }

    public function load()
    {
        // return view('loadpage');
        return redirect('/login');
    }
}
