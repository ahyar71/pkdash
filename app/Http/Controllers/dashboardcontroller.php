<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\pengguna;
use App\model_datakos;
use App\model_pengguna;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Mail\kirimemail;
use PDF;
use Illuminate\Support\Facades\Validator;
use DB;

class dashboardcontroller extends Controller
{
	public function home()
	{
		if (Auth::guard('pemilik')->check()) {
			$data_user = \App\pengguna::find(Auth::user()->id);
			$datakos = \App\model_datakos::all()->where('idpemilik', Auth::user()->id);
			return view('home', ['data_user' => $data_user, 'datakost' => $datakos]);
		}
	}

	public function etagih_index()
	{
		return view('nonapp.etagih_p');
	}

	public function profil_index()
	{
		return view('nonapp.profil_p');
	}

	public function faq_index()
	{
		return view('nonapp.faq_p');
	}

	// Fetch records
	public function getUsers1()
	{
		$username = $_GET['username'];
		$tag = $_GET['tag'];
		$counterkeuangan = 0;
		$tempData['datapenghuni'] = \App\penghuni::where('username', $username)->where('tag', $tag)->orderBy('id', 'asc')->get();
		foreach ($tempData['datapenghuni'] as $rowpenghuni) {
			$tempData['datakeuangan'] = \App\keuangan::where('idpenghuni', $rowpenghuni->id)->where('statustransaksi', 'belumbayar')->first();
			$counterkeuangan++;
		}
		$tempData['jumlahdata'] = $counterkeuangan;

		echo json_encode($tempData);
		exit;
	}

	public function getFinKamar()
	{
		$idkamar = $_GET['idkamarpilihan'];
		$tahun = $_GET['tahun'];
		$tempData['authid'] = $_GET['idkosan'];
		$idkost = $_GET['idkosan'];
		$counterkamar = 0;
		if ($idkamar == 'all') {
			$getData['tempfinkamar'] = \App\keuangan::where('statustransaksi', 'sudah')->where('idkost', $idkost)->orderBy('jatuhtempo', 'asc')->get();
		} else {
			$getData['tempfinkamar'] = \App\keuangan::where('idkamar', $idkamar)->where('idkost', $idkost)->where('statustransaksi', 'sudah')->get();
		}
		foreach ($getData['tempfinkamar'] as $rowfinkamar) {
			if ($rowfinkamar->jenistransaksi == "bulanan" || $rowfinkamar->jenistransaksi == "lunaslangsung" || $rowfinkamar->jenistransaksi == "lunas" || $rowfinkamar->jenistransaksi == "dp") {
				if (date('Y', strtotime($rowfinkamar->jatuhtempo)) == $tahun) {
					$tempData['datafinkamar'][$counterkamar] = $rowfinkamar;
					$tempData['datafinkamar'][$counterkamar]->namapenghuni = \App\penghuni::where('id', $rowfinkamar->idpenghuni)->value('namapenghuni', 'id');
					$tempData['datafinkamar'][$counterkamar]->namakamarkost = \App\kamar::where('id', $rowfinkamar->idkamar)->value('namakamarkost', 'id');
					$counterkamar++;
				}
			}
		}
		$tempData['jumlahdata'] = $counterkamar;

		echo json_encode($tempData);
		exit;
	}

	// Fetch records
	public function getWarning()
	{
		$idkeuangan = $_GET['idkeuanganbulanan'];
		$counterkeuangan = 0;
		$data['tanggaltempo'] = date('yy-m-d');
		$tempData['datakeuangan'] = \App\keuangan::where('id', $idkeuangan)->get();
		$data['hariterlewat'] = 0;
		foreach ($tempData['datakeuangan'] as $rowkeuangan) {
			if($rowkeuangan->statustransaksi=='belumbayar'){
				if((strtotime("-3 days")>=strtotime($rowkeuangan->jatuhtempo))&&(strtotime("-7 days")<=strtotime($rowkeuangan->jatuhtempo))){
					$data['hariterlewat'] = 3;
				}else if((strtotime("-7 days")>=strtotime($rowkeuangan->jatuhtempo))){
					$data['hariterlewat'] = 7;
				}else{
					$data['hariterlewat'] = 0;
				}
				$data['tanggaltempo']=date('Y-m-d', strtotime($rowkeuangan->jatuhtempo. ' + 1 month'));
			}
			$counterkeuangan++;
		}
		$data['jumlahdata'] = $counterkeuangan;

		echo json_encode($data);
		exit;
	}
	// Fetch records
	public function getRincian()
	{
		//tes
		$start = $_GET['start'];
		$counterrincian = 0;
		$isAdmin = Auth::user()->isAdmin;
		if($isAdmin==0){
			$idkos = Session::get('idkost');
			$datakos = \App\model_datakos::where('id', last($idkos))->get();
		}else if($isAdmin==1){
			$idpenjaga = Auth::user()->id;
			$datakos = \App\model_datakos::where('idpenjaga', $idpenjaga)->get();	
		}
		foreach ($datakos as $rowkost) {
		$datak['datarincian'] = \App\keuangan::where('statustransaksi', 'sudah')->where('idkost', $rowkost->id)->skip($start)->take(10)->orderBy('updated_at', 'desc')->get();
		$datak['datanotpaid'] = \App\keuangan::where('statustransaksi', 'belumbayar')->where('idkost', $rowkost->id)->orderBy('updated_at', 'desc')->get();
		
		}
		foreach ($datak['datarincian'] as $rowrincian) {
			if($rowrincian->statustransaksi=='sudah'){
				$rowrincian->namakamar = $rowrincian->getKamarKost();
				$counterrincian++;
			}
		}
		$datak['jumlahdata'] = $counterrincian;
		return response($datak, 200); 
	}



	public function mail(request $isiemail)
	{
		$name = $isiemail['nama'];
		$password = $isiemail['pass'];
		$email = $isiemail['email'];
		$wa = $isiemail['wa'];
		Mail::to('ahyar93@gmail.com')->send(new kirimemail($name, $password, $email, $wa));

		return back()->with('success', 'Mohon Menunggu Balasan dari Admin');
	}


	public function newKost(Request $request)
	{
		$validator = Validator::make($request->all(), ['usernamepenjaga' => 'required|unique:pengguna,username']);
		$validatorkost = Validator::make($request->all(), ['usernamekost' => 'required|unique:data_kost,kost']);

		if ($validator->fails()) {
			return back()->with('error', 'Username Penjaga Kost Telah Ada, Silahkan input username yang lain.');
		} else if ($validatorkost->fails()) {
			return back()->with('error', 'Username Kost Telah Ada, Silahkan input username yang lain.');
		} else {

			$datapengguna['nama'] = $request['namapenjaga'];
			$datapengguna['password'] = md5($request['passpenjaga']) . '781';
			$datapengguna['username'] = $request['usernamepenjaga'];
			$datapengguna['isAdmin'] = '1';
			\App\model_pengguna::create($datapengguna);
			$idpenjaga = \App\pengguna::where('username', $request['usernamepenjaga'])->value('id');

			$datakost['kost'] = $request['usernamekost'];
			$datakost['namakost'] = $request['namakost'];
			$datakost['gambar'] = 'premiere.jpg';
			$datakost['idpenjaga'] = $idpenjaga;
			$datakost['idpemilik'] = Auth::user()->id;
			\App\model_datakos::create($datakost);
			$idkost = \App\model_datakos::where('kost', $request['usernamekost'])->value('id');

			$datapenjaga = \App\model_pengguna::find($idpenjaga);
			$datapenjaga->idkost = $idkost;
			$datapenjaga->save();
		}
		return redirect()->action('dashboardcontroller@home')->with('success', 'Data Kost ' . $request['namakost'] . ' Berhasil Ditambah');
	}


	public function pengurus()
	{

		$datakos = \App\model_datakos::where('idpenjaga', Auth::user()->id)->get();
		foreach ($datakos as $rowkost) {

			$datakost = \App\model_datakos::all()->where('id', $rowkost->id);
			$data_aset = \App\model_dataaset::where('idkost', $rowkost->id)->get();
			$data_penghuni = \App\penghuni::all()->where('tanggalkeluar', null)->where('idkost', $rowkost->id);
			$data_kamar = \App\kamar::where('idkost', $rowkost->id)->get();
			$data_keuangan = \App\keuangan::where('idkost', $rowkost->id)->get();
			$data_keuanganlimit = \App\keuangan::limit(10)->where('statustransaksi', 'sudah')->orderBy('updated_at', 'desc')->where('idkost', $rowkost->id)->get();
			$data_user = \App\pengguna::find(Auth::user()->id);
			$databelumbayar = DB::table('keuangan')
				->join('penghuni', 'keuangan.idpenghuni', '=', 'penghuni.id')
				->join('kamar', 'keuangan.idkamar', '=', 'kamar.id')
				->select('keuangan.*', 'penghuni.namapenghuni', 'kamar.namakamarkost')
				->orderBy('jatuhtempo', 'desc')
				->where('keuangan.idkost', $rowkost->id)
				->get();
			// foreach ($data_keuangan as $rowuang) {
			// 	if ($rowuang->statustransaksi == 'belumbayar') {
			// 		if ($rowuang->jenistransaksi == 'lunas') {
			// 			if (strtotime("now") >= strtotime($rowuang->jatuhtempo)) {
			// 				$rowuang->statustransaksi = 'dphangus';
			// 				$rowuang->save();
			// 				$penghuni = \App\penghuni::find($rowuang->idpenghuni);
			// 				$penghuni->tanggalkeluar = date("Y-m-d", strtotime("now"));
			// 				$penghuni->save();
			// 			}
			// 		} elseif ($rowuang->jenistransaksi == 'bulanan') {
			// 			if (strtotime("now") >= strtotime($rowuang->jatuhtempo) && (strtotime("-3 day") >= strtotime($rowuang->jatuhtempo))) {
			// 				$idkamar = $rowuang->idkamar;
			// 				$hargakamar = \App\kamar::find($idkamar);
			// 				$datanominal = \App\keuangan::find($rowuang->id);
			// 				$nominaltambahanyangada = \App\keuangan::where('id', $rowuang->id)->value('nominaltambahan');
			// 				if ($nominaltambahanyangada == 300000) {
			// 					$tambahan = (int) (0.1 * $hargakamar->harga);
			// 					$datanominal->nominaltambahan += $tambahan;
			// 					$datanominal->save();
			// 				} elseif ($nominaltambahanyangada == null || $nominaltambahanyangada == 0) {
			// 					$tambahan = (int) (0.1 * $hargakamar->harga);
			// 					$datanominal->nominaltambahan = $tambahan;
			// 					$datanominal->save();
			// 				}
			// 			}
			// 			if (strtotime("now") >= strtotime($rowuang->jatuhtempo) && (strtotime("-8 day") >= strtotime($rowuang->jatuhtempo))) {
			// 				$idpenghuni = $rowuang->idpenghuni;
			// 				$idkamar = $rowuang->idkamar;
			// 				$kamar = \App\kamar::find($idkamar);
			// 				$kamar->status = 'kosong';
			// 				$kamar->save();
			// 				$penghuni = \App\penghuni::find($idpenghuni);
			// 				$penghuni->tanggalkeluar = date("Y-m-d", strtotime("now"));
			// 				$penghuni->save();
			// 				$rowuang->statustransaksi = 'batal';
			// 				$rowuang->save();
			// 			}
			// 		}
			// 	}
			// }
			foreach ($data_penghuni as $rowpenghuni) {
				if ($rowpenghuni->idkamar != $rowpenghuni->getIdKamarKeuangan() && $rowpenghuni->tanggalkeluar == null && strtotime($rowpenghuni->tanggalmasuk) <= strtotime("-4 day")) {
					$kamar =  \App\kamar::find($rowpenghuni->idkamar);
					$kamar->status = 'kosong';
					$kamar->save();
					$destinationPath = public_path('/uploads');
					$filename = $destinationPath . '/' . $rowpenghuni->fotonik;
					if (File::exists($filename)) {
						File::delete($filename);
					}

					$penghuni = \App\penghuni::find($rowpenghuni->id);
					$penghuni->delete();
				} else if (
					$rowpenghuni->idkamar == $rowpenghuni->getIdKamarKeuangan()
					&& $rowpenghuni->id != $rowpenghuni->getIdPenghuniKeuangan()
					&& $rowpenghuni->getStatusTransaksi() == null
					&& $rowpenghuni->tanggalkeluar == null
					&& strtotime($rowpenghuni->tanggalmasuk) <= strtotime("-4 day")
				) {

					$kamar = \App\kamar::find($rowpenghuni->idkamar);
					$kamar->status = 'kosong';
					$kamar->save();
					$destinationPath = public_path('/uploads');
					$filename = $destinationPath . '/' . $rowpenghuni->fotonik;
					if (File::exists($filename)) {
						File::delete($filename);
					}


					$penghuni = \App\penghuni::find($rowpenghuni->id);
					$penghuni->delete();
				}
			}

			$totaluang = 0;
			$txuang = 0;
			$temp = 0;
			foreach ($data_keuangan as $rowuang) {
				if ($rowuang->statustransaksi == 'sudah') {
					if ($rowuang->pilihantransaksi == "pemasukkan" && $rowuang->metode == "tunai") {
						$totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
					} 
					if ($rowuang->pilihantransaksi == "pengeluaran" && $rowuang->metode == "tunai") {
						$totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
					}
				}
			}
			$txuang = $totaluang;


			$tot_kosong = 0;
			$totalkamarterisi = 0;
			$persenkamarterisi = 0;
			foreach ($data_kamar as $rowkamar) {
				if ($rowkamar->status == 'terisi') {
					$totalkamarterisi++;
				} else if ($rowkamar->status == 'kosong') {
					$tot_kosong++;
				}
			}
			$totalpenghuni = count($data_penghuni);
			$totalkamar = count($data_kamar);
			if ($totalkamar != 0) {
				$persenkamarterisi = $totalkamarterisi / $totalkamar * 100;
			}



			$totalbelumbayar = 0;
			foreach ($data_keuangan as $rowuang) {
				if ($rowuang->jenistransaksi == 'bulanan' && $rowuang->statustransaksi == 'belumbayar') {
					if (strtotime("+3 day") >= strtotime($rowuang->jatuhtempo)) {
						$totalbelumbayar++;
					}
				}
				if ($rowuang->jenistransaksi == 'lunas' && $rowuang->statustransaksi == 'belumbayar') {
					if (strtotime("now") >= strtotime($rowuang->jatuhtempo)) {
						$totalbelumbayar++;
					}
				}
			}
			foreach ($data_penghuni as $rowpenghuni) {
				if ($rowpenghuni->idkamar != $rowpenghuni->getIdKamarKeuangan()) {
					$totalbelumbayar++;
				} elseif ($rowpenghuni->idkamar == $rowpenghuni->getIdKamarKeuangan() && $rowpenghuni->id != $rowpenghuni->getIdPenghuniKeuangan() && $rowpenghuni->getStatusTransaksi() == null) {
					$totalbelumbayar++;
				}
			}

			$totalmotor = 0;
			$totalmobil = 0;
			foreach ($data_penghuni as $rowpenghuni) {
				if ($rowpenghuni->jeniskendaraan == 'motor') {
					$totalmotor++;
				} else if ($rowpenghuni->jeniskendaraan == 'mobil') {
					$totalmobil++;
				}
			}

			$hasilpencarian = '';

			$judul = 'Data Penghuni';
		}

		return view('dashboardpengurus', [
			'data_kamar' => $data_kamar, 'data_keuangan' => $data_keuangan, 'data_penghuni' => $data_penghuni, 'data_user' => $data_user, 'data_pencarian' => $hasilpencarian, 'databelumbayar' => $databelumbayar, 'data_keuanganlimit' => $data_keuanganlimit, 'totaluang' => $txuang, 'totalpenghuni' => $totalpenghuni, 'totalkamar' => $totalkamar,
			'persenkamarterisi' => $persenkamarterisi, 'totalbelumbayar' => $totalbelumbayar, 'totalmobil' => $totalmobil, 'totalmotor' => $totalmotor, 'tot_kosong' => $tot_kosong, 'totalkamarterisi' => $totalkamarterisi, 'data_aset' => $data_aset, 'idkost' => $datakos, 'data_kost' => $datakost
		]);
	}

	public function pemilik($idk)
	{	

		Session::push('idkost', $idk);
		$idkos = Session::get('idkost');
		$datakost = \App\model_datakos::all()->where('id', last($idkos));
		foreach ($datakost as $rowkost) {
			$data_aset = \App\model_dataaset::where('idkost', $rowkost->id)->get();
			$data_penghuni = \App\penghuni::all()->where('tanggalkeluar', null)->where('idkost', $rowkost->id);
			$data_kamar = \App\kamar::where('idkost', $rowkost->id)->get();
			$data_keuangan = \App\keuangan::all()->where('idkost', $rowkost->id);
			$databelumbayar = DB::table('keuangan')
				->join('penghuni', 'keuangan.idpenghuni', '=', 'penghuni.id')
				->join('kamar', 'keuangan.idkamar', '=', 'kamar.id')
				->select('keuangan.*', 'penghuni.namapenghuni', 'kamar.namakamarkost')->orderBy('jatuhtempo', 'desc')
				->where('keuangan.idkost', $rowkost->id)->get();
			$data_keuanganlimit = \App\keuangan::limit(10)->where('statustransaksi', 'sudah')->orderBy('updated_at', 'desc')->where('idkost', $rowkost->id)->get();
			$data_keuangan_notpaid = \App\keuangan::where('statustransaksi', 'belumbayar')->where('idkost', $rowkost->id)->orderBy('updated_at', 'desc')->get();
			$data_pengguna = \App\pengguna::all();
			$data_penghuni = \App\penghuni::all()->where('tanggalkeluar', null)->where('idkost', $rowkost->id);
			$data_user = \App\pengguna::find(Auth::user()->id);
			// foreach ($data_keuangan as $rowuang) {
			// 	if ($rowuang->statustransaksi == 'belumbayar') {
			// 		if ($rowuang->jenistransaksi == 'lunas') {
			// 			if (strtotime("now") >= strtotime($rowuang->jatuhtempo)) {
			// 				$rowuang->statustransaksi = 'dphangus';
			// 				$rowuang->save();
			// 				$penghuni = \App\penghuni::find($rowuang->idpenghuni);
			// 				$penghuni->tanggalkeluar = date("Y-m-d", strtotime("now"));
			// 				$penghuni->save();
			// 			}
			// 		} elseif ($rowuang->jenistransaksi == 'bulanan') {
			// 			if (strtotime("now") >= strtotime($rowuang->jatuhtempo) && (strtotime("-3 day") >= strtotime($rowuang->jatuhtempo))) {
			// 				$idkamar = $rowuang->idkamar;
			// 				$hargakamar = \App\kamar::find($idkamar);
			// 				$datanominal = \App\keuangan::find($rowuang->id);
			// 				$tambahan = (int) (0.1 * $hargakamar->harga);
			// 				$datanominal->nominaltambahan = $tambahan;
			// 				$datanominal->save();
			// 			}
			// 			if (strtotime("now") >= strtotime($rowuang->jatuhtempo) && (strtotime("-8 day") >= strtotime($rowuang->jatuhtempo))) {
			// 				$idpenghuni = $rowuang->idpenghuni;
			// 				$idkamar = $rowuang->idkamar;
			// 				$kamar = \App\kamar::find($idkamar);
			// 				$kamar->status = 'kosong';
			// 				$kamar->save();
			// 				$penghuni = \App\penghuni::find($idpenghuni);
			// 				$penghuni->tanggalkeluar = date("Y-m-d", strtotime("now"));
			// 				$penghuni->save();
			// 				$rowuang->statustransaksi = 'batal';
			// 				$rowuang->save();
			// 			}
			// 		}
			// 	}
			// }

			foreach ($data_penghuni as $rowpenghuni) {
				if ($rowpenghuni->idkamar != $rowpenghuni->getIdKamarKeuangan() && $rowpenghuni->tanggalkeluar == null && strtotime($rowpenghuni->tanggalmasuk) <= strtotime("-4 day")) {
					$kamar =  \App\kamar::find($rowpenghuni->idkamar);
					$kamar->status = 'kosong';
					$kamar->save();
					$destinationPath = public_path('/uploads');
					$filename = $destinationPath . '/' . $rowpenghuni->fotonik;
					if (File::exists($filename)) {
						File::delete($filename);
					}

					$penghuni = \App\penghuni::find($rowpenghuni->id);
					$penghuni->delete();
				} elseif ($rowpenghuni->idkamar == $rowpenghuni->getIdKamarKeuangan() && $rowpenghuni->id != $rowpenghuni->getIdPenghuniKeuangan() && $rowpenghuni->getStatusTransaksi() == null && $rowpenghuni->tanggalkeluar == null && strtotime($rowpenghuni->tanggalmasuk) <= strtotime("-4 day")) {

					$kamar =  \App\kamar::find($rowpenghuni->idkamar);
					$kamar->status = 'kosong';
					$kamar->save();

					$destinationPath = public_path('/uploads');
					$filename = $destinationPath . '/' . $rowpenghuni->fotonik;
					if (File::exists($filename)) {
						File::delete($filename);
					}


					$penghuni = \App\penghuni::find($rowpenghuni->id);
					$penghuni->delete();
				}
			}


			$profile = $this->http_request("api.openweathermap.org/data/2.5/weather?id=1649378&appid=2a65b8281996d0f24298e2b416c666fd");

			// ubah string JSON menjadi array
			$profile = json_decode($profile, TRUE);


			$namapenjaga = \App\model_pengguna::select('nama')->where('isAdmin', 1)->where('idkost', $rowkost->id)->get();

			
			$totaluang = 0;
			$txuang = 0;
			$temp = 0;
			foreach ($data_keuangan as $rowuang) {
				if ($rowuang->statustransaksi == 'sudah') {
					if ($rowuang->pilihantransaksi == "pemasukkan" && $rowuang->metode == "tunai") {
						$totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
					} 
					if ($rowuang->pilihantransaksi == "pengeluaran" && $rowuang->metode == "tunai") {
						$totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
					}
				}
			}
			$txuang = $totaluang;


			$tot_kosong = 0;
			$totalkamarterisi = 0;
			foreach ($data_kamar as $rowkamar) {
				if ($rowkamar->status == 'terisi') {
					$totalkamarterisi++;
				} else if ($rowkamar->status == 'kosong') {
					$tot_kosong++;
				}
			}
			$totalpenghuni = count($data_penghuni);
			$totalkamar = count($data_kamar);
			if ($totalkamar != 0) {
				$persenkamarterisi = $tot_kosong / $totalkamar * 100;
			} else {
				$persenkamarterisi = 0;
			}

			$totalbelumbayar = 0;
			foreach ($data_keuangan as $rowuang) {
				if ($rowuang->jenistransaksi == 'bulanan' && $rowuang->statustransaksi == 'belumbayar') {
					if (strtotime("+3 day") >= strtotime($rowuang->jatuhtempo)) {
						$totalbelumbayar++;
					}
				}
				if ($rowuang->jenistransaksi == 'lunas' && $rowuang->statustransaksi == 'belumbayar') {
					if (strtotime("now") >= strtotime($rowuang->jatuhtempo)) {
						$totalbelumbayar++;
					}
				}
			}
			foreach ($data_penghuni as $rowpenghuni) {
				if ($rowpenghuni->idkamar != $rowpenghuni->getIdKamarKeuangan()) {
					$totalbelumbayar++;
				} elseif ($rowpenghuni->idkamar == $rowpenghuni->getIdKamarKeuangan() && $rowpenghuni->id != $rowpenghuni->getIdPenghuniKeuangan() && $rowpenghuni->getStatusTransaksi() == null) {
					$totalbelumbayar++;
				}
			}

			$totalmotor = 0;
			$totalmobil = 0;
			foreach ($data_penghuni as $rowpenghuni) {
				if ($rowpenghuni->jeniskendaraan == 'motor') {
					$totalmotor++;
				} else if ($rowpenghuni->jeniskendaraan == 'mobil') {
					$totalmobil++;
				}
			}

			$hasilpencarian = '';

			$judul = 'Data Penghuni';
		}
		return view('dashboardpemilik', ['data_kamar' => $data_kamar, 'data_keuangan' => $data_keuangan, 'data_penghuni' => $data_penghuni, 'data_user' => $data_user, 'data_pencarian' => $hasilpencarian, 'data_pengguna' => $data_pengguna, 'databelumbayar' => $databelumbayar, 'data_keuanganlimit' => $data_keuanganlimit,'data_keuangan_notpaid'=>$data_keuangan_notpaid, 'totaluang' => $totaluang, 'totalpenghuni' => $totalpenghuni, 'totalkamar' => $totalkamar, 'persenkamarterisi' => $persenkamarterisi, 'totalbelumbayar' => $totalbelumbayar, 'totalmobil' => $totalmobil, 'totalmotor' => $totalmotor, 'tot_kosong' => $tot_kosong, 'totalkamarterisi' => $totalkamarterisi, 'datakost' => $datakost, 'data_aset' => $data_aset, 'namapenjaga' => $namapenjaga]);
	}


	public function http_request($url)
	{
		// persiapkan curl
		$ch = curl_init();

		// set url 
		curl_setopt($ch, CURLOPT_URL, $url);

		// set user agent    
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

		// return the transfer as a string 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// $output contains the output string 
		$output = curl_exec($ch);

		// tutup curl 
		curl_close($ch);

		// mengembalikan hasil curl
		return $output;
	}

	public function caritanggal(Request $request)
	{

		if (empty($request)) {
			return redirect('/pengurus');
		}

		$data_penghuni = \App\penghuni::all();
		$data_kamar = \App\kamar::orderBy('status')->get();
		$data_keuangan = \App\keuangan::all();
		$data_user = \App\pengguna::find(Auth::user()->id);

		$from = date("Y-m-d", strtotime($request['from']));
		$to = date("Y-m-d", strtotime($request['to']));

		$hasilpencarian = \App\keuangan::whereBetween('tanggaltransaksi', [$from, $to])->where('statustransaksi', 'sudah')->orderBy('updated_at', 'desc')->get();

		if (empty($hasilpencarian)) {
			$hasilpencarian = "tidakada";
		}

		return view('pencarian', ['data_penghuni' => $data_penghuni, 'data_kamar' => $data_kamar, 'data_keuangan' => $data_keuangan, 'data_user' => $data_user, 'data_pencarian' => $hasilpencarian, 'tanggalawal' => $from, 'tanggalakhir' => $to]);
	}


	public function downloadfile(Request $request)
	{
		if (empty($request)) {
			return redirect('/keuangan');
		}

		$from = '';
		$to = '';
		$tanggalawal = $request['from'];
		$tanggalakhir = $request['to'];
		$pilihanwaktu = $request['pilihanwaktu'];
		$pilihanbulan = $request['pilihanbulan'];
		$pilihantahun = $request['pilihantahun'];
		$tahun = $request['tahun'];
		$dasar = $request['dasar'];

		if ($dasar == 'rentang') {
			$from = date("Y-m-d", strtotime($tanggalawal));
			$to = date("Y-m-d", strtotime($tanggalakhir));
			$hasilpencarian = \App\keuangan::whereBetween('tanggaltransaksi', [$from, $to])->where('statustransaksi', 'sudah')->orderBy('updated_at', 'asc')->get();
		} elseif ($dasar == 'bulan') {
			$hasilpencarian = \App\keuangan::whereYear('tanggaltransaksi', '=', $pilihantahun)->whereMonth('tanggaltransaksi', '=', $pilihanbulan)->where('statustransaksi', 'sudah')->orderBy('updated_at', 'asc')->get();
		} else {
			return back()->with('error', 'Mohon Refresh Halaman dan Isikan Data Dengan Benar');
		}




		$data = ['data_pencarian' => $hasilpencarian, 'tanggalawal' => $from, 'tanggalakhir' => $to, 'pilihanbulan' => $pilihanbulan, 'pilihantahun' => $pilihantahun, 'dasar' => $dasar];
		$pdf = PDF::loadView('laporan', $data);

		return $pdf->download('laporankeuangan' . time() . '.pdf');
	}
}
