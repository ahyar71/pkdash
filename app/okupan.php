<?php

    namespace App;

    use Illuminate\Notifications\Notifiable;
    use Illuminate\Foundation\Auth\User as Authenticatable;

class okupan extends Authenticatable
{
    use Notifiable;

	protected $table = 'penghuni';

    protected $fillable = ['username', 'tag', 'namapenghuni', 'idkost', 'idkamar', 'nik', 'email', 'nopenghuni', 'pekerjaan', 'jeniskendaraan', 'platkendaraan', 'tanggalmasuk', 'deskripsi', 'fotonik','remember_token', 'created_at', 'updated_at'];

}