<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKamarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('namakamarkost');
            $table->string('lokasikost');
            $table->string('kasur');
            $table->string('kmdalam');
            $table->string('ac');
            $table->string('mejakursi');
            $table->string('lemari');
            $table->string('kmluar');
            $table->string('pemanasair');
            $table->string('tv');
            $table->string('air');
            $table->string('wifi');
            $table->string('listriktoken');
            $table->string('harga');
            $table->string('catatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamar');
    }
}
