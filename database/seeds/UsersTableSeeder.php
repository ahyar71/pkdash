<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pengguna')->insert([
        	[
        		'nama' => 'Admin 1',
        		'username' => 'admin1',
        		'password' => bcrypt('pass1'),
        		'isAdmin' => '1',
        		'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
        		'nama' => 'Pengguna 1',
        		'username' => 'pengguna1',
        		'password' => bcrypt('pass1'),
        		'isAdmin' => '0',
        		'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        	]
    	]);
    }
}